# Implementation and characterization of a low-resolution multi-GHz flash-ADC utilizing FPGA I/O ressources

## TODO

## 2-bit CMOD A7 pin mapping

### Function generator

+ `fct_gen_o` : Pmod header 0 (LVCMOS33)
+ `fct_gen_n` : PIO 21
+ `fct_gen_p` : PIO 22

### SPI DAC interface

+ `mosi_o` : PIO 5
+ `cs_o[0]` : PIO 7
+ `cs_o[1]` : PIO 9
+ `cs_o[2]` : PIO 42
+ `sck_o` : PIO 8
+ `dac_vref_p` : PIO 18
+ `dac_vref_n` : PIO 19

### ADC input

+ `analog_i[0]` : PIO 12
+ `analog_i[1]` : PIO 6
+ `analog_i[2]` : PIO 20
+ `threshold_i[0]` : PIO 14
+ `threshold_i[1]` : PIO 11
+ `threshold_i[2]` : PIO 17

## 3-bit CMOD A7 pin mapping

### Channel 1 : Bank 34

+ `analog_ch1_i[0]`    : `IO_L1P_T0_34 Sch=pio[28]`       : X1Y0
+ `threshold_ch1_i[0]` : `IO_L1N_T0_34 Sch=pio[30]`       : X1Y0

+ `analog_ch1_i[1]`    : `IO_L2P_T0_34 Sch=pio[26]`       : X1Y0
+ `threshold_ch1_i[1]` : `IO_L2N_T0_34 Sch=pio[27]`       : X1Y0

+ `analog_ch1_i[2]`    : `IO_L3P_T0_DQS_34 Sch=pio[29]`   : X1Y0
+ `threshold_ch1_i[2]` : `IO_L3N_T0_DQS_34 Sch=pio[31]`   : X1Y0

+ `analog_ch1_i[3]`    : `IO_L5P_T0_34 Sch=pio[33]`       : X1Y0
+ `threshold_ch1_i[3]` : `IO_L5N_T0_34 Sch=pio[32]`       : X1Y0

+ `analog_ch1_i[4]`    : `IO_L6P_T0_34 Sch=pio[35]`       : X1Y0
+ `threshold_ch1_i[4]` : `IO_L6N_T0_VREF_34 Sch=pio[34]`  : X1Y0

+ `analog_ch1_i[5]`    : `IO_L11P_T1_SRCC_34 Sch=pio[38]` : X1Y0
+ `threshold_ch1_i[5]` : `IO_L11N_T1_SRCC_34 Sch=pio[37]` : X1Y0

+ `analog_ch1_i[6]`    : `IO_L16P_T2_34 Sch=pio[41]`      : X1Y1
+ `threshold_ch1_i[6]` : `IO_L16N_T2_34 Sch=pio[39]`      : X1Y1

### Channel 2 : Bank 35

+ `analog_ch2_i[0]`    : `IO_L10P_T1_AD15P_35 Sch=pio[22]`   : X1Y1
+ `threshold_ch2_i[0]` : `IO_L10N_T1_AD15N_35 Sch=pio[21]`   : X1Y1

+ `analog_ch2_i[1]`    : `IO_L9P_T1_DQS_AD7P_35 Sch=pio[20]` : X1Y1
+ `threshold_ch2_i[1]` : `IO_L9N_T1_DQS_AD7N_35 Sch=pio[17]` : X1Y1

+ `analog_ch2_i[2]`    : `IO_L12P_T1_MRCC_35 Sch=pio[18]`    : X1Y1
+ `threshold_ch2_i[2]` : `IO_L12N_T1_MRCC_35 Sch=pio[19]`    : X1Y1

+ `analog_ch2_i[3]`    : `IO_L5P_T0_AD13P_35 Sch=pio[12]`    : X1Y1
+ `threshold_ch2_i[3]` : `IO_L5N_T0_AD13N_35 Sch=pio[14]`    : X1Y1

+ `analog_ch2_i[4]`    : `IO_L3P_T0_DQS_AD5P_35 Sch=pio[06]` : X1Y1
+ `threshold_ch2_i[4]` : `IO_L3N_T0_DQS_AD5N_35 Sch=pio[11]` : X1Y1

+ `analog_ch2_i[5]`    : `IO_L7P_T1_AD6P_35 Sch=pio[10]`     : X1Y1
+ `threshold_ch2_i[5]` : `IO_L7N_T1_AD6N_35 Sch=pio[04]`     : X1Y1

+ `analog_ch2_i[6]`    : `IO_L8P_T1_AD14P_35 Sch=pio[02]`    : X1Y1
+ `threshold_ch2_i[6]` : `IO_L8N_T1_AD14N_35 Sch=pio[01]`    : X1Y1

<!--
### Bank 34

IO_L1P_T0_34 Sch=pio[28]
IO_L1N_T0_34 Sch=pio[30]

IO_L2P_T0_34 Sch=pio[26]
IO_L2N_T0_34 Sch=pio[27]

IO_L3P_T0_DQS_34 Sch=pio[29]
IO_L3N_T0_DQS_34 Sch=pio[31]

IO_L5P_T0_34 Sch=pio[33]
IO_L5N_T0_34 Sch=pio[32]

IO_L6P_T0_34 Sch=pio[35]
IO_L6N_T0_VREF_34 Sch=pio[34]

IO_L11P_T1_SRCC_34 Sch=pio[38]
IO_L11N_T1_SRCC_34 Sch=pio[37]

IO_L16P_T2_34 Sch=pio[41]
IO_L16N_T2_34 Sch=pio[39]

IO_L9P_T1_DQS_34 Sch=pio[44]
IO_L9N_T1_DQS_34 Sch=pio[42]

### Bank 35

IO_L8P_T1_AD14P_35 Sch=pio[02]
IO_L8N_T1_AD14N_35 Sch=pio[01]

IO_L7P_T1_AD6P_35 Sch=pio[10]
IO_L7N_T1_AD6N_35 Sch=pio[04]

IO_L3P_T0_DQS_AD5P_35 Sch=pio[06]
IO_L3N_T0_DQS_AD5N_35 Sch=pio[11]

IO_L5P_T0_AD13P_35 Sch=pio[12]
IO_L5N_T0_AD13N_35 Sch=pio[14]

IO_L12P_T1_MRCC_35 Sch=pio[18]
IO_L12N_T1_MRCC_35 Sch=pio[19]

IO_L9P_T1_DQS_AD7P_35 Sch=pio[20]
IO_L9N_T1_DQS_AD7N_35 Sch=pio[17]

IO_L10P_T1_AD15P_35 Sch=pio[22]
IO_L10N_T1_AD15N_35 Sch=pio[21]
-->

[Multi-cycle paths](https://www.xilinx.com/support/answers/63222.html)
