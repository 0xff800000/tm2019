library ieee;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

entity adctop is
    generic(N : integer := 1);
    port(
            threshold_i : in  std_logic_vector((2**N-2) downto 0);
            analog_i    : in  std_logic_vector((2**N-2) downto 0);
            digital_o   : out std_logic_vector(N-1 downto 0);
            clk_i       : in std_logic
        );
end adctop;

architecture arch of adctop is
    signal therm_s : std_logic_vector((2**N-2) downto 0);
    signal cmp_s : std_logic_vector((2**N-2) downto 0);
    signal Dout_s : std_logic_vector(N-1 downto 0);

    component comparator
        generic(N : integer := 1);
        port(
                threshold_i : in  std_logic_vector((2**N-2) downto 0);
                analog_i    : in  std_logic_vector((2**N-2) downto 0);
                cmp_o       : out std_logic_vector((2**N-2) downto 0)
            );
    end component;

    component therm2bin
        generic(N : integer := 1);
        port(
                Din    : in  std_logic_vector((2**N-2) downto 0);
                Dout   : out std_logic_vector(N-1 downto 0)
            );
    end component;

    signal clk_ser_s : std_logic;
    signal clk_ser_90_s : std_logic;

    signal par_s : std_logic_vector(7 downto 0);
begin
    cmp_comparator : comparator
    generic map(N => N)
    port map(
                threshold_i => threshold_i,
                analog_i    => analog_i,
                cmp_o       => cmp_s
            );

    -- Sample
    process(clk_ser_s)
    begin
        if rising_edge(clk_ser_s)
        then
            therm_s <= cmp_s;
        end if;
    end process;

    cmp_therm2bin : therm2bin
    generic map(N => N)
    port map(
                Din => therm_s,
                Dout => digital_o
            );

end architecture arch;
