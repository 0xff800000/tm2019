----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:34:06 01/26/2017 
-- Design Name: 
-- Module Name:    UartFrontEnd - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity UartFrontEnd is
   generic (
      REG_ADDR_BITS_G : integer := 32;
      REG_DATA_BITS_G : integer := 32;
      CLOCK_RATE_G : integer := 80000000;
      BAUD_RATE_G  : integer := 115200
      --GATE_DELAY_G : time    := 1 ns   
   );
   port (
      clk         : in  std_logic;
      sRst        : in  std_logic;
      uartRx      : in  std_logic;
      uartTx      : out std_logic;
      regAddr     : out std_logic_vector(REG_ADDR_BITS_G-1 downto 0);
      regWrData   : out std_logic_vector(REG_DATA_BITS_G-1 downto 0);
      regRdData   : in  std_logic_vector(REG_DATA_BITS_G-1 downto 0);
      regReq      : out std_logic;
      regOp       : out std_logic;
      regAck      : in  std_logic
   );
end UartFrontEnd;

architecture Behavioral of UartFrontEnd is

   signal txByte      : std_logic_vector(7 downto 0) := (others => '0');
   signal txByteValid : std_logic := '0';
   signal txByteReady : std_logic;

   signal rxByte      : std_logic_vector(7 downto 0) := (others => '0');
   signal rxByteValid : std_logic := '0';

--   signal crcByte     : std_logic_vector(7 downto 0);
--   signal crcRst      : std_logic;
--   signal crcOrSysRst : std_logic;
--   signal crcEn       : std_logic;
--   signal crc         : std_logic_vector(7 downto 0);   

begin

   -- This coordinates the UART communications
   U_UartTop : entity work.UartTop
      generic map (
         CLOCK_RATE_G => CLOCK_RATE_G,
         BAUD_RATE_G  => BAUD_RATE_G --,
         --GATE_DELAY_G => GATE_DELAY_G
      )
      port map (
         clk         => clk,
         sRst        => sRst,
         rxByte      => rxByte,
         rxByteValid => rxByteValid,
         txByte      => txByte,
         txByteValid => txByteValid,
         txByteReady => txByteReady,
         uartRx      => uartRx,
         uartTx      => uartTx
      );

   -- Interprets data coming in from the UART
   U_CommandInterpreter : entity work.CommandInterpreter
      generic map (
         REG_ADDR_BITS_G => REG_ADDR_BITS_G,
         REG_DATA_BITS_G => REG_DATA_BITS_G,
         TIMEOUT_G       => 40000 --, -- 500 ms @ 80 MHz clock
         --GATE_DELAY_G    => GATE_DELAY_G
      )
      port map (
         -- User clock and reset
         usrClk      => clk, -- in  sl;
         usrRst      => sRst, -- in  sl := '0';
         -- Incoming data
         rxData      => rxByte, -- in  slv(7 downto 0);
         rxDataValid => rxByteValid, -- in  sl;
         -- CRC interface
         --crcByte     => crcByte, -- out slv(7 downto 0);
         --crcRst      => crcRst, -- out sl;
         --crcEn       => crcEn, -- out sl;
         --crc         => crc, -- in  slv(7 downto 0);
         -- Outgoing response
         txData      => txByte, -- out slv(7 downto 0);
         txDataValid => txByteValid, -- out sl;
         txDataReady => txByteReady, -- in  sl;
         -- Register interfaces
         regAddr     => regAddr, -- out slv(REG_ADDR_BITS_G-1 downto 0);
         regWrData   => regWrData, -- out slv(REG_DATA_BITS_G-1 downto 0);
         regRdData   => regRdData, -- in  slv(REG_DATA_BITS_G-1 downto 0);
         regReq      => regReq, -- out sl;
         regOp       => regOp, -- out sl;
         regAck      => regAck -- in  sl
      );
   -- Auxiliary CRC calculator
--   crcOrSysRst <= crcRst or sRst;
--   U_Crc8 : entity work.Crc8
--      generic map (
--         CRC_INIT_G   => x"00",
--         GATE_DELAY_G => GATE_DELAY_G
--      )
--      port map (
--         data_in => crcByte,
--         crc_en  => crcEn,
--         rst     => crcOrSysRst,
--         clk     => clk,
--         crc_out => crc
--      );


end Behavioral;

