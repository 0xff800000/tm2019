library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;

LIBRARY work;
use work.UtilityPkg.all;

entity registers is
    generic(
               DATA_WIDTH : integer := 8;
               ADDR_WIDTH : integer := 8;
               TOTAL_SIZE : integer := 8
           );
    port(
            clk_i        :  in  std_logic;
            done_o       :  out std_logic;
            we_i         :  in  std_logic;
            op_i         :  in  std_logic;
            write_addr_i :  in  std_logic_vector (ADDR_WIDTH-1 downto 0);
            write_data_i :  in  std_logic_vector (DATA_WIDTH-1 downto 0);
            read_addr_i  :  in  std_logic_vector (ADDR_WIDTH-1 downto 0);
            read_data_o  :  out std_logic_vector (DATA_WIDTH-1 downto 0)
        );
end registers;

architecture behavioral of registers is

    type std_logic_matrix is array (integer range <>) of std_logic_vector(DATA_WIDTH-1 downto 0);

    signal regs      : std_logic_matrix(TOTAL_SIZE-1 downto 0);
    signal rd_done_s : std_logic;
    signal wr_done_s : std_logic;

begin

    -- Read data process
    process(clk_i) 
    begin
        if falling_edge(clk_i) then
            if we_i = '1' and op_i = '0' then	
                read_data_o <= regs(to_integer(unsigned(read_addr_i)));
                wr_done_s <= '1';
            else
                wr_done_s <= '0';
            end if;			
        end if;
    end process;

    -- Write data process
    process(clk_i, we_i) 
    begin
        if rising_edge(clk_i) then
            if we_i = '1' and op_i = '1' then	
                regs(to_integer(unsigned(write_addr_i))) <= write_data_i;
                rd_done_s <= '1';
            else
                rd_done_s <= '0';
            end if;			
        end if;
    end process;

    -- Done
    done_o <= rd_done_s or wr_done_s;

end behavioral;
