library ieee;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

package UtilityPkg is

    alias sl is std_logic;
    alias slv is std_logic_vector;
    --type std_logic_matrix is array (integer range <>) of std_logic_vector; -- Not supported by Vivado :(
    type std_logic_matrix is array (integer range <>) of std_logic_vector(31 downto 0);
    type std_logic_deser is array (integer range <>) of std_logic_vector(7 downto 0);
    type region_select is array (6 downto 0) of integer;

end package UtilityPkg;

package body UtilityPkg is
    end package body;
