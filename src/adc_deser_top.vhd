library ieee;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

library work;
use work.UtilityPkg.all;

entity adc_deser_top is
    generic(
               N : integer := 1;
               REGION_SELECT : region_select
           );
    port(
            rst_i       : in  std_logic;
            threshold_i : in  std_logic_vector((2**N-2) downto 0);
            analog_i    : in  std_logic_vector((2**N-2) downto 0);
            digital_o   : out std_logic_deser(N-1 downto 0);
            clk_i       : in  std_logic_vector(1 downto 0);
            --clk_div_o   : out std_logic;
            clk_div_i   : in  std_logic_vector(1 downto 0);
            clk_we_i    : in  std_logic;
            we_i        : out std_logic
        );
end adc_deser_top;

architecture arch of adc_deser_top is
    signal cmp_s   : std_logic_vector((2**N-2) downto 0);

    signal par_s   : std_logic_deser((2**N-2) downto 0);

    -- Index 1 : sample, Index 2 therm
    type THERM_t is array (7 downto 0) of std_logic_vector((2**N-2) downto 0);
    signal therm_s : THERM_t;
    --signal therm_s : std_logic_deser((2**N-2) downto 0);

    -- Index 1 : bit sample, Index 2 digital
    type DOUT_t is array (7 downto 0) of std_logic_vector(N-1 downto 0);
    signal Dout_s  : DOUT_t;
    --signal Dout_s  : std_logic_deser((2**N-2) downto 0);
begin
    cmp_comparator : entity work.comparator
    generic map(N => N)
    port map(
                threshold_i => threshold_i,
                analog_i    => analog_i,
                cmp_o       => cmp_s
            );

    inst_deser : entity work.deserializer_frontend
    generic map(
                   N => N,
                   REGION_SELECT => region_select
               )

    port map(
            rst_i     => rst_i,
            en_i      => '1',
            ser_i     => cmp_s,
            clk_ser_i => clk_i,
            clk_we_i  => clk_we_i,
            we_i      => we_i,
            --clk_div_o => clk_div_o,
            clk_div_i => clk_div_i,
            par_o     => par_s
        );

    FOR1 : for sample in 0 to 7 generate
        -- Instanciate therm2bin
        cmp_therm2bin : entity work.therm2bin_bec
        generic map(N => N)
        port map(
                    Din => therm_s(sample),
                    Dout => Dout_s(sample)
                );

        -- Transpose par_s on therm_s
        FOR2 : for nbit in (2**N-2) downto 0 generate
            therm_s(sample)(nbit) <= par_s(nbit)(sample);
        end generate FOR2;

        -- Retranspose back on digital_o
        FOR3 : for nbit in N-1 downto 0 generate
            digital_o(nbit)(sample) <= Dout_s(sample)(nbit); 
        end generate FOR3;
    end generate FOR1;



end architecture arch;
