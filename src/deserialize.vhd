library ieee;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

type std_logic_matrix is array (integer range <>) of std_logic_vector(7 downto 0);

entity deserializer is
    generic(N : integer := 1);
    port(
            cmp_i : in  std_logic_vector((2**N-2) downto 0);
            digital_o : out std_logic_matrix(N-1 downto 0);
            rst_i : in std_logic;
            clk_ser_i : in std_logic;
            clk_ser_90_i : in std_logic
        );
end deserializer;

architecture arch of deserializer is
begin

    FOR1 : for i in cmp_i'range generate
        ISERDESE2_inst : ISERDESE2 
        generic map(
                       DATA_RATE => "DDR",         -- DDR, SDR
                       DATA_WIDTH=>8,             -- Parallel data width (2-8,10,14)
                       DYN_CLKDIV_INV_EN=>"FALSE", -- Enable DYNCLKDIVINVSEL inversion (FALSE,TRUE)
                       DYN_CLK_INV_EN=>"FALSE",    -- Enable DYNCLKINVSEL inversion (FALSE,TRUE)
                                                   -- INIT_Q1-INIT_Q4 : Initial value on the Q outputs (0/1)
                       INIT_Q1=>'0',
                       INIT_Q2=>'0',
                       INIT_Q3=>'0',
                       INIT_Q4=>'0',
                       INTERFACE_TYPE=>"MEMORY", -- MEMORY, MEMORY_DDR3, MEMORY_QDR, NETWORKING, OVERSAMPLE
                       IOBDELAY=>"NONE",         -- NONE, BOTH, IBUF,
                       IFDNUM_CE=>2,             -- Number of clock enables (1, 2)
                       OFB_USED=>"FALSE",        -- Select OFB path(FALSE,TRUE)
                       SERDES_MODE=>"MASTER",    -- MASTER,SLAVE
                                                 -- SRVAL_Q1-SRVAL_Q4 : Q output values when SR isused (0/1)
                       SRVAL_Q1=>'0',
                       SRVAL_Q2=>'0',
                       SRVAL_Q3=>'0',
                       SRVAL_Q4=>'0'
                   )
        port map(
    --                O=>O, -- 1-bit output : Combinatorial output
                    -- Q1-Q8 : 1-bit (each) output :Registered data outputs
                    Q1=>digital_o[i][0],
                    Q2=>digital_o[i][1],
                    Q3=>digital_o[i][2],
                    Q4=>digital_o[i][3],
                    Q5=>digital_o[i][4],
                    Q6=>digital_o[i][5],
                    Q7=>digital_o[i][6],
                    Q8=>digital_o[i][7],
                    -- SHIFTOUT1-SHIFTOUT2 : 1-bit (each) output: Data width expansion output ports
    --                SHIFTOUT1=>SHIFTOUT1,
    --                SHIFTOUT2=>SHIFTOUT2,
                    BITSLIP=>'0', -- 1-bit input : The BITSLIP pin performs a Bitslip operation synchronous to
                                      -- CLKDIV when asserted (active High). Subsequently, the data seen on the
                                      -- Q1 to Q8 output ports will shift, as in a barrel-shifter operation, one
                                      -- position every time Bitslip is invoked (DDR operation is different from
                                      -- SDR).
                                      -- CE1, CE2 : 1-bit (each) input : Data register clock enable inputs
                    CE1=>'1',
                    CE2=>'1',
                    CLKDIVP=>'0',  -- 1-bit input : TBD
                                       -- Clocks : 1-bit (each) input : ISERDESE2 clock input ports
                    CLK=>clk_iserdese,          -- 1-bit input : High-speed clock
    --                CLKB=>CLKB,        -- 1-bit input : High-speed secondary clock
                    CLKDIV=>clk_dbg,    -- 1-bit input : Divided clock
                    OCLK=>OCLK,        -- 1-bit input : High speed output clock used when INTERFACE_TYPE="MEMORY"
                                       -- Dynamic Clock Inversions : 1-bit (each) input : Dynamic clock inversion pins to switch clock polarity
                    OCLKB=>OCLKB,               -- 1-bit input : High speed negative edge output clock

                    DYNCLKDIVSEL=>'0', -- 1-bit input : Dynamic CLKDIV inversion
                    DYNCLKSEL=>'0',       -- 1-bit input : Dynamic CLK/CLKB inversion
                                                -- Input Data : 1-bit (each) input : ISERDESE2 data input ports
                    D=>cmp_i,                       -- 1-bit input : Data input
                    DDLY=>'0',                 -- 1-bit input : Serial data from IDELAYE2
                    OFB=>'0',                   -- 1-bit input : Data feedback from OSERDESE2
                    RST=>rst_i,                   -- 1-bit input : Active high asynchronous reset
                                                -- SHIFTIN1-SHIFTIN2 : 1-bit (each) input : Data width expansion input ports
                    SHIFTIN1=>SHIFTIN1,
                    SHIFTIN2=>SHIFTIN2
                );
    end generate FOR1;
end architecture arch;
