library ieee;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

entity comparator is
    generic(N : integer := 1);
    port(
            threshold_i : in  std_logic_vector((2**N-2) downto 0);
            analog_i    : in  std_logic_vector((2**N-2) downto 0);
            cmp_o       : out std_logic_vector((2**N-2) downto 0)
);
end comparator;

architecture arch of comparator is
begin
    FOR1 : for i in analog_i'range generate
        IBUFDS_inst : IBUFDS
        generic map(
                       DIFF_TERM    => FALSE, -- Differential Termination
                       IBUF_LOW_PWR => FALSE, -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
                       IOSTANDARD   => "LVDS_25")
        port map(
                    O  => cmp_o(i),      -- Buffer output
                    I  => analog_i(i),   -- Diff_p buffer input (connect directly to top-level port)
                    IB => threshold_i(i) -- Diff_n buffer input (connect directly to top-level port)
                );
    end generate FOR1;
end architecture arch;
