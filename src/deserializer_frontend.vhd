library ieee;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

library work;
use work.UtilityPkg.all;

use ieee.std_logic_unsigned.all;

entity deserializer_frontend is
    generic(
               N : integer := 1;
               REGION_SELECT : region_select
           );
    port(
            rst_i     : in  std_logic;
            en_i      : in  std_logic;
            ser_i     : in  std_logic_vector((2**N-2) downto 0);
            clk_ser_i : in  std_logic_vector(1 downto 0);
            clk_we_i  : in  std_logic;
            we_i      : out std_logic;
            clk_div_i : in  std_logic_vector(1 downto 0);
            --clk_div_o : out std_logic;
            par_o     : out std_logic_deser((2**N-2) downto 0)
        );
end deserializer_frontend;

architecture arch of deserializer_frontend is
    signal clk_ser_s     : std_logic;
    signal clk_div_s     : std_logic;
    signal clk_ser_p_s   : std_logic;
    signal clk_ser_n_s   : std_logic;

    signal not_clk_ser_i : std_logic;

    signal we_count_s    : std_logic_vector(1 downto 0);
    signal clk_cout_s    : std_logic;
    --signal we_count_s    : std_logic_vector(3 downto 0);
begin
    -- Map each channel
    FOR1 : for i in (2**N-2) downto 0 generate
        inst_deser : entity work.single_deserializer
        port map(
                    rst_i       => rst_i,
                    en_i        => en_i,
                    ser_i       => ser_i(i),
                    clk_ser_i   => clk_ser_i(region_select(i)),
                    clk_div_i   => clk_div_i(region_select(i)),
                    par_o       => par_o(i)
                );
    end generate FOR1;
--
--    -- Write enable should stay high for one clk_ser_p_s period
--    process(rst_i,clk_we_i)
--    begin
--        if rst_i = '1' then
--            we_count_s <= (others => '0');
--        elsif rising_edge(clk_we_i) then
--            we_count_s <= we_count_s(we_count_s'high-1 downto 0) & clk_div_i(region_select(0));
--        end if;
--    end process;
--
--    we_i <= '1' when we_count_s(we_count_s'high) = '0' and we_count_s(we_count_s'high-1) = '1' else '0';
    process(clk_div_i)
    begin
        if rst_i = '1' then
            clk_cout_s <= '0';
        elsif rising_edge(clk_div_i(region_select(0))) then
            clk_cout_s <= '1';
        end if;
    end process;

    we_i <= clk_cout_s;

end Architecture arch;
