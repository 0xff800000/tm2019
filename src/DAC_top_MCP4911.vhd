library ieee;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

library work;
use work.UtilityPkg.all;

entity dactop_MCP4911 is
    generic(
               N          : integer := 3;
               ADDR_WIDTH : integer := 32;
               DATA_WIDTH : integer := 32;
               BASE_ADDR  : integer := 16#10#
           );
    port(
            -- SPI
            clk_i   :  in    std_logic;
            sck_o   :  out   std_logic;
            cs_o    :  out   std_logic_vector((2**N-2) downto 0);
            mosi_o  :  out   std_logic;
            -- Registers
            done_o       :  out std_logic;
            we_i         :  in  std_logic;
            op_i         :  in  std_logic;
            write_addr_i :  in  std_logic_vector(ADDR_WIDTH-1 downto 0);
            write_data_i :  in  std_logic_vector(DATA_WIDTH-1 downto 0);
            read_addr_i  :  in  std_logic_vector(ADDR_WIDTH-1 downto 0);
            read_data_o  :  out std_logic_vector(DATA_WIDTH-1 downto 0)

        );
end dactop_MCP4911;

architecture arch of dactop_MCP4911 is

    constant TOTAL_SIZE : integer := 3;
    signal regAck_s    : std_logic;
    signal regReq_s    : std_logic;
    signal regOp_s     : std_logic;
    signal regAddr_s   : std_logic_vector(ADDR_WIDTH-1 downto 0);
    signal regWrData_s : std_logic_vector(DATA_WIDTH-1 downto 0);
    signal regRdData_s : std_logic_vector(DATA_WIDTH-1 downto 0);
    signal cs_s     : std_logic;
    signal send_s     : std_logic;
    signal regs_s     : std_logic_matrix(TOTAL_SIZE-1 downto 0);

    signal done_s     : std_logic;
begin
    inst_DAC_spi : entity work.DAC_MCP4911
    port map(
                CLK   => clk_i,
                SCK   => sck_o,
                CS    => cs_s,
                MOSI  => mosi_o,
                VALUE => regs_s(2)(11 downto 0),
                SEND  => send_s
            );

    inst_registers_r : entity work.registers_rw
    generic map(
                   DIRECTION   => "W",
                   BASE_OFFSET => 16#10#,
                   DATA_WIDTH  => DATA_WIDTH,
                   ADDR_WIDTH  => ADDR_WIDTH,
                   TOTAL_SIZE  => TOTAL_SIZE
               )
    port map(
                clk_i        => clk_i,
                done_o       => done_s,
                we_i         => we_i,
                op_i         => op_i,
                write_addr_i => write_addr_i,
                write_data_i => write_data_i,
                read_addr_i  => read_addr_i,
                read_data_o  => read_data_o,

                regs_o       => regs_s
            );

    -- Chip select
    process(cs_s, regs_s(1))
    begin
        cs_o <= (others => '1');
        if to_integer(unsigned(regs_s(1))) <= cs_o'high then
            cs_o(to_integer(unsigned(regs_s(1)))) <= cs_s;
        end if;
    end process;
    
    done_o <= done_s;

    -- Send data when anything is written to BASE_ADDR
    send_s <= '1' when 
              to_integer(unsigned(read_addr_i)) = BASE_ADDR and
              regReq_s = '1'
                                                   else '0';
end architecture arch;
