library ieee;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

entity clock_gen is
    port(
            rst_i        : in  std_logic;
            clk_in_i      : in  std_logic;
            clk_ser_o      : out  std_logic;
            clk_ser_90_o : out  std_logic;
            locked       : out  std_logic
        );
end clock_gen;

architecture arch of clock_gen is
    signal clkfbout_clk_wiz_0 : std_logic;
    signal clkfbout_buf_clk_wiz_0 : std_logic;

    -- Unused
    signal CLKOUT0B_unused : std_logic;
    signal CLKOUT1B_unused : std_logic;
    signal CLKOUT2_unused : std_logic;
    signal CLKOUT2B_unused : std_logic;
    signal CLKOUT3_unused : std_logic;
    signal CLKOUT3B_unused : std_logic;
    signal CLKOUT4_unused : std_logic;
    signal CLKOUT5_unused : std_logic;
    signal CLKOUT6_unused : std_logic;
    signal DO_unused : std_logic_vector(15 downto 0);
    signal DRDY_unused : std_logic;
    signal PSDONE_unused : std_logic;
    signal clkfboutb_unused : std_logic;
    signal CLKFBSTOPPED_unused : std_logic;
    signal CLKINSTOPPED_unused : std_logic;

begin
    -- MMCME2_ADV : Advanced Mixed Mode Clock Manager
    -- 7 Series
    -- Xilinx HDL Libraries Guide, version2012.2

    MMCME2_ADV_inst : MMCME2_ADV
    generic map(
                   BANDWIDTH       => "OPTIMIZED", -- Jitter programming (OPTIMIZED, HIGH, LOW)
                   CLKFBOUT_MULT_F => 62.500,   -- Multiply value for all CLKOUT (2.000-64.000).
                   CLKFBOUT_PHASE  => 0.0,    -- Phase offset in degrees of CLKFB(-360.000-360.000).
                                              -- CLKIN_PERIOD : Input clock period in n stops resolution (i.e. 33.333 is 30MHz).
                   CLKIN1_PERIOD => 83.333,
                   CLKIN2_PERIOD => 0.0,
                   -- CLKOUT0_DIVIDE-CLKOUT6_DIVIDE : Divide amount for CLKOUT (1-128)
                   CLKOUT1_DIVIDE   => 8,
                   CLKOUT2_DIVIDE   => 1,
                   CLKOUT3_DIVIDE   => 1,
                   CLKOUT4_DIVIDE   => 1,
                   CLKOUT5_DIVIDE   => 1,
                   CLKOUT6_DIVIDE   => 1,
                   CLKOUT0_DIVIDE_F => 8.0,
                   -- DivideamountforCLKOUT0(1.000-128.000).
                   -- CLKOUT0_DUTY_CYCLE-CLKOUT6_DUTY_CYCLE : Duty cycle for CLKOUT outputs (0.01-0.99).
                   CLKOUT0_DUTY_CYCLE => 0.5,
                   CLKOUT1_DUTY_CYCLE => 0.5,
                   CLKOUT2_DUTY_CYCLE => 0.5,
                   CLKOUT3_DUTY_CYCLE => 0.5,
                   CLKOUT4_DUTY_CYCLE => 0.5,
                   CLKOUT5_DUTY_CYCLE => 0.5,
                   CLKOUT6_DUTY_CYCLE => 0.5,
                   -- CLKOUT0_PHASE-CLKOUT6_PHASE : Phase offset for CLKOUT outputs (-360.000-360.000).
                   CLKOUT0_PHASE   => 0.0,
                   CLKOUT1_PHASE   => 90.0,
                   CLKOUT2_PHASE   => 0.0,
                   CLKOUT3_PHASE   => 0.0,
                   CLKOUT4_PHASE   => 0.0,
                   CLKOUT5_PHASE   => 0.0,
                   CLKOUT6_PHASE   => 0.0,
                   CLKOUT4_CASCADE => FALSE,
                   -- Cascade CLKOUT4 counter with CLKOUT6 (FALSE, TRUE)
                   COMPENSATION  => "ZHOLD", -- ZHOLD, BUF_IN, EXTERNAL, INTERNAL
                   DIVCLK_DIVIDE => (1),      -- Masterdivisionvalue(1-106)
                                              -- REF_JITTER : Reference input jitter in UI (0.000-0.999).
                   REF_JITTER1  => 0.0,
                   REF_JITTER2  => 0.0,
                   STARTUP_WAIT => FALSE,     -- Delays DONE until MMCM is locked (FALSE, TRUE)
                                              -- Spread Spectrum : Spread Spectrum Attributes
                   SS_EN                 => "FALSE",               -- Enables spread spectrum (FALSE, TRUE)
                   SS_MODE               => "CENTER_HIGH",       -- CENTER_HIGH, CENTER_LOW, DOWN_HIGH,
                                                                 --                   DOWN_LOWSS_MOD_PERIOD => 10000, -- Spread spectrum modulation period (ns)(VALUES)
                                                                 -- USE_FINE_PS : Fine phase shift enable (TRUE/FALSE)
                   CLKFBOUT_USE_FINE_PS => FALSE,
                   CLKOUT0_USE_FINE_PS  => FALSE,
                   CLKOUT1_USE_FINE_PS  => FALSE,
                   CLKOUT2_USE_FINE_PS  => FALSE,
                   CLKOUT3_USE_FINE_PS  => FALSE,
                   CLKOUT4_USE_FINE_PS  => FALSE,
                   CLKOUT5_USE_FINE_PS  => FALSE,
                   CLKOUT6_USE_FINE_PS  => FALSE
               )
    port map(
                -- Clock Outputs : 1-bit (each) output : User configurable clock outputs
                CLKOUT0  => clk_ser_o,  -- 1-bit output : CLKOUT0
                CLKOUT0B => CLKOUT0B_unused, -- 1-bit output : Inverted CLKOUT0
                CLKOUT1  => clk_ser_90_o,  -- 1-bit output : CLKOUT1
                CLKOUT1B => CLKOUT1B_unused, -- 1-bit output : Inverted CLKOUT1
                CLKOUT2  => CLKOUT2_unused,  -- 1-bit output : CLKOUT2
                CLKOUT2B => CLKOUT2B_unused, -- 1-bit output : Inverted CLKOUT2
                CLKOUT3  => CLKOUT3_unused,  -- 1-bit output : CLKOUT3
                CLKOUT3B => CLKOUT3B_unused, -- 1-bit output : Inverted CLKOUT3
                CLKOUT4  => CLKOUT4_unused,  -- 1-bit output : CLKOUT4
                CLKOUT5  => CLKOUT5_unused,  -- 1-bit output : CLKOUT5
                CLKOUT6  => CLKOUT6_unused,  -- 1-bit output : CLKOUT6
                                             -- DRP Ports : 16-bit (each) output : Dynamic reconfiguration ports
                DO   => DO_unused,   -- 16-bit output : DRP data
                DRDY => DRDY_unused, -- 1-bit output : DRP readb
                                     -- Dynamic Phase Shift Ports : 1-bit (each) output : Ports used for dynamic phase shifting of the outputs
                PSDONE => PSDONE_unused,             -- 1-bit output : Phase shift done
                                                     -- Feedback Clocks : 1-bit (each) output :Clock feedback ports
                CLKFBOUT  => clkfbout_clk_wiz_0,        -- 1-bit output : Feedback clock
                CLKFBOUTB => clkfboutb_unused,       -- 1-bit output : Inverted CLKFBOUT
                                                     -- Status Ports : 1-bit (each) output : MMCM status ports
                CLKFBSTOPPED => CLKFBSTOPPED_unused, -- 1-bit output : Feedback clock stopped
                CLKINSTOPPED => CLKINSTOPPED_unused, -- 1-bit output : Input clock stopped
                LOCKED       => locked, -- 1-bit output : LOCK           
                                        -- ClockInputs:1-bit(each)input:Clockinputs
                CLKIN1 => clk_in_i,       -- 1-bit input : Primary clock
                CLKIN2 => '0',       -- 1-bit input : Secondary clock 
                                     -- ControlPorts : 1-bit (each) input : MMCM control ports
                CLKINSEL => '1', -- 1-bit input : Clock select, High = CLKIN1 Low = CLKIN2
                PWRDWN   => '0',   -- 1-bit input : Power-down
                RST      => rst_i,      -- 1-bit input : Reset
                                        -- DRP Ports : 7-bit (each) input : Dynamic reconfiguration ports
                DADDR => b"0000000",         -- 7-bit input : DRP address
                DCLK  => '0',          -- 1-bit input : DRP clock
                DEN   => '0',           -- 1-bit input : DRP enable
                DI    => x"0000",            -- 16-bit input : DRP data
                DWE   => '0',           -- 1-bit input : DRP write enable
                                        -- Dynamic Phase Shift Ports : 1-bit (each) input : Ports used for dynamic phase shifting of the outputs
                PSCLK    => '0',      -- 1-bit input : Phase shift clock
                PSEN     => '0',       -- 1-bit input : Phase shift enable
                PSINCDEC => '0',   -- 1-bit input : Phase shift increment/decrement
                                   -- Feedback Clocks : 1-bit (each) input : Clock feedback ports
                CLKFBIN => clkfbout_buf_clk_wiz_0      -- 1-bit input : Feedback clock
            );


    -- Clock Monitor clock assigning
    ----------------------------------------
    -- Output buffering
    -------------------------------------
    BUFG_inst : BUFG
    port map(
                O => clkfbout_buf_clk_wiz_0,
                I => clkfbout_clk_wiz_0
            );

end architecture arch;
