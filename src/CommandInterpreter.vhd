---------------------------------------------------------------------------------
-- Title         : Command Interpreter
-- Project       : General Purpose Core
---------------------------------------------------------------------------------
-- File          : CommandInterpreter.vhd
-- Author        : Kurtis Nishimura
---------------------------------------------------------------------------------
-- Description:
-- Packet parser modified specifically for Paradromics P1 camera.
---------------------------------------------------------------------------------

LIBRARY ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

LIBRARY work;
use work.UtilityPkg.all;


entity CommandInterpreter is 
   generic (
      REG_ADDR_BITS_G : integer := 32;
      REG_DATA_BITS_G : integer := 32;
      --TIMEOUT_G       : integer := 40000 --; -- 500 ms @ 80 MHz clock
      TIMEOUT_G       : integer := 8192 --; -- ~500 ms @ 20 MHz clock
      --GATE_DELAY_G    : time    := 1 ns
   );
   port ( 
      -- User clock and reset
      usrClk      : in  std_logic;
      usrRst      : in  std_logic := '0';
      -- Incoming data
      rxData      : in  std_logic_vector(7 downto 0);
      rxDataValid : in  std_logic;
      -- CRC interface
--      crcByte     : out std_logic_vector(7 downto 0);
--      crcRst      : out std_logic;
--      crcEn       : out std_logic;
--      crc         : in  std_logic_vector(7 downto 0);
      -- Outgoing response
      txData      : out std_logic_vector(7 downto 0);
      txDataValid : out std_logic;
      txDataReady : in  std_logic;
      -- Register interfaces
      regAddr     : out std_logic_vector(REG_ADDR_BITS_G-1 downto 0);
      regWrData   : out std_logic_vector(REG_DATA_BITS_G-1 downto 0);
      regRdData   : in  std_logic_vector(REG_DATA_BITS_G-1 downto 0);
      regReq      : out std_logic;
      regOp       : out std_logic;
      regAck      : in  std_logic
   ); 
end CommandInterpreter;

-- Define architecture
architecture rtl of CommandInterpreter is

    constant NB_BYTES_REG_ADDR : integer := REG_ADDR_BITS_G / 8;
    constant NB_BYTES_REG_DATA : integer := REG_DATA_BITS_G / 8;

   type StateType is (IDLE_S,CMD_S,ARG_S,ADDR_S,PLEN_S,RESV_S,HCRC_S,CHECK_CRC_S,
                      WRITE_PAYLOAD_S,DCHK_S,DO_WRITE_S,DO_READ_S,READ_RESP_S,
                      WRITE_RESP_S,NAK_RESP_S,DONE_S);
   
   type RegType is record
      state       : StateType;
      regAddr     : slv(REG_ADDR_BITS_G-1 downto 0);
      regWrData   : slv(REG_DATA_BITS_G-1 downto 0);
      regRdData   : slv(REG_DATA_BITS_G-1 downto 0);
      regReq      : sl;
      regOp       : sl;
--      err         : sl;
--      errType     : slv(15 downto 0);
      arg         : slv(31 downto 0);
--      adrCount         : slv(11 downto 0);
      byteCount   : slv(3 downto 0);
      --byteCount   : slv(31 downto 0);
--      crcReset    : sl;
--      crcEn       : sl;
--      crcByte     : slv(7 downto 0);
--      hcrc        : slv(7 downto 0);
--      pLen        : slv(15 downto 0);
--      dchk        : slv(15 downto 0);
      txData      : slv(7 downto 0);
      txDataValid : sl;
      timeoutCnt  : slv(12 downto 0);
   end record RegType;
   
   constant REG_INIT_C : RegType := (
      state       => IDLE_S,
      regAddr     => (others => '0'),
      regWrData   => (others => '0'),
      regRdData   => (others => '0'),
      regReq      => '0',
      regOp       => '0',
--      err         => '0',
--      errType     => (others => '0'),
      arg         => (others => '0'),
--      adrCount         => (others => '0'),
      byteCount   => (others => '0'),
--      crcReset    => '0',
--      crcEn       => '0',
--      crcByte     => (others => '0'),
--      hcrc        => (others => '0'),
--      pLen        => (others => '0'),
--      dchk        => (others => '0'),
      txData      => (others => '0'),
      txDataValid => '0',
      timeoutCnt  => (others => '0')
   );
   
   signal r   : RegType := REG_INIT_C;
   signal rin : RegType;

   -- ISE attributes to keep signals for debugging
   -- attribute keep : string;
   -- attribute keep of r : signal is "true";
   -- attribute keep of crcOut : signal is "true";      
   
   -- Vivado attributes to keep signals for debugging
   -- attribute dont_touch : string;
   -- attribute dont_touch of r : signal is "true";
   -- attribute dont_touch of crcOut : signal is "true";   
   
--   constant BYTE_STX_C      : slv(7 downto 0)  := x"02";
   constant BYTE_CMD_RD_C   : slv(7 downto 0)  := x"72";
   constant BYTE_CMD_WR_C   : slv(7 downto 0)  := x"77";
   constant BYTE_CMD_SS_C   : slv(7 downto 0)  := x"73";
--   constant BYTE_CMD_ACK_C  : slv(7 downto 0)  := x"06";
--   constant BYTE_CMD_NAK_C  : slv(7 downto 0)  := x"0F";
--   constant BYTE_RSV_C      : slv(7 downto 0)  := x"00";
--   constant NAK_BAD_HCRC_C  : slv(15 downto 0) := x"FFFF";
--   constant NAK_BAD_DCHK_C  : slv(15 downto 0) := x"FFFE";
--   constant NAK_FIFO_FULL_C : slv(15 downto 0) := x"FFFD";
--   constant NAK_UNKN_CMD_C  : slv(15 downto 0) := x"FFFC";
--   constant NAK_NBYTES_C    : slv(15 downto 0) := x"FFFB";
--   constant NAK_TIMEOUT_C   : slv(15 downto 0) := x"FFFA";
   
begin

   -- Outputs to ports
   txData      <= r.txData;
   txDataValid <= r.txDataValid;
   -- Register interfaces
   regAddr     <= r.regAddr;
   regWrData   <= r.regWrData;
   regReq      <= r.regReq;
   regOp       <= r.regOp;
   -- CRC interfaces
   --crcByte     <= r.crcByte;
   --crcRst      <= r.crcReset;
   --crcEn       <= r.crcEn;


   comb : process(r,rxData,rxDataValid,txDataReady,regRdData,regAck) is
      variable v : RegType;
   begin
      v := r;

      -- Resets for pulsed outputs
      v.regReq      := '0';
      v.txDataValid := '0';
      --v.crcEn       := '0';
      --v.crcReset    := '0';
      
      -- State machine 
      case(r.state) is 
         when IDLE_S =>
            v.byteCount  := (others => '0');
--            v.crcReset   := '1';
            v.timeoutCnt := (others => '0');
--            if rxDataValid = '1' and rxData = BYTE_STX_C then
--               v.err      := '0';
--               v.crcReset := '0';
--               v.crcByte  := rxData;
--               v.crcEn    := '1';
--               v.state    := CMD_S;
--            end if;
--         when CMD_S  =>
            if rxDataValid = '1' then
               case(rxData) is
                  when BYTE_CMD_WR_C =>
                     v.regOp := '1';
--                     v.adrCount := x"008";
                     v.state := ADDR_S;
                  when BYTE_CMD_RD_C =>
                     v.regOp := '0';
--                     v.adrCount := x"004";
                     v.state := ADDR_S;
                  when BYTE_CMD_SS_C =>
                     v.regOp := '0';
                     v.byteCount := (others => '0');
--                     v.adrCount := to_unsigned(8,v.adrCount'length);
                     v.state := DO_READ_S;
                  when others =>
--                     v.err := '1';
--                     v.errType := NAK_UNKN_CMD_C;
--                     v.adrCount := (others => '0');
               end case;
--               v.crcByte  := rxData;
--               v.crcEn := '1';
--               v.state := ARG_S;
            end if;
--         when ARG_S  =>
--            if rxDataValid = '1' then
--               v.crcByte := rxData;
--               v.crcEn := '1';
--               v.arg((conv_integer(r.byteCount)+1)*8-1 downto(conv_integer(r.byteCount))*8) := rxData;
--               v.byteCount := r.byteCount + 1;
--               if r.byteCount = 1 then
--                  v.byteCount := (others => '0');
--                  v.state     := ADDR_S;
--               end if;
--               if v.byteCount = v.adrCount then
--                  v.byteCount := (others => '0');
--                  v.state     := ADDR_S;
--               end if;
--            end if;
         when ADDR_S =>
            if rxDataValid = '1' then
--               v.crcByte := rxData;
--               v.crcEn := '1';
               --v.regAddr((conv_integer(r.byteCount)+1)*8-1 downto (conv_integer(r.byteCount))*8) := rxData;
               --v.regAddr((4-conv_integer(r.byteCount))*8-1 downto (3-conv_integer(r.byteCount))*8) := rxData;
               v.regAddr((NB_BYTES_REG_ADDR-conv_integer(r.byteCount))*8-1 downto ((NB_BYTES_REG_ADDR-1)-conv_integer(r.byteCount))*8) := rxData;
               v.byteCount := r.byteCount + 1;
               --if r.byteCount = 3 then
               if r.byteCount = NB_BYTES_REG_ADDR-1 then
                  v.byteCount := (others => '0');
--                  v.adrCount := (others => '0');
--                  v.state     := PLEN_S;
                  if r.regOp = '1' then
                     v.state := WRITE_PAYLOAD_S;
                  else
                     v.state := DO_READ_S;
                  end if;
               end if;
            end if;
--         when PLEN_S =>
--            if rxDataValid = '1' then
--               v.crcByte := rxData;
--               v.crcEn := '1';
--               v.pLen((conv_integer(r.byteCount)+1)*8-1 downto(conv_integer(r.byteCount))*8) := rxData;
--               v.byteCount := r.byteCount + 1;
--               if r.byteCount = 1 then
--                  v.byteCount := (others => '0');
--                  v.state     := RESV_S;
--               end if;
--            end if;
--         when RESV_S =>
--            if rxDataValid = '1' then
--               v.crcByte := rxData;
--               v.crcEn := '1';
--               v.state := HCRC_S;
--            end if;
--         when HCRC_S =>
--            if rxDataValid = '1' then
--               v.crcByte := rxData;
--               v.crcEn   := '1';
--               v.hcrc    := rxData;
--               v.state   := CHECK_CRC_S;
--            end if;
--         when CHECK_CRC_S =>
--            v.crcReset := '1';
--            -- CRC is still lagging by a cycle here, so
--            -- comparing to the HCRC byte instead of 0.
--            if crc /= r.hcrc then
--               v.err      := '1';
--               v.errType  := NAK_BAD_HCRC_C;
--               v.state    := NAK_RESP_S;
--            -- Writes are 32-bit atomic, expect payload and arg to both be 4 bytes
--            elsif r.regOp = '1' and (r.pLen /= 4 or r.arg /= 4) then
--               v.err      := '1';
--               v.errType  := NAK_NBYTES_C;
--               v.state    := NAK_RESP_S;
--            -- Read has no payload, but arg should be 4 bytes
--            elsif r.regOp = '0' and (r.pLen /= 0 or r.arg /= 4) then
--               v.err      := '1';
--               v.errType  := NAK_NBYTES_C;
--               v.state    := NAK_RESP_S;
--            -- Other errors that might have come up
--            elsif r.err = '1' then
--               v.state   := NAK_RESP_S;
--            -- Good command, read the rest and execute it
--            else
--               v.dchk := (others => '0');
--               -- Do a write
--               if r.regOp = '1' then
--                  v.state := WRITE_PAYLOAD_S;
--               -- Do a read
--               else
--                  v.state := DCHK_S;
--               end if;
--            end if;
         when WRITE_PAYLOAD_S =>
            if rxDataValid = '1' then
               --v.regWrData((conv_integer(r.byteCount)+1)*8-1 downto(conv_integer(r.byteCount))*8) := rxData;
               --v.regWrData((4-conv_integer(r.byteCount))*8-1 downto(3-conv_integer(r.byteCount))*8) := rxData;
               v.regWrData((NB_BYTES_REG_DATA-conv_integer(r.byteCount))*8-1 downto((NB_BYTES_REG_DATA-1)-conv_integer(r.byteCount))*8) := rxData;
               v.byteCount := r.byteCount + 1;
               if r.byteCount = NB_BYTES_REG_DATA-1 then
                  v.byteCount := (others => '0');
                  v.state     := DO_WRITE_S;
               end if;
--               if r.byteCount(0) = '0' then
--                  v.dchk := r.dchk + (x"00" & rxData);
--               else
--                  v.dchk := r.dchk + (rxData & x"00");
--               end if;
            end if;
--         when DCHK_S =>
--            if rxDataValid = '1' then
--               v.byteCount := r.byteCount + 1;
--               if r.byteCount = 1 then
--                  v.byteCount := (others => '0');
--                  if r.regOp = '1' then
--                     v.state := DO_WRITE_S;
--                  else
--                     v.state := DO_READ_S;
--                  end if;
--               end if;
--               if r.byteCount(0) = '0' then
--                  v.dchk := r.dchk + (x"00" & rxData);
--               else
--                  v.dchk := r.dchk + (rxData & x"00");
--               end if;
--            end if;
         when DO_WRITE_S =>
            v.timeoutCnt := r.timeoutCnt + 1;
            if r.timeoutCnt > TIMEOUT_G then
--               v.errType  := NAK_TIMEOUT_C;
--               v.state    := NAK_RESP_S;            
               v.state    := IDLE_S;            
            end if;
            v.regReq := '1';
            if regAck = '1' then
               v.regReq := '0';
--               if r.dchk /= x"0000" then
--                  v.crcReset := '1';
--                  v.err      := '1';
--                  v.errType  := NAK_BAD_DCHK_C;
--                  v.state    := NAK_RESP_S;
--               else
--                  v.crcReset := '1';
--                  v.state := WRITE_RESP_S;
--               end if;
               v.state := DONE_S;
            end if;
         when DO_READ_S =>
            v.timeoutCnt := r.timeoutCnt + 1;
            if r.timeoutCnt > TIMEOUT_G then
--               v.errType  := NAK_TIMEOUT_C;
--               v.state    := NAK_RESP_S;            
               v.state    := IDLE_S;            
            end if;
            v.regReq := '1';
            if regAck = '1' then
               v.regRdData := regRdData;
--               if r.dchk /= x"0000" then
--                  v.crcReset := '1';
--                  v.err      := '1';
--                  v.errType  := NAK_BAD_DCHK_C;
--                  v.state    := NAK_RESP_S;
--               else
--                  v.crcReset := '1';
--                  v.state    := READ_RESP_S;
--               end if;
               v.state := READ_RESP_S;
            end if;
         when READ_RESP_S =>
            v.txDataValid := '1';
--            v.dchk        := x"0000" - r.regRdData(31 downto 16) - r.regRdData(15 downto 0);
            if txDataReady = '1' then
--               v.crcEn     := '1';
               v.byteCount := r.byteCount + 1;
               if r.byteCount = 3 then
                  v.state := DONE_S;
               end if;
            end if;
--            case(conv_integer(r.byteCount)) is
----               when 0 => v.txData := BYTE_STX_C;
----               when 1 => v.txData := BYTE_CMD_ACK_C;
----               when 2 => v.txData := r.arg(7 downto 0);
----               when 3 => v.txData := r.arg(15 downto 8);
----               when 4 => v.txData := r.regAddr(7 downto 0);
----               when 5 => v.txData := r.regAddr(15 downto 8);
----               when 6 => v.txData := r.regAddr(23 downto 16);
----               when 7 => v.txData := r.regAddr(31 downto 24);
----               when 8 => v.txData := r.arg(7 downto 0);
----               when 9 => v.txData := r.arg(15 downto 8);
----               when 10 => v.txData := BYTE_RSV_C;
----               when 11 => v.txData := crc;
----               when 12 => v.txData := r.regRdData( 7 downto  0);
----               when 13 => v.txData := r.regRdData(15 downto  8);
----               when 14 => v.txData := r.regRdData(23 downto 16);
----               when 15 => v.txData := r.regRdData(31 downto 24);
--               when 3 => v.txData := r.regRdData( 7 downto  0);
--               when 2 => v.txData := r.regRdData(15 downto  8);
--               when 1 => v.txData := r.regRdData(23 downto 16);
--               when 0 => v.txData := r.regRdData(31 downto 24);
----               when 16 => v.txData := r.dchk(7 downto 0);
----               when 17 => v.txData := r.dchk(15 downto 8);
--               when others => v.txData := x"00";
--            end case;
            -- Generic register size
            if r.byteCount < NB_BYTES_REG_DATA then
                v.txData := r.regRdData(
                            (NB_BYTES_REG_DATA-conv_integer(r.byteCount))*8-1
                            downto
                            ((NB_BYTES_REG_DATA-1)-conv_integer(r.byteCount))*8
                        );
            else
                v.txData := x"00";
            end if;
--            v.crcByte := v.txData;
--         when WRITE_RESP_S =>
--            v.txDataValid := '1';
--            if txDataReady = '1' then
--               v.crcEn     := '1';
--               v.byteCount := r.byteCount + 1;
--               if r.byteCount = 13 then
--                  v.state := DONE_S;
--               end if;
--            end if;
--            case(conv_integer(r.byteCount)) is
--               when 0 => v.txData := BYTE_STX_C;
--               when 1 => v.txData := BYTE_CMD_ACK_C;
--               when 2 => v.txData := r.arg(7 downto 0);
--               when 3 => v.txData := r.arg(15 downto 8);
--               when 4 => v.txData := r.regAddr(7 downto 0);
--               when 5 => v.txData := r.regAddr(15 downto 8);
--               when 6 => v.txData := r.regAddr(23 downto 16);
--               when 7 => v.txData := r.regAddr(31 downto 24);
--               when 8 => v.txData := (others => '0');
--               when 9 => v.txData := (others => '0');
--               when 10 => v.txData := BYTE_RSV_C;
--               when 11 => v.txData := crc;
--               when 12 => v.txData := (others => '0');
--               when 13 => v.txData := (others => '0');
--               when others => v.txData := x"00";
--            end case;
--            v.crcByte := v.txData;
--         when NAK_RESP_S =>
--            v.txDataValid := '1';
--            v.dchk        := x"0000" - r.errType;
--            if txDataReady = '1' then
--               v.crcEn     := '1';
--               v.byteCount := r.byteCount + 1;
--               if r.byteCount = 15 then
--                  v.state := DONE_S;
--               end if;
--            end if;
--            case(conv_integer(r.byteCount)) is
--               when 0 => v.txData := BYTE_STX_C;
--               when 1 => v.txData := BYTE_CMD_NAK_C;
--               when 2 => v.txData := r.arg(7 downto 0);
--               when 3 => v.txData := r.arg(15 downto 8);
--               when 4 => v.txData := r.regAddr(7 downto 0);
--               when 5 => v.txData := r.regAddr(15 downto 8);
--               when 6 => v.txData := r.regAddr(23 downto 16);
--               when 7 => v.txData := r.regAddr(31 downto 24);
--               when 8 => v.txData := x"02";
--               when 9 => v.txData := x"00";
--               when 10 => v.txData := BYTE_RSV_C;
--               when 11 => v.txData := crc;
--               when 12 => v.txData := r.errType(7 downto 0);
--               when 13 => v.txData := r.errType(15 downto 8);
--               when 14 => v.txData := r.dchk(7 downto 0);
--               when 15 => v.txData := r.dchk(15 downto 8);
--               when others => v.txData := x"00";
--            end case;
--            v.crcByte := v.txData;
         when DONE_S =>
            if regAck = '0' then
               v.state := IDLE_S;
            end if;
         when others =>
            v.state := IDLE_S;
      end case;
      
      -- Assignment of combinatorial variable to signal
      rin <= v;

   end process;

   seq : process (usrClk) is
   begin
      if (rising_edge(usrClk)) then
         if usrRst = '1' then
            --r <= REG_INIT_C after GATE_DELAY_G;
            r <= REG_INIT_C;
         else
            --r <= rin after GATE_DELAY_G;
            r <= rin;
         end if;
      end if;
   end process seq;

end rtl;
