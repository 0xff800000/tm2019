library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;

entity DAC_AD5672 is
    port(
            CLK   :  in  std_logic;
            SCK   :  out std_logic;
            CS    :  out std_logic;
            MOSI  :  out std_logic;
            VALUE :  in  std_logic_vector(23 downto 0);
            SEND  :  in  std_logic
        );
end DAC_AD5672;

architecture behavioral of DAC_AD5672 is

    signal SENDING : std_logic := '0';
    signal reg : std_logic_vector (VALUE'range);

begin

    process(CLK, SEND)

        variable counter : integer range 0 to VALUE'high := 0;

    begin

        if rising_edge(CLK) then

            if SEND = '1' and SENDING = '0' then
                reg <= VALUE;
                counter := 0;
                CS <= '0';
                SENDING <= '1';

            elsif SENDING = '1' then
                reg <= reg(VALUE'high-1 downto 0) & '0';

                if counter = VALUE'high then
                    counter := 0;
                    CS <= '1';
                    SENDING <= '0';
                else
                    counter := counter + 1;
                end if;
            end if;
        end if;

    end process;

    SCK <= CLK AND SENDING;
    MOSI <= reg(VALUE'high);

end behavioral;
