library ieee;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity therm2bin is
    generic(N : integer := 1);
    port(
            Din    : in  std_logic_vector((2**N-2) downto 0);
            Dout   : out std_logic_vector(N-1 downto 0)
);
end therm2bin;

architecture arch of therm2bin is
begin
    -- Process version
    process(Din)
        variable sum_v : unsigned(Dout'range);
    begin
        sum_v := (others => '0');
        for i in Din'range loop
            if Din(i) = '1' then
                sum_v := sum_v + 1;
            end if;
        end loop;
        Dout <= std_logic_vector(sum_v);
    end process;

    -- Lookup-table version
--    FOR1 : for i in Din'range generate
--        Dout <= std_logic_vector(to_signed(i,Dout'length)) when Din = (Din'high downto i => '0') & (i downto 0 => '1');
--    end generate FOR1;
end architecture arch;
