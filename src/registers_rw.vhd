library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.UtilityPkg.all;

entity registers_rw is
    generic(
               DIRECTION   : string  := "W";
               BASE_OFFSET : integer := 0;
               DATA_WIDTH  : integer := 8;
               ADDR_WIDTH  : integer := 8;
               TOTAL_SIZE  : integer := 8
           );
    port(
            clk_i        :  in  std_logic;
            done_o       :  out std_logic;
            we_i         :  in  std_logic;
            op_i         :  in  std_logic;
            write_addr_i :  in  std_logic_vector(ADDR_WIDTH-1 downto 0);
            write_data_i :  in  std_logic_vector(DATA_WIDTH-1 downto 0);
            read_addr_i  :  in  std_logic_vector(ADDR_WIDTH-1 downto 0);
            read_data_o  :  out std_logic_vector(DATA_WIDTH-1 downto 0);

            regs_o  :  inout std_logic_matrix(TOTAL_SIZE-1 downto 0)
        );
end registers_rw;

architecture behavioral of registers_rw is


    signal regs : std_logic_matrix(TOTAL_SIZE-1 downto 0);
    signal rd_done_s : std_logic;
    signal wr_done_s : std_logic;

begin
    write_direction_g: if direction = "W" generate
    process(clk_i)
    begin
        if rising_edge(clk_i) then
        --if falling_edge(clk_i) then
        regs_o <= regs;
        end if;
    end process;
    end generate write_direction_g;

    read_direction_g : if direction = "R" generate
    process(clk_i)
    begin
        if rising_edge(clk_i) then
        --if falling_edge(clk_i) then
        regs <= regs_o;
        end if;
    end process;
    end generate read_direction_g;

    -- Read data process
    process(clk_i)
    begin
        if rising_edge(clk_i) then
        --if falling_edge(clk_i) then
            if we_i = '1' and op_i = '0' and
            to_integer(unsigned(read_addr_i)) >= BASE_OFFSET and
            to_integer(unsigned(read_addr_i)) <= BASE_OFFSET + (TOTAL_SIZE - 1)
                                                 then
                read_data_o <= regs(to_integer(unsigned(read_addr_i)) - BASE_OFFSET);
                wr_done_s <= '1';
            else
                read_data_o <= (others => 'Z');
                wr_done_s <= '0';
            end if;
        end if;
    end process;

    -- Write data process
    process(clk_i, we_i)
    begin
        if rising_edge(clk_i) then
        --if falling_edge(clk_i) then
            if we_i = '1' and op_i = '1' and
            to_integer(unsigned(write_addr_i)) >= BASE_OFFSET and
            to_integer(unsigned(write_addr_i)) <= BASE_OFFSET + (TOTAL_SIZE - 1)
                                                  then
                regs(to_integer(unsigned(write_addr_i)) - BASE_OFFSET) <= write_data_i;
                rd_done_s <= '1';
            else
                rd_done_s <= '0';
            end if;
        end if;
    end process;

    -- Done process
    process(clk_i, we_i)
    begin
        if rising_edge(clk_i) then
        --if falling_edge(clk_i) then
            if (
            to_integer(unsigned(write_addr_i)) >= BASE_OFFSET and
            to_integer(unsigned(write_addr_i)) <= BASE_OFFSET + (TOTAL_SIZE - 1)
                                                  )
            then
                if rd_done_s = '1' or wr_done_s = '1' then
                    done_o <= '1';
                else
                    done_o <= '0';
                end if;
            else
                done_o <= 'Z';
            end if;
        end if;
    end process;

end behavioral;
