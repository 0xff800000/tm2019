library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_signed.ALL;

LIBRARY work;
use work.UtilityPkg.all;

entity circular_buffer is
    generic(
               DATA_WIDTH : integer := 8;
               ADDR_WIDTH : integer := 8;
               TOTAL_SIZE : integer := 8;
               PIPELINE_SIZE : integer := 2
           );
    port(
            clk_i        :  in  std_logic;
            clk_slow_i    :  in  std_logic;
            rst_i        :  in  std_logic;
            empty_o      :  out std_logic;
            req_i        :  in  std_logic;
            op_i         :  in  std_logic;
            write_data_i :  in  std_logic_vector (DATA_WIDTH-1 downto 0);
            read_data_o  :  out std_logic_vector (DATA_WIDTH-1 downto 0)
        );
end circular_buffer;

architecture behavioral of circular_buffer is

    type std_logic_matrix is array (integer range <>) of std_logic_vector(DATA_WIDTH-1 downto 0);

    signal regs      : std_logic_matrix(TOTAL_SIZE-1 downto 0);
    signal rd_done_s : std_logic;
    signal wr_done_s : std_logic;
    signal req_s     : std_logic;
    signal empty_s   : std_logic := '1';
    signal empty_s_cdc : std_logic := '1';

    signal doutb_s  : std_logic_vector (DATA_WIDTH-1 downto 0);
    signal next_tail_s : std_logic_vector (ADDR_WIDTH-1 downto 0) := (others => '0');
    signal next_head_s : std_logic_vector (ADDR_WIDTH-1 downto 0) := (others => '0');
    signal overlap_s : std_logic_vector (ADDR_WIDTH-1 downto 0) := (others => '0');

    signal head_s : std_logic_vector (ADDR_WIDTH-1 downto 0) := (others => '0');
    signal tail_s : std_logic_vector (ADDR_WIDTH-1 downto 0) := (others => '0');

    signal tail_s_cdc : std_logic_vector (9 downto 0) := (others => '0');

    signal head_s_pl : std_logic_matrix (PIPELINE_SIZE downto 0);
    signal tail_s_pl : std_logic_matrix (PIPELINE_SIZE downto 0);

    signal reg_request : std_logic;
    signal req_rise : std_logic_vector(1 downto 0) := (others => '0');

begin

    bram_inst : entity work.blk_mem_gen_0
    PORT MAP (
                 clka => clk_i,
                 ena => op_i,
                 wea(0) => op_i,
                 addra => head_s_pl(PIPELINE_SIZE)(9 downto 0),
                 dina => write_data_i,
                 clkb => clk_slow_i,
                 enb => req_i,
                 addrb => tail_s_cdc,
                 doutb => doutb_s
             );

    empty_s <= '1' when head_s = tail_s else '0';
    empty_o <= empty_s;


    cdc_empty_inst : entity work.clkcrossing_buf
    generic map(
               NBITS => 1
           )
    port map(
                nrst => not rst_i,
                DA(0) => empty_s,
                QB(0) => empty_s_cdc,
                ClkA => clk_i,
                ClkB => clk_slow_i
            );

--    read_data_o <= doutb_s when empty_s = '0' else (others => '1');
    process(clk_slow_i)
    begin
        if rising_edge(clk_slow_i) then
            if empty_s_cdc = '0' then
                read_data_o <= doutb_s;
            else
                read_data_o <= (others => '1');
            end if;
        end if;
    end process;

    -- Reg request only once per regClk cycle
    reg_request <= '1' when req_rise(1) = '0' and req_rise(0) = '1' else '0';
    process(rst_i, clk_i) 
    begin
        if rst_i = '1' then
--            reg_request <= '0';
        elsif falling_edge(clk_i) then
--            if req_i = '1' and req_rise(1) = '0' then
--                reg_request <= '1';
--            else
--                reg_request <= '0';
--            end if;
            req_rise <= req_rise(0) & req_s;
        end if;
    end process;

    cdc_req_inst : entity work.clkcrossing_buf
    generic map(
               NBITS => 1
           )
    port map(
                nrst => not rst_i,
                DA(0) => req_i,
                QB(0) => req_s,
                ClkA => clk_slow_i,
                ClkB => clk_i
            );

    -- Read data process
    process(rst_i, clk_i) 
        variable next_tail : std_logic_vector(ADDR_WIDTH-1 downto 0);
        variable next_head : std_logic_vector(ADDR_WIDTH-1 downto 0);
    begin
        
        if rst_i = '1' then
            head_s <= (others => '0');
            tail_s <= (others => '0');
        elsif rising_edge(clk_i) then
            -- Read reg
            if reg_request = '1' and op_i = '0' then
                if empty_s = '0' then
                    -- Increment tail
                    next_tail := tail_s + 1;
                    --if next_tail > TOTAL_SIZE-1 then
                    if next_tail = TOTAL_SIZE then
                        next_tail := (others => '0');
                    end if;
                    tail_s <= next_tail;
                end if;
            -- Write reg
            elsif op_i = '1' then -- Sample every rising edge of clk
                --regs(to_integer(unsigned(head_s))) <= write_data_i;
                -- Increment head
                next_head := head_s + 1;
                --if next_head > TOTAL_SIZE-1 then
                if next_head = TOTAL_SIZE then
                    next_head := (others => '0');
                end if;
                -- Increment tail when overlap
                if next_head = tail_s then
                    next_tail := next_head + 1;
                    --if next_tail > TOTAL_SIZE-1 then
                    if next_tail = TOTAL_SIZE then
                        next_tail := (others => '0');
                    end if;
                    tail_s <= next_tail;
                end if;
                head_s <= next_head;
            end if;
        end if;
    end process;


    process(clk_i)
    begin
        if rst_i = '1' then
            for i in 1 to PIPELINE_SIZE loop
                head_s_pl(i) <= (others => '0');
                tail_s_pl(i) <= (others => '0');
            end loop;
        elsif rising_edge(clk_i) then
            head_s_pl(0) <= head_s;
            tail_s_pl(0) <= tail_s;
            for i in 0 to PIPELINE_SIZE-1 loop
                head_s_pl(i+1) <= head_s_pl(i);
                tail_s_pl(i+1) <= tail_s_pl(i);
            end loop;
        end if;
    end process;

    cdc_inst : entity work.clkcrossing_buf
    generic map(
               NBITS => 10
           )
    port map(
                nrst => not rst_i,
                DA => tail_s_pl(PIPELINE_SIZE)(9 downto 0),
                QB => tail_s_cdc,
                ClkA => clk_i,
                ClkB => clk_slow_i
            );

end behavioral;
