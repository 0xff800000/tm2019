library ieee;
use ieee.std_logic_1164.all;
use IEEE.math_real.all;
use ieee.std_logic_unsigned.all;

library work;
use work.UtilityPkg.all;

entity clock_divider is
    generic(
               FREQ_IN  : integer := 0;
               FREQ_OUT : integer := 0
           );
    port(
            clk_i : in  std_logic;
            clk_o : out std_logic
        );
end clock_divider;

architecture behavioral of clock_divider is

    -- 1/FREQ_IN*TCOUNT = 1/(2*FREQ_OUT)
    constant TCOUNT : integer := integer(ceil(real(FREQ_IN)/(2.0*real(FREQ_OUT))));
    signal counter_s : std_logic_vector(integer(ceil(log2(real(TCOUNT))))-1 downto 0) := (others => '0');
    signal clk_s : std_logic := '0';

begin
    clk_o <= clk_s;
    process(clk_i)
    begin
        if rising_edge(clk_i) then
            if counter_s = TCOUNT then
                clk_s <= not clk_s;
                counter_s <= (others => '0');
            else
                counter_s <= counter_s + 1;
            end if;
        end if;
    end process;

end behavioral;


