library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.UtilityPkg.all;

entity adc_frontend is
    generic(
               N             : integer := 3;      -- NOTE : N must be less than or equal to 4
               ADDR_WIDTH    : integer := 32;
               DATA_WIDTH    : integer := 32;
               BASE_OFFSET   : integer := 16#10#;
               BUFF_SIZE     : integer := 512;
               REGION_SELECT : region_select
           );
    port(
            rst_i         : in  std_logic;
            threshold_i   : in  std_logic_vector((2**N-2) downto 0);
            analog_i      : in  std_logic_vector((2**N-2) downto 0);
            clk_i         : in  std_logic_vector(1 downto 0);
            clk_div_i     : in  std_logic_vector(1 downto 0);
            stop_sample_i : in  std_logic;
            clk_we_i      : in  std_logic;
            -- Registers
            regClk    :  in  std_logic;
            regAck    :  out std_logic;
            regReq    :  in  std_logic;
            regOp     :  in  std_logic;
            regAddr   :  in  std_logic_vector(ADDR_WIDTH-1 downto 0);
            regWrData :  in  std_logic_vector(DATA_WIDTH-1 downto 0);
            regRdData :  out std_logic_vector(DATA_WIDTH-1 downto 0)

        );
end adc_frontend;

architecture arch of adc_frontend is
    signal req_s        : std_logic;

    signal buffer_we_s  : std_logic;
    signal buffer_we_s_cdc  : std_logic;
    signal buffer_op_s  : std_logic;
    signal buffer_op_s_cdc  : std_logic;
    signal we_s         : std_logic;
    signal done_s       : std_logic;
    
    signal clk_div_s    : std_logic;

    signal regs_s       : std_logic_matrix(0 downto 0);

    signal digital_s    : std_logic_deser(N-1 downto 0);
    signal write_data_s : std_logic_vector(DATA_WIDTH-1 downto 0);
    signal regs_buff_s  : std_logic_vector(DATA_WIDTH-1 downto 0);
begin
    inst_adc_deser_top : entity work.adc_deser_top
    generic map(
                   N => N,
                   REGION_SELECT => region_select
               )
    port map(
                rst_i       => rst_i,
                threshold_i => threshold_i,
                analog_i    => analog_i,
                digital_o   => digital_s,
                clk_i       => clk_i,
                --clk_div_o   => clk_div_s,
                clk_div_i   => clk_div_i,
                clk_we_i    => clk_we_i,
                we_i        => buffer_we_s
            );

    inst_registers_r : entity work.registers_ro
    generic map(
                   BASE_OFFSET => BASE_OFFSET,
                   DATA_WIDTH  => DATA_WIDTH,
                   ADDR_WIDTH  => ADDR_WIDTH,
                   TOTAL_SIZE  => 1
               )
    port map(
                clk_i        => regClk,
                done_o       => regAck,
                we_i         => regReq,
                op_i         => regOp,
                write_addr_i => regAddr,
                write_data_i => regWrData,
                read_addr_i  => regAddr,
                read_data_o  => regRdData,

                regs_o       => regs_s
            );

    -- Dummy buffer for handling clock domain crossing
--    inst_cross_clock : entity work.fifo_cross_clock
--    port map (
--                 clka => clk_div_i(region_select(0)),
--                 ena => '1',
--                 wea => (0 downto 0 => '1'),
--                 addra => (0 downto 0 => '0'),
--                 dina => regs_buff_s,
--                 clkb => regClk,
--                 enb => '1',
--                 addrb => (0 downto 0 => '0'),
--                 doutb => regs_s(0)
--             );

    cdc_buffer_we_inst : entity work.clkcrossing_buf
    generic map(
               NBITS => 1
           )
    port map(
                nrst => not rst_i,
                DA(0) => buffer_we_s,
                QB(0) => buffer_we_s_cdc,
                ClkA => clk_div_i(region_select(0)),
                ClkB => regClk
            );

    cdc_buffer_op_inst : entity work.clkcrossing_buf
    generic map(
               NBITS => 1
           )
    port map(
                nrst => not rst_i,
                DA(0) => buffer_op_s,
                QB(0) => buffer_op_s_cdc,
                ClkA => regClk,
                ClkB => clk_div_i(region_select(0))
            );

    req_s <= regReq when 
             to_integer(unsigned(regAddr)) = BASE_OFFSET
         else '0' when stop_sample_i = '1'
         else buffer_we_s_cdc;

    buffer_op_s <= '0' when 
             to_integer(unsigned(regAddr)) = BASE_OFFSET or
             stop_sample_i = '1'
         else '1';

    inst_circular_buffer : entity work.circular_buffer
    generic map(
                   DATA_WIDTH => DATA_WIDTH,
                   ADDR_WIDTH => ADDR_WIDTH,
                   TOTAL_SIZE => BUFF_SIZE
               )
    port map(
                clk_i        => clk_div_i(region_select(0)),
                clk_slow_i   => regClk,
                rst_i        => rst_i,
                empty_o      => open,
                req_i        => req_s,
                op_i         => buffer_op_s_cdc,
                write_data_i => write_data_s,
                --write_data_i => (31 downto 8 => '0') & digital_s(0),
                --write_data_i => (31 downto 24 => '0') & digital_s(2) & digital_s(1) & digital_s(0),
                read_data_o  => regs_s(0)
                --read_data_o  => regs_buff_s
            );

    FOR1 : for i in 3 downto 0 generate
        IF1 : if i <= N-1 generate
            write_data_s((i+1)*8-1 downto (i)*8) <= digital_s(i);
        end generate IF1;
        IF2 : if i > N-1 generate
            write_data_s((i+1)*8-1 downto (i)*8) <= (others => '0');
        end generate IF2;
    end generate FOR1;

end architecture arch;
