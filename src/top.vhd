library ieee;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

library work;
use work.UtilityPkg.all;

entity toplevel is
    generic(N : integer := 3);
    port(
            threshold_ch1_i   :  in  std_logic_vector((2**N-2) downto 0);
            analog_ch1_i      :  in  std_logic_vector((2**N-2) downto 0);

            threshold_ch2_i   :  in  std_logic_vector((2**N-2) downto 0);
            analog_ch2_i      :  in  std_logic_vector((2**N-2) downto 0);

            clk_i         :  in  std_logic;

            mosi_o        :  out std_logic;
            sck_o         :  out std_logic;
            cs_o          :  out std_logic;

            uart_rxd_out  :  out std_logic;
            uart_txd_in   :  in  std_logic
        );
end toplevel;

architecture arch of toplevel is
    -- Select clock region BUFIO/R
    constant clk_region_ch1 : region_select := (0,0,0,0,0,0,0);
    constant clk_region_ch2 : region_select := (1,1,1,1,1,1,1);

    constant REG_ADDR_BITS_G : integer := 32;
    constant REG_DATA_BITS_G : integer := 32;

    signal clk_s         : std_logic;
    signal clk_100M_s    : std_logic;
    signal clk_100M_mr_s : std_logic;
    signal clk_div_s     : std_logic;
    signal clk_ser_s     : std_logic;
    signal clk_ser_90_s  : std_logic;
    signal clk_fct_gen_s : std_logic;
    
    signal clk_ser_r1_s  : std_logic;
    signal clk_ser_r2_s  : std_logic;
    signal clk_100M_r1_s  : std_logic;
    signal clk_100M_r2_s  : std_logic;
    signal clk_div_r1_s  : std_logic;
    signal clk_div_r2_s  : std_logic;

    signal regAck_s      : std_logic;
    signal regReq_s      : std_logic;
    signal regOp_s       : std_logic;
    signal regAddr_s     : std_logic_vector(REG_ADDR_BITS_G-1 downto 0);
    signal regWrData_s   : std_logic_vector(REG_DATA_BITS_G-1 downto 0);
    signal regRdData_s   : std_logic_vector(REG_DATA_BITS_G-1 downto 0);

    signal rst_adc_s     : std_logic;

    signal clk_spi_s     : std_logic;
    signal clk_simple_s  : std_logic;

    signal stop_sample_s : std_logic;

    signal fct_gen_s     : std_logic;

begin
    --       ____ _            _        
    --      / ___| | ___   ___| | _____ 
    --     | |   | |/ _ \ / __| |/ / __|
    --     | |___| | (_) | (__|   <\__ \
    --      \____|_|\___/ \___|_|\_\___/

    clk_s <= clk_i;
    inst_clk_adc : entity work.clk_adc_wrapper
    port map(
                clk_in1_0  => clk_simple_s,
                clk_out1_0 => clk_100M_mr_s,
                --clk_out1_0 => clk_100M_s,
                --clk_out2_0 => clk_div_s,
                reset_0    => '0'
            );

    bufmr1_inst : BUFMR
    port map(
                O => clk_100M_r1_s,
                I  => clk_100M_mr_s
            );

    bufmr2_inst : BUFMR
    port map(
                O => clk_100M_r2_s,
                I  => clk_100M_mr_s
            );

    -- Handle multi-regional clk : Region 1
    clk_r1_inst : BUFIO
    port map(
                O => clk_ser_r1_s,
                I => clk_100M_r1_s
            );

    clk_div_r1_inst : BUFR
    generic map(
                BUFR_DIVIDE => "4",
                SIM_DEVICE => "7SERIES"
               )
    port map(
                CE => '1',
                CLR => '0',
                O => clk_div_r1_s,
                I => clk_100M_r1_s
            );

    -- Handle multi-regional clk : Region 2
    clk_r2_inst : BUFIO
    port map(
                O => clk_ser_r2_s,
                I => clk_100M_r2_s
            );

    clk_div_r2_inst : BUFR
    generic map(
                BUFR_DIVIDE => "4",
                SIM_DEVICE => "7SERIES"
               )
    port map(
                CE => '1',
                CLR => '0',
                O => clk_div_r2_s,
                I => clk_100M_r2_s
            );


    inst_simple_clk : entity work.simple_clk_wrapper
    port map(
                clk_out1_0 => clk_simple_s,
                sys_clock  => clk_s,
                reset      => '0'
            );

    --         _    ____   ____ 
    --        / \  |  _ \ / ___|
    --       / _ \ | | | | |    
    --      / ___ \| |_| | |___ 
    --     /_/   \_\____/ \____|

    inst_adc_ctrl : entity work.adc_ctrl
    generic map(
               ADDR_WIDTH => 32,
               DATA_WIDTH => 32,
               BASE_ADDR  => 16#00#
           )
    port map(
            -- CTRL signals
            stop_sample_o => stop_sample_s,
            reset_adcs_o  => rst_adc_s,
            -- Registers
            regClk    => clk_simple_s,
            regAck    => regAck_s,
            regReq    => regReq_s,
            regOp     => regOp_s,
            regAddr   => regAddr_s,
            regWrData => regWrData_s,
            regRdData => regRdData_s

        );
    

    inst_adc_fe_ch1 : entity work.adc_frontend
    generic map(
                   N           => N,
                   ADDR_WIDTH  => REG_ADDR_BITS_G,
                   DATA_WIDTH  => REG_DATA_BITS_G,
                   BASE_OFFSET => 16#01#,
                   BUFF_SIZE   => 1024,
                   REGION_SELECT => clk_region_ch1
               )
    port map(
                rst_i       => rst_adc_s,
                threshold_i => threshold_ch1_i,
                analog_i    => analog_ch1_i,
                clk_i       => clk_ser_r2_s & clk_ser_r1_s,
                clk_div_i   => clk_div_r2_s & clk_div_r1_s,
                clk_we_i    => clk_100M_mr_s,
                stop_sample_i => stop_sample_s,
                -- Registers
                regClk    => clk_simple_s,
                regAck    => regAck_s,
                regReq    => regReq_s,
                regOp     => regOp_s,
                regAddr   => regAddr_s,
                regWrData => regWrData_s,
                regRdData => regRdData_s
            );

    inst_adc_fe_ch2 : entity work.adc_frontend
    generic map(
                   N           => N,
                   ADDR_WIDTH  => REG_ADDR_BITS_G,
                   DATA_WIDTH  => REG_DATA_BITS_G,
                   BASE_OFFSET => 16#02#,
                   BUFF_SIZE   => 1024,
                   REGION_SELECT => clk_region_ch2
               )
    port map(
                rst_i       => rst_adc_s,
                threshold_i => threshold_ch2_i,
                analog_i    => analog_ch2_i,
                clk_i       => clk_ser_r2_s & clk_ser_r1_s,
                clk_div_i   => clk_div_r2_s & clk_div_r1_s,
                clk_we_i    => clk_100M_mr_s,
                stop_sample_i => stop_sample_s,
                -- Registers
                regClk    => clk_simple_s,
                regAck    => regAck_s,
                regReq    => regReq_s,
                regOp     => regOp_s,
                regAddr   => regAddr_s,
                regWrData => regWrData_s,
                regRdData => regRdData_s
            );

    --       _   _   _    ____ _____ 
    --      | | | | / \  |  _ \_   _|
    --      | | | |/ _ \ | |_) || |  
    --      | |_| / ___ \|  _ < | |  
    --       \___/_/   \_\_| \_\|_|  
    --

    cmp_uart_fe : entity work.UartFrontEnd
    generic map(
                   REG_ADDR_BITS_G => REG_ADDR_BITS_G,
                   REG_DATA_BITS_G => REG_DATA_BITS_G,
                   CLOCK_RATE_G => 50000000,
                   --CLOCK_RATE_G => 20000000,
                   BAUD_RATE_G  => 115200
               )
    port map(
                clk        => clk_simple_s,
                sRst       => '0',
                uartRx     => uart_txd_in,
                uartTx     => uart_rxd_out,
                regAddr    => regAddr_s,
                regWrData  => regWrData_s,
                regRdData  => regRdData_s,
                regReq     => regReq_s,
                regOp      => regOp_s,
                regAck     => regAck_s
            );

    --       ____    _    ____ 
    --      |  _ \  / \  / ___|
    --      | | | |/ _ \| |    
    --      | |_| / ___ \ |___ 
    --      |____/_/   \_\____|

    inst_dac : entity work.dactop_ad5672
    generic map(
               ADDR_WIDTH => REG_ADDR_BITS_G,
               DATA_WIDTH => REG_DATA_BITS_G,
               BASE_ADDR  => 16#10#
           )
    port map(
            -- SPI
            clk_i   => clk_simple_s,
            sck_o   => sck_o,
            cs_o    => cs_o,
            mosi_o  => mosi_o,
            -- Registers
            done_o       => regAck_s,
            we_i         => regReq_s,
            op_i         => regOp_s,
            write_addr_i => regAddr_s,
            write_data_i => regWrData_s,
            read_addr_i  => regAddr_s,
            read_data_o  => regRdData_s

        );

end architecture arch;
