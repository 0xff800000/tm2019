library ieee;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

library work;
use work.UtilityPkg.all;

entity adc_ctrl is
    generic(
               ADDR_WIDTH : integer := 32;
               DATA_WIDTH : integer := 32;
               BASE_ADDR  : integer := 16#10#
           );
    port(
            -- CTRL signals
            stop_sample_o : out std_logic;
            reset_adcs_o  : out std_logic;
            -- Registers
            regClk    :  in  std_logic;
            regAck    :  out std_logic;
            regReq    :  in  std_logic;
            regOp     :  in  std_logic;
            regAddr   :  in  std_logic_vector(ADDR_WIDTH-1 downto 0);
            regWrData :  in  std_logic_vector(DATA_WIDTH-1 downto 0);
            regRdData :  out std_logic_vector(DATA_WIDTH-1 downto 0)

        );
end adc_ctrl;

architecture arch of adc_ctrl is

    constant TOTAL_SIZE : integer := 1;
    signal regAck_s    : std_logic;
    signal regReq_s    : std_logic;
    signal regOp_s     : std_logic;
    signal regAddr_s   : std_logic_vector(ADDR_WIDTH-1 downto 0);
    signal regWrData_s : std_logic_vector(DATA_WIDTH-1 downto 0);
    signal regRdData_s : std_logic_vector(DATA_WIDTH-1 downto 0);
    signal regs_s      : std_logic_matrix(TOTAL_SIZE-1 downto 0);
begin
    inst_registers_r : entity work.registers_rw
    generic map(
                   DIRECTION   => "W",
                   BASE_OFFSET => BASE_ADDR,
                   DATA_WIDTH  => DATA_WIDTH,
                   ADDR_WIDTH  => ADDR_WIDTH,
                   TOTAL_SIZE  => TOTAL_SIZE
               )
    port map(
                clk_i        => regClk,
                done_o       => regAck,
                we_i         => regReq,
                op_i         => regOp,
                write_addr_i => regAddr,
                write_data_i => regWrData,
                read_addr_i  => regAddr,
                read_data_o  => regRdData,
                regs_o       => regs_s
            );
    
    -- Stop sampling for all the channels
    stop_sample_o <= regs_s(0)(0);
    reset_adcs_o  <= regs_s(0)(1);

end architecture arch;
