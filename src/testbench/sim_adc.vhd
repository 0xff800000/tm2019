library ieee;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

entity sim_adc is
end sim_adc;

architecture Behavioral of sim_adc is

    constant NBITS : integer := 3;
    constant clk_period : time := 10 ns;

    signal threshold_s : std_logic_vector((2**NBITS-2) downto 0);
    signal analog_s : std_logic_vector((2**NBITS-2) downto 0);
    signal digital_s : std_logic_vector(NBITS-1 downto 0);
    signal clk_s : std_logic;

    signal thermo_s : std_logic_vector((2**NBITS-2) downto 0);
    signal not_thermo_s : std_logic_vector((2**NBITS-2) downto 0);

    component adc_top
        generic(N : integer := NBITS);
        port(
                threshold_i : in  std_logic_vector((2**N-2) downto 0);
                analog_i    : in  std_logic_vector((2**N-2) downto 0);
                digital_o   : out std_logic_vector(N-1 downto 0);
                clk_i       : in std_logic
            );
    end component;

begin

    not_thermo_s <= not thermo_s;
    threshold_s <= not_thermo_s;
    analog_s <= thermo_s;

    
    inst_adc_top : adc_top
    generic map(N => NBITS)
    port map(
                threshold_i => threshold_s,
                analog_i    => analog_s,
                digital_o   => digital_s,
                clk_i       => clk_s
        );

    -- Clock genenration
    process
    begin
        clk_s <= '0';
        wait for clk_period/2;
        clk_s <= '1';
        wait for clk_period/2;
    end process;

    -- Simulation
    process
    begin
        thermo_s <= (others => '0');
        --wait for clk_period;
        wait for 745 ns;
        for n in thermo_s'range loop
            thermo_s <= thermo_s(thermo_s'high-1 downto 0) & '1';
            wait for clk_period;
        end loop;

        for n in thermo_s'range loop
            thermo_s <= '0' & thermo_s(thermo_s'high downto 1);
            wait for clk_period;
        end loop;
        wait;
    end process;


end Behavioral;

