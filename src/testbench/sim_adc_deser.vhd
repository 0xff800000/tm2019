library ieee;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

library work;
use work.UtilityPkg.all;

entity sim_adc_deser is
end sim_adc_deser;

architecture Behavioral of sim_adc_deser is

    constant NBITS      : integer := 3;
    constant clk_period : time    := 10 ns;

    signal threshold_s  : std_logic_vector((2**NBITS-2) downto 0);
    signal analog_s     : std_logic_vector((2**NBITS-2) downto 0);
    signal digital_s    : std_logic_deser(NBITS-1 downto 0);
    signal clk_s        : std_logic;
    signal rst_s        : std_logic;

    signal thermo_s     : std_logic_vector((2**NBITS-2) downto 0);
    signal not_thermo_s : std_logic_vector((2**NBITS-2) downto 0);

begin
    inst_adc_deser : entity work.adc_deser_top
    generic map(N => NBITS)
    port map(
            rst_i       => rst_s,
            threshold_i => threshold_s,
            analog_i    => analog_s,
            digital_o   => digital_s,
            clk_i       => clk_s
        );

    not_thermo_s <= not thermo_s;
    threshold_s  <= not_thermo_s;
    analog_s     <= thermo_s;

    -- Clock genenration
    process
    begin
        clk_s <= '0';
        wait for clk_period/2;
        clk_s <= '1';
        wait for clk_period/2;
    end process;

    -- Input signal genenration
    process
    begin
        rst_s <= '1';
        thermo_s <= (others => '0');
        wait for 10*clk_period;
        rst_s <= '0';
        wait for 9*clk_period-clk_period/4;
        for n in thermo_s'range loop
            thermo_s <= thermo_s(thermo_s'high-1 downto 0) & '1';
            wait for clk_period/2;
        end loop;

        for n in thermo_s'range loop
            thermo_s <= '0' & thermo_s(thermo_s'high downto 1);
            wait for clk_period/2;
        end loop;

        wait;
    end process;


end Behavioral;

