library ieee;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

LIBRARY work;
use work.UtilityPkg.all;

entity sim_command_regs is
    alias sl is std_logic;
    alias slv is std_logic_vector;
end sim_command_regs;

architecture Behavioral of sim_command_regs is

    constant BYTE_CMD_RD_C   : slv(7 downto 0)  := x"72";
    constant BYTE_CMD_WR_C   : slv(7 downto 0)  := x"77";

    constant ADDR_W : integer := 32;

    constant clk_period : time := 10 ns;

    signal rxData_s : slv(7 downto 0);
    signal rxDataValid_s : sl;
    signal txData_s : slv(7 downto 0);
    signal txDataValid_s : sl;
    signal txDataReady_s : sl;
    signal regAddr_s : slv(ADDR_W-1 downto 0);
    signal regWrData_s : slv(ADDR_W-1 downto 0);
    signal regRdData_s : slv(ADDR_W-1 downto 0);
    signal regReq_s : sl;
    signal regOp_s : sl;
    signal regAck_s : sl;

    signal clk_s : sl;
    signal usrRst_s : sl;

    component registers
        generic(
                   DATA_WIDTH : integer := 8;
                   ADDR_WIDTH : integer := 8;
                   TOTAL_SIZE : integer := 8
               );
        port(
                clk_i: in std_logic;
                done_o : out std_logic;
                we_i : in std_logic;
                op_i : in std_logic;
                write_addr_i : in std_logic_vector (ADDR_WIDTH-1 downto 0);
                write_data_i : in std_logic_vector (DATA_WIDTH-1 downto 0);
                read_addr_i : in std_logic_vector (ADDR_WIDTH-1 downto 0);
                read_data_o : out std_logic_vector (DATA_WIDTH-1 downto 0)
            );
    end component;

    component CommandInterpreter
        generic (
                    REG_ADDR_BITS_G : integer := 32;
                    REG_DATA_BITS_G : integer := 32;
                    TIMEOUT_G       : integer := 40000 -- 500 ms @ 80 MHz clock
                    --GATE_DELAY_G    : time    := 1 ns
                );
        port ( 
                 -- User clock and reset
                 usrClk      : in  sl;
                 usrRst      : in  sl := '0';
                 -- Incoming data
                 rxData      : in  slv(7 downto 0);
                 rxDataValid : in  sl;
                 -- CRC interface
                 --      crcByte     : out slv(7 downto 0);
                 --      crcRst      : out sl;
                 --      crcEn       : out sl;
                 --      crc         : in  slv(7 downto 0);
                 -- Outgoing response
                 txData      : out slv(7 downto 0);
                 txDataValid : out sl;
                 txDataReady : in  sl;
                 -- Register interfaces
                 regAddr     : out slv(REG_ADDR_BITS_G-1 downto 0);
                 regWrData   : out slv(REG_DATA_BITS_G-1 downto 0);
                 regRdData   : in  slv(REG_DATA_BITS_G-1 downto 0);
                 regReq      : out sl;
                 regOp       : out sl;
                 regAck      : in  sl
             ); 
    end component;

begin
    inst_cmd_interp : CommandInterpreter
    generic map(
                   REG_ADDR_BITS_G => 32,
                   REG_DATA_BITS_G => 32,
                   TIMEOUT_G       => 40000 --, -- 500 ms @ 80 MHz clock
                   --GATE_DELAY_G    => 1 ns
               )
    port map( 
                -- User clock and reset
                usrClk => clk_s,
                usrRst => usrRst_s,
                -- Incoming data
                rxData => rxData_s,
                rxDataValid => rxDataValid_s,
                -- CRC interface
                --      crcByte     : out slv(7 downto 0);
                --      crcRst      : out sl;
                --      crcEn       : out sl;
                --      crc         : in  slv(7 downto 0);
                -- Outgoing response
                txData => txData_s,
                txDataValid => txDataValid_s,
                txDataReady => txDataReady_s,
                -- Register interfaces
                regAddr => regAddr_s,
                regWrData => regWrData_s,
                regRdData => regRdData_s,
                regReq => regReq_s,
                regOp => regOp_s,
                regAck => regAck_s
            ); 



    inst_registers : registers
    generic map(
                   DATA_WIDTH => 32,
                   ADDR_WIDTH => 32,
                   TOTAL_SIZE => 512
               )
    port map(
                clk_i => clk_s,
                done_o => regAck_s,
                we_i => regReq_s,
                op_i => regOp_s,
                write_addr_i => regAddr_s,
                write_data_i => regWrData_s,
                read_addr_i => regAddr_s,
                read_data_o => regRdData_s
            );

    -- Clock genenration
    process
    begin
        clk_s <= '0';
        wait for clk_period/2;
        clk_s <= '1';
        wait for clk_period/2;
    end process;

    -- Simulation
    process
    begin
        -- Init
        txDataReady_s <= '0';
        usrRst_s <= '1';
        rxData_s <= (others => '0');
        rxDataValid_s <= '0';
        wait for 3*clk_period/2;

        -----------------------
        -- Write command 0xc0decafe @ address 0x00000000
        txDataReady_s <= '1';
        usrRst_s <= '0';
        rxData_s <= BYTE_CMD_WR_C;
        rxDataValid_s <= '1';
        wait for clk_period;

        -- Address 0xdeadbeef
        txDataReady_s <= '1';
        usrRst_s <= '0';
        rxDataValid_s <= '1';
        rxData_s <= x"00";
        wait for clk_period;
        rxData_s <= x"00";
        wait for clk_period;
        rxData_s <= x"00";
        wait for clk_period;
        rxData_s <= x"00";
        wait for clk_period;

        -- Data 0xc0decafe
        txDataReady_s <= '1';
        usrRst_s <= '0';
        rxDataValid_s <= '1';
        rxData_s <= x"c0";
        wait for clk_period;
        rxData_s <= x"de";
        wait for clk_period;
        rxData_s <= x"ca";
        wait for clk_period;
        rxData_s <= x"fe";
        wait for clk_period;

        wait for 10*clk_period;

        -----------------------
        -- Write command 0xdeadbeef @ address 0x0000000f
        txDataReady_s <= '1';
        usrRst_s <= '0';
        rxData_s <= BYTE_CMD_WR_C;
        rxDataValid_s <= '1';
        wait for clk_period;

        -- Address 0x0000000f
        txDataReady_s <= '1';
        usrRst_s <= '0';
        rxDataValid_s <= '1';
        rxData_s <= x"00";
        wait for clk_period;
        rxData_s <= x"00";
        wait for clk_period;
        rxData_s <= x"00";
        wait for clk_period;
        rxData_s <= x"0f";
        wait for clk_period;

        -- Data 0xdeadbeef
        txDataReady_s <= '1';
        usrRst_s <= '0';
        rxDataValid_s <= '1';
        rxData_s <= x"de";
        wait for clk_period;
        rxData_s <= x"ad";
        wait for clk_period;
        rxData_s <= x"be";
        wait for clk_period;
        rxData_s <= x"ef";
        wait for clk_period;

        wait for 10*clk_period;

        -- Read command
        txDataReady_s <= '1';
        usrRst_s <= '0';
        rxData_s <= BYTE_CMD_RD_C;
        rxDataValid_s <= '1';
        wait for clk_period;

        -- Address 0xc001c0de; data 0x00000000
        txDataReady_s <= '1';
        usrRst_s <= '0';
        rxDataValid_s <= '1';
        rxData_s <= x"00";
        wait for clk_period;
        rxData_s <= x"00";
        wait for clk_period;
        rxData_s <= x"00";
        wait for clk_period;
        rxData_s <= x"00";
        wait for clk_period;

        wait for 10*clk_period;

        -- Read command
        txDataReady_s <= '1';
        usrRst_s <= '0';
        rxData_s <= BYTE_CMD_RD_C;
        rxDataValid_s <= '1';
        wait for clk_period;

        -- Address 0xc001c0de; data 0x0000000f
        txDataReady_s <= '1';
        usrRst_s <= '0';
        rxDataValid_s <= '1';
        rxData_s <= x"00";
        wait for clk_period;
        rxData_s <= x"00";
        wait for clk_period;
        rxData_s <= x"00";
        wait for clk_period;
        rxData_s <= x"0f";
        wait for clk_period;

        -- End simulation
        wait for 4*clk_period;
        wait;
    end process;


end Behavioral;

