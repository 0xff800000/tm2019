library ieee;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

library work;
use work.UtilityPkg.all;

entity sim_clk_div is
end sim_clk_div;

architecture Behavioral of sim_clk_div is

    constant clk_period : time       := 50 ns;

    signal clk_in_s     : std_logic;
    signal clk_out_s    : std_logic;

begin

    inst_clk_div : entity work.clock_divider
    generic map(
                   FREQ_IN  => 20000000,
                   FREQ_OUT => 200000
               )
    port map(
                clk_i => clk_in_s,
                clk_o => clk_out_s
            );

    -- Clock genenration
    process
    begin
        clk_in_s <= '0';
        wait for clk_period/2;
        clk_in_s <= '1';
        wait for clk_period/2;
    end process;

end Behavioral;

