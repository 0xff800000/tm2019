library ieee;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

library work;
use work.UtilityPkg.all;

entity sim_adc_frontend is
end sim_adc_frontend;

architecture Behavioral of sim_adc_frontend is

    constant NBITS      : integer := 3;
    constant clk_period     : time    := 5 ns;
    constant reg_clk_period : time    := 50 ns;

    constant clk_region_ch2 : region_select := (1,0,1,0,1,0,1);
    constant ADDR_WIDTH : integer := 32;
    constant DATA_WIDTH : integer := 32;

    signal threshold_ch1_s  : std_logic_vector((2**NBITS-2) downto 0);
    signal analog_ch1_s     : std_logic_vector((2**NBITS-2) downto 0);
    signal threshold_ch2_s  : std_logic_vector((2**NBITS-2) downto 0);
    signal analog_ch2_s     : std_logic_vector((2**NBITS-2) downto 0);
    signal digital_s    : std_logic_deser(NBITS-1 downto 0);
    signal clk_s        : std_logic;
    signal clk_div_s    : std_logic;
    signal rst_s        : std_logic;

    signal thermo_s     : std_logic_vector((2**NBITS-2) downto 0);
    signal not_thermo_s : std_logic_vector((2**NBITS-2) downto 0);

    signal stop_sample_s : std_logic;

    signal regclk_s    : std_logic;
    signal regAck_s    : std_logic;
    signal regReq_s    : std_logic;
    signal regOp_s     : std_logic;
    signal regAddr_s   : std_logic_vector(ADDR_WIDTH-1 downto 0);
    signal regWrData_s : std_logic_vector(DATA_WIDTH-1 downto 0);
    signal regRdData_s : std_logic_vector(DATA_WIDTH-1 downto 0);

begin

    inst_adc_ctrl : entity work.adc_ctrl
    generic map(
               ADDR_WIDTH => 32,
               DATA_WIDTH => 32,
               BASE_ADDR  => 16#00#
           )
    port map(
            -- CTRL signals
            stop_sample_o => stop_sample_s,
            reset_adcs_o  => open,
            -- Registers
            regClk    => regClk_s,
            regAck    => regAck_s,
            regReq    => regReq_s,
            regOp     => regOp_s,
            regAddr   => regAddr_s,
            regWrData => regWrData_s,
            regRdData => regRdData_s

        );
    

    inst_adc_fe_ch1 : entity work.adc_frontend
    generic map(
                   N           => NBITS,
                   ADDR_WIDTH  => ADDR_WIDTH,
                   DATA_WIDTH  => DATA_WIDTH,
                   BASE_OFFSET => 16#01#,
                   BUFF_SIZE   => 8,
                   REGION_SELECT => clk_region_ch2
               )
    port map(
                rst_i       => rst_s,
                threshold_i => threshold_ch1_s,
                analog_i    => analog_ch1_s,
                clk_i       => clk_s & clk_s,
                clk_div_i   => clk_div_s & clk_div_s,
                clk_we_i    => clk_s,
                stop_sample_i => stop_sample_s,
                -- Registers
                regClk    => regClk_s,
                regAck    => regAck_s,
                regReq    => regReq_s,
                regOp     => regOp_s,
                regAddr   => regAddr_s,
                regWrData => regWrData_s,
                regRdData => regRdData_s
            );

    inst_adc_fe_ch2 : entity work.adc_frontend
    generic map(
                   N           => NBITS,
                   ADDR_WIDTH  => ADDR_WIDTH,
                   DATA_WIDTH  => DATA_WIDTH,
                   BASE_OFFSET => 16#02#,
                   BUFF_SIZE   => 8,
                   REGION_SELECT => clk_region_ch2
               )
    port map(
                rst_i       => rst_s,
                threshold_i => threshold_ch2_s,
                analog_i    => analog_ch2_s,
                clk_i       => clk_s & clk_s,
                clk_div_i   => clk_div_s & clk_div_s,
                clk_we_i    => clk_s,
                stop_sample_i => stop_sample_s,
                -- Registers
                regClk    => regClk_s,
                regAck    => regAck_s,
                regReq    => regReq_s,
                regOp     => regOp_s,
                regAddr   => regAddr_s,
                regWrData => regWrData_s,
                regRdData => regRdData_s
            );

    not_thermo_s <= not thermo_s;
    threshold_ch1_s  <= not_thermo_s;
    analog_ch1_s     <= thermo_s;

    threshold_ch2_s  <= thermo_s;
    analog_ch2_s     <= not_thermo_s;

    -- Clock genenration
    process
    begin
        clk_s <= '1';
        wait for clk_period/2;
        clk_s <= '0';
        wait for clk_period/2;
    end process;
    process
    begin
        clk_div_s <= '1';
        wait for 2*clk_period;
        clk_div_s <= '0';
        wait for 2*clk_period;
    end process;

    -- reg Clock genenration
    process
    begin
        regclk_s <= '0';
        wait for reg_clk_period/2;
        regclk_s <= '1';
        wait for reg_clk_period/2;
    end process;

    -- Reset generation
    process
    begin
        rst_s <= '1';
        wait for 10*clk_period;
        rst_s <= '0';
        wait;
    end process;

    -- Input signal genenration
    process
    begin
        thermo_s <= (others => '0');
        wait for 0.5*clk_period/2;
        --wait for 19*clk_period-clk_period/4;
        for n in thermo_s'range loop
            thermo_s <= thermo_s(thermo_s'high-1 downto 0) & '1';
            wait for clk_period/2;
        end loop;
        thermo_s <= (others => '0');

        wait for 0.5*clk_period/2;

        --for n in thermo_s'range loop
        --    thermo_s <= '0' & thermo_s(thermo_s'high downto 1);
        --    wait for clk_period/2;
        --end loop;

    end process;

    -- Capture signal and read
    process
    begin
        -- Wait for ISERDESE to start
        wait for 17*clk_period;

        -- Capture data
        regAddr_s   <= (others => '0');
        regWrData_s <= (others => '0');
        wait for reg_clk_period;
        regReq_s    <= '1';
        regOp_s     <= '1';
        wait for reg_clk_period;
        regReq_s    <= '0';
        wait for reg_clk_period;

        -- Stop capture
        regAddr_s   <= (others => '0');
        regWrData_s <= (regWrData_s'high downto 1 => '0', 0 => '1');
        regReq_s    <= '1';
        regOp_s     <= '1';
        wait for reg_clk_period;
        regReq_s    <= '0';
        wait for reg_clk_period;

        -- Read buffer ch1
        f1:for n in 0 to 30 loop
            regAddr_s   <= x"00000001";
            regReq_s    <= '1';
            regOp_s     <= '0';
            wait for reg_clk_period;
            regReq_s    <= '0';
            if regRdData_s = x"ffffffff" then
                exit f1;
            end if;
            wait for reg_clk_period;
        end loop;
        -- Read buffer ch2
        f2:for n in 0 to 30 loop
            regAddr_s   <= x"00000002";
            regReq_s    <= '1';
            regOp_s     <= '0';
            wait for reg_clk_period;
            regReq_s    <= '0';
            if regRdData_s = x"ffffffff" then
                exit f2;
            end if;
            wait for reg_clk_period;
        end loop;
    end process;


end Behavioral;

