library ieee;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_signed.ALL;

library unisim;
use unisim.vcomponents.all;

entity sim_circ_buff is
end sim_circ_buff;

architecture Behavioral of sim_circ_buff is

    constant clk_period : time := 10 ns;
    constant DATA_WIDTH : integer := 32;
    constant ADDR_WIDTH : integer := 32;
    constant TOTAL_SIZE : integer := 8;

    signal write_data_s : std_logic_vector(DATA_WIDTH-1 downto 0);
    signal read_data_s : std_logic_vector(DATA_WIDTH-1 downto 0);
    signal clk_s : std_logic;
    signal op_s : std_logic;
    signal req_s : std_logic;
    signal empty_s : std_logic;

begin
    inst_circ_buff : entity work.circular_buffer
    generic map(
               DATA_WIDTH  => DATA_WIDTH,
               ADDR_WIDTH  => ADDR_WIDTH,
               TOTAL_SIZE  => TOTAL_SIZE
               )
    port map(
            clk_i        => clk_s,
            empty_o      => empty_s,
            req_i         => req_s,
            op_i         => op_s,
            write_data_i => write_data_s,
            read_data_o  => read_data_s
        );

    -- Clock genenration
    process
    begin
        clk_s <= '0';
        wait for clk_period/2;
        clk_s <= '1';
        wait for clk_period/2;
    end process;

    -- Simulation
    process
    begin
        -- Test write - read
        write_data_s <= x"cafec0de";
        op_s <= '1';
        req_s <= '1';
        wait for clk_period;
        assert empty_s = '0' report "empty_s signal wrong" severity failure;
        op_s <= '0';
        req_s <= '1';
        wait for clk_period;
        assert empty_s = '1' report "empty_s signal wrong" severity failure;
        assert write_data_s = read_data_s report "read != write" severity failure;

        -- Test write overlap
        write_data_s <= (others => '0');
        op_s <= '1';
        req_s <= '1';
        for n in 0 to TOTAL_SIZE loop
            wait for clk_period;
            write_data_s <= write_data_s + 1;
        end loop;

        op_s <= '0';
        req_s <= '1';
        for n in 0 to TOTAL_SIZE loop
            wait for clk_period;
        end loop;
        req_s <= '0';

--        for n in thermo_s'range loop
--            thermo_s <= thermo_s(thermo_s'high-1 downto 0) & '1';
--            wait for clk_period;
--        end loop;
--
--        for n in thermo_s'range loop
--            thermo_s <= '0' & thermo_s(thermo_s'high downto 1);
--            wait for clk_period;
--        end loop;
        wait;
    end process;


end Behavioral;

