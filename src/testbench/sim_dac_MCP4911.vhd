library ieee;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

entity sim_dac is
    end sim_dac;

architecture Behavioral of sim_dac is

    constant NBITS : integer := 1;
    constant clk_period : time := 10 ns;

    signal clk_s : std_logic;
    signal sck_s : std_logic;
    signal cs_s : std_logic;
    signal mosi_s : std_logic;
    signal send_s : std_logic;
    signal dac_val_s : std_logic_vector(11 downto 0);

    component DAC
        port(
                CLK: in std_logic;
                SCK: out std_logic;
                CS: out std_logic;
                MOSI: out std_logic;
                VALUE: in std_logic_vector (11 downto 0);
                SEND: in std_logic
            );
    end component;

begin

    inst_dac : DAC
        port map (
                CLK => clk_s,
                SCK => sck_s,
                CS => cs_s,
                MOSI => mosi_s,
                VALUE => dac_val_s,
                SEND => send_s
            );

    -- Clock genenration
    process
    begin
        clk_s <= '0';
        wait for clk_period/2;
        clk_s <= '1';
        wait for clk_period/2;
    end process;

    -- Simulation
    process
    begin
        send_s <= '0';
        dac_val_s <= (others => '0');
        wait for clk_period/2;
        for n in dac_val_s'range loop
            dac_val_s <= std_logic_vector(to_unsigned(n,dac_val_s'length));
            send_s <= '1';
            wait for clk_period;
            send_s <= '0';
            wait for 64*clk_period/2;
        end loop;

        wait;
    end process;


end Behavioral;

