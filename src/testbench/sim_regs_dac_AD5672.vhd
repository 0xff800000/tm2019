library ieee;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

LIBRARY work;
use work.UtilityPkg.all;

entity sim_regs_dac_ad5672 is
end sim_regs_dac_ad5672;

architecture Behavioral of sim_regs_dac_ad5672 is

    constant BYTE_CMD_RD_C : slv(7 downto 0) := x"72";
    constant BYTE_CMD_WR_C : slv(7 downto 0) := x"77";
    constant ADDR_W        : integer         := 32;
    constant DATA_W        : integer         := 32;
    constant clk_period    : time            := 10 ns;

    signal rxData_s      : slv(7 downto 0);
    signal rxDataValid_s : sl;
    signal txData_s      : slv(7 downto 0);
    signal txDataValid_s : sl;
    signal txDataReady_s : sl;
    signal regAddr_s     : slv(ADDR_W-1 downto 0);
    signal regWrData_s   : slv(DATA_W-1 downto 0);
    signal regRdData_s   : slv(DATA_W-1 downto 0);
    signal regReq_s      : sl;
    signal regOp_s       : sl;
    signal regAck_s      : sl;

    signal clk_s         : sl;
    signal usrRst_s      : sl;
    
    signal sck_s : sl;
    signal cs_s : sl;
    signal mosi_s : sl;

begin
    inst_cmd_interp : entity work.CommandInterpreter
    generic map(
                   REG_ADDR_BITS_G => ADDR_W,
                   REG_DATA_BITS_G => DATA_W,
                   TIMEOUT_G       => 40000
               )
    port map( 
                usrClk      => clk_s,
                usrRst      => usrRst_s,
                rxData      => rxData_s,
                rxDataValid => rxDataValid_s,
                txData      => txData_s,
                txDataValid => txDataValid_s,
                txDataReady => txDataReady_s,
                regAddr     => regAddr_s,
                regWrData   => regWrData_s,
                regRdData   => regRdData_s,
                regReq      => regReq_s,
                regOp       => regOp_s,
                regAck      => regAck_s
            ); 

    inst_dactop : entity work.dactop_ad5672
    generic map(
               ADDR_WIDTH => ADDR_W,
               DATA_WIDTH => DATA_W,
               BASE_ADDR  => 16#10#
           )
    port map(
            -- SPI
            clk_i  => clk_s,
            sck_o  => sck_s,
            cs_o   => cs_s,
            mosi_o => mosi_s,
            -- Registers
            done_o       => regAck_s,
            we_i         => regReq_s,
            op_i         => regOp_s,
            write_addr_i => regAddr_s,
            write_data_i => regWrData_s,
            read_addr_i  => regAddr_s,
            read_data_o  => regRdData_s
        );

    -- Clock genenration
    process
    begin
        clk_s <= '0';
        wait for clk_period/2;
        clk_s <= '1';
        wait for clk_period/2;
    end process;

    -- Simulation
    process
    begin
        -- Init
        txDataReady_s <= '0';
        usrRst_s <= '1';
        rxData_s <= (others => '0');
        rxDataValid_s <= '0';
        wait for clk_period;
        usrRst_s <= '0';
        wait for clk_period;

        -----------------------
        -- Write command 0x00aaaaaa @ address 0x00000011
        -- Select the DAC #1
        txDataReady_s <= '1';
        usrRst_s <= '0';
        rxData_s <= BYTE_CMD_WR_C;
        rxDataValid_s <= '1';
        wait for clk_period;
        -- Address
        txDataReady_s <= '1';
        usrRst_s <= '0';
        rxDataValid_s <= '1';
        rxData_s <= x"00";
        wait for clk_period;
        rxData_s <= x"00";
        wait for clk_period;
        rxData_s <= x"00";
        wait for clk_period;
        rxData_s <= x"10";
        wait for clk_period;
        -- Data
        txDataReady_s <= '1';
        usrRst_s <= '0';
        rxDataValid_s <= '1';
        rxData_s <= x"00";
        wait for clk_period;
        rxData_s <= x"aa";
        wait for clk_period;
        rxData_s <= x"aa";
        wait for clk_period;
        rxData_s <= x"aa";
        wait for clk_period;

        wait for 10*clk_period;


        -----------------------
        -- Write command 0x00999999 @ address 0x00000010 : perform the SPI transfert
        -- Select the DAC #1
        txDataReady_s <= '1';
        usrRst_s <= '0';
        rxData_s <= BYTE_CMD_WR_C;
        rxDataValid_s <= '1';
        wait for clk_period;
        -- Address
        txDataReady_s <= '1';
        usrRst_s <= '0';
        rxDataValid_s <= '1';
        rxData_s <= x"00";
        wait for clk_period;
        rxData_s <= x"00";
        wait for clk_period;
        rxData_s <= x"00";
        wait for clk_period;
        rxData_s <= x"10";
        wait for clk_period;
        -- Data
        txDataReady_s <= '1';
        usrRst_s <= '0';
        rxDataValid_s <= '1';
        rxData_s <= x"00";
        wait for clk_period;
        rxData_s <= x"99";
        wait for clk_period;
        rxData_s <= x"99";
        wait for clk_period;
        rxData_s <= x"99";
        wait for clk_period;

        wait until regReq_s = '0';
        wait until regAck_s = '0';
        wait for clk_period;
        txDataReady_s <= '1';
        usrRst_s <= '0';
        rxData_s <= BYTE_CMD_WR_C;
        rxDataValid_s <= '1';
        wait for clk_period;
        -- Address
        txDataReady_s <= '1';
        usrRst_s <= '0';
        rxDataValid_s <= '1';
        rxData_s <= x"00";
        wait for clk_period;
        rxData_s <= x"00";
        wait for clk_period;
        rxData_s <= x"00";
        wait for clk_period;
        rxData_s <= x"00";
        wait for clk_period;
        -- Data
        txDataReady_s <= '1';
        usrRst_s <= '0';
        rxDataValid_s <= '1';
        rxData_s <= x"00";
        wait for clk_period;
        rxData_s <= x"99";
        wait for clk_period;
        rxData_s <= x"99";
        wait for clk_period;
        rxData_s <= x"99";
        wait for clk_period;

        wait for 10*clk_period;

        -- End simulation
        wait for 4*clk_period;
        wait;
    end process;


end Behavioral;

