library ieee;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

LIBRARY work;
use work.UtilityPkg.all;

entity sim_top is
    alias sl is std_logic;
    alias slv is std_logic_vector;
end sim_top;

architecture Behavioral of sim_top is

    constant NBITS : integer := 1;

    constant BYTE_CMD_RD_C   : slv(7 downto 0)  := x"72";
    constant BYTE_CMD_WR_C   : slv(7 downto 0)  := x"77";

    constant ADDR_W : integer := 32;

    constant CLK_PERIOD : time := 83.333333333333333 ns;

    signal rxData_s : slv(7 downto 0);
    signal rxDataValid_s : sl;
    signal txData_s : slv(7 downto 0);
    signal txDataValid_s : sl;
    signal txDataReady_s : sl;

    signal clk_s        : sl;

    signal threshold_s  : slv((2**NBITS-2) downto 0);
    signal analog_s     : slv((2**NBITS-2) downto 0);
    signal digital_s    : slv(NBITS-1 downto 0);

    signal mosi_s       : sl;
    signal sck_s        : sl;
    signal cs_s         : slv(NBITS-1 downto 0);

    signal uart_rxd_out_s : sl;
    signal uart_txd_in_s  : sl;

    signal usrRst_s: sl;

begin

    --    procedure p_uart_write_register
    --    (
    --    p_addr : in slv(ADDR_W-1 downto 0);
    --    p_data : in slv(ADDR_W-1 downto 0)) is
    --    begin
    --        p_txDataReady_s <= '1';
    --        p_txData_s <= BYTE_CMD_WR_C;
    --        wait for CLK_PERIOD;
    --        for i in 4 downto 1 loop
    --            p_txData_s <= p_addr(i*8-1 downto i*7-1);
    --            wait for CLK_PERIOD;
    --        end loop;
    --        for i in 4 downto 1 loop
    --            p_txData_s <= p_data(i*8-1 downto i*7-1);
    --            wait for CLK_PERIOD;
    --        end loop;
    --        p_txDataReady_s <= '0';
    --    end p_write_register;

    inst_top : entity work.toplevel
    generic map(
                   N => NBITS
               )
    port map(
                threshold_i  => threshold_s,
                analog_i     => analog_s,
                digital_o    => digital_s,
                clk_i        => clk_s,

                mosi_o       => mosi_s,
                sck_o        => sck_s,
                cs_o         => cs_s,

                uart_rxd_out => uart_rxd_out_s,
                uart_txd_in  => uart_txd_in_s
            );

    inst_computer_uart : entity work.UartTop
    generic map(
                   CLOCK_RATE_G => 12000000,
                   BAUD_RATE_G  => 115200
               )
    port map(
                clk         => clk_s,
                sRst        => usrRst_s,
                rxByte      => rxData_s,
                rxByteValid => rxDataValid_s,
                txByte      => txData_s,
                txByteValid => txDataValid_s,
                txByteReady => txDataReady_s,
                uartRx      => uart_rxd_out_s,
                uartTx      => uart_txd_in_s
            );

    -- Clock genenration
    process
    begin
        clk_s <= '0';
        wait for CLK_PERIOD/2;
        clk_s <= '1';
        wait for CLK_PERIOD/2;
    end process;

    -- Simulation
    process
    begin
        -- Init
        --txDataReady_s <= '0';
        usrRst_s <= '1';
        txData_s <= (others => '0');
        --rxDataValid_s <= '0';
        --wait for 3*CLK_PERIOD/2;
        wait for CLK_PERIOD;
        usrRst_s <= '0';
        wait for CLK_PERIOD;

        -----------------------
        -- Write command 0xc0decafe @ address 0x00000000
        --txDataReady_s <= '1';
        txDataValid_s <= '1';
        wait until rising_edge(txDataReady_s);
        txData_s <= BYTE_CMD_WR_C;
        --rxDataValid_s <= '1';
        wait until rising_edge(txDataReady_s);

        -- Address 0xdeadbeef
        --txDataReady_s <= '1';
        usrRst_s <= '0';
        --rxDataValid_s <= '1';
        txData_s <= x"00";
        wait until rising_edge(txDataReady_s);
        txData_s <= x"00";
        wait until rising_edge(txDataReady_s);
        txData_s <= x"00";
        wait until rising_edge(txDataReady_s);
        txData_s <= x"12";
        wait until rising_edge(txDataReady_s);

        -- Data 0xc0decafe
        --txDataReady_s <= '1';
        usrRst_s <= '0';
        --rxDataValid_s <= '1';
        txData_s <= x"c0";
        wait until rising_edge(txDataReady_s);
        txData_s <= x"de";
        wait until rising_edge(txDataReady_s);
        txData_s <= x"ca";
        wait until rising_edge(txDataReady_s);
        txData_s <= x"fe";
        wait until rising_edge(txDataReady_s);

        wait for 10*CLK_PERIOD;

        -----------------------
        -- Write command 0xdeadbeef @ address 0x0000000f
        --txDataReady_s <= '1';
        usrRst_s <= '0';
        txData_s <= BYTE_CMD_WR_C;
        --rxDataValid_s <= '1';
        wait until rising_edge(txDataReady_s);

        -- Address 0x0000000f
        --txDataReady_s <= '1';
        usrRst_s <= '0';
        --rxDataValid_s <= '1';
        txData_s <= x"00";
        wait until rising_edge(txDataReady_s);
        txData_s <= x"00";
        wait until rising_edge(txDataReady_s);
        txData_s <= x"00";
        wait until rising_edge(txDataReady_s);
        txData_s <= x"10";
        wait until rising_edge(txDataReady_s);

        -- Data 0xdeadbeef
        --txDataReady_s <= '1';
        usrRst_s <= '0';
        --rxDataValid_s <= '1';
        txData_s <= x"de";
        wait until rising_edge(txDataReady_s);
        txData_s <= x"ad";
        wait until rising_edge(txDataReady_s);
        txData_s <= x"be";
        wait until rising_edge(txDataReady_s);
        txData_s <= x"ef";
        wait until rising_edge(txDataReady_s);

        wait for 10*CLK_PERIOD;

        -- Read command
        --txDataReady_s <= '1';
        usrRst_s <= '0';
        txData_s <= BYTE_CMD_RD_C;
        --rxDataValid_s <= '1';
        wait until rising_edge(txDataReady_s);

        -- Address 0xc001c0de; data 0x00000000
        --txDataReady_s <= '1';
        usrRst_s <= '0';
        --rxDataValid_s <= '1';
        txData_s <= x"00";
        wait until rising_edge(txDataReady_s);
        txData_s <= x"00";
        wait until rising_edge(txDataReady_s);
        txData_s <= x"00";
        wait until rising_edge(txDataReady_s);
        txData_s <= x"00";
        wait until rising_edge(txDataReady_s);

        wait for 10*CLK_PERIOD;

        -- Read command
        --txDataReady_s <= '1';
        usrRst_s <= '0';
        txData_s <= BYTE_CMD_RD_C;
        --rxDataValid_s <= '1';
        wait until rising_edge(txDataReady_s);

        -- Address 0xc001c0de; data 0x0000000f
        --txDataReady_s <= '1';
        usrRst_s <= '0';
        --rxDataValid_s <= '1';
        txData_s <= x"00";
        wait until rising_edge(txDataReady_s);
        txData_s <= x"00";
        wait until rising_edge(txDataReady_s);
        txData_s <= x"00";
        wait until rising_edge(txDataReady_s);
        txData_s <= x"0f";
        wait until rising_edge(txDataReady_s);

        -- End simulation
        wait for 4*CLK_PERIOD;
        wait;
    end process;


end Behavioral;

