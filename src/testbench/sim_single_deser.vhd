library ieee;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

entity sim_single_deser is
end sim_single_deser;

architecture Behavioral of sim_single_deser is

    constant NBITS : integer := 3;
    constant clk_period : time := 100 ns;
    constant clk_data_period : time := 3 ns;

    signal digital_s    : std_logic_vector(31 downto 0);
    signal par_s        : std_logic_vector(7 downto 0);

    signal clk_i        : std_logic;
    signal clk_ser_s    : std_logic;
    signal clk_ser_90_s : std_logic;
    signal clk_100M_s   : std_logic;
    signal clk_spi_s    : std_logic;

    signal rst_s        : std_logic;
    signal en_s        : std_logic;
    signal ser_s        : std_logic;

    component single_deserializer
    port(
            rst_i        : in  std_logic;
            en_i         : in  std_logic;
            ser_i        : in  std_logic;
            clk_ser_i    : in  std_logic;
            clk_ser_90_i : in  std_logic;
            par_o        : out std_logic_vector(7 downto 0)
        );
    end component;


begin
    inst_clk_gen : entity work.clock_gen_wiz_wrapper
    port map(
                clk_ser_90_o_0 => clk_ser_90_s,
                clk_ser_o_0    => clk_ser_s,
                clk_100M_o_0   => clk_100M_s,
                clk_spi_o_0    => clk_spi_s,
                sys_clock      => clk_i,
                reset_rtl      => '0'
            );

    inst_single_deser : single_deserializer
    port map(
            rst_i        => rst_s,
            en_i         => en_s,
            ser_i        => ser_s,
            clk_ser_i    => clk_i,
            --clk_ser_i    => clk_ser_s,
            clk_ser_90_i => clk_ser_90_s,
            par_o        => par_s
        );

    -- Clock genenration
    process
    begin
        clk_i <= '1';
        wait for clk_period/2;
        clk_i <= '0';
        wait for clk_period/2;
    end process;

--    -- Simulation
--    process
--    begin
--        digital_s <= x"aa";
--        ser_s <= '0';
--        rst_s <= '1';
--        -- Wait for MMCM to begin
--        wait for 6648.5 ns;
--        rst_s <= '0';
--        for n in digital_s'range loop
--            ser_s <= digital_s(n);
--            wait for clk_data_period;
--        end loop;
--
--        wait;
--    end process;

    -- Simulation
    en_s <= not rst_s;


    process
    begin
        digital_s <= x"deadbeef";
        ser_s <= '0';
        rst_s <= '1';
        -- Wait for MMCM to begin
        wait for 8*clk_period+clk_period/4;
        rst_s <= '0';
        wait for 2*14*clk_period/3;
        for n in digital_s'range loop
            ser_s <= digital_s(n);
            --wait for clk_data_period;
            wait for clk_period/2;
        end loop;

        wait;
    end process;



end Behavioral;

