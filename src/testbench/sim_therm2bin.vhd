library ieee;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;


entity therm2bin_tb is
    end therm2bin_tb;

architecture behav of therm2bin_tb is
    constant NBITS : integer := 3;
    -- Timings
    constant clk_period : time := 10 ns;

    -- Components
    component therm2bin
        generic(N : integer := 1);
        port(
                Din    : in  std_logic_vector((2**N-2) downto 0);
                Dout   : out std_logic_vector(N-1 downto 0)
            );
    end component;

    -- Signals
    signal thermo_s : std_logic_vector((2**NBITS-2) downto 0);
    signal binary_s : std_logic_vector(NBITS-1 downto 0);

begin
    --  Component instantiation.
    therm2bin_0: therm2bin 
    generic map (N => NBITS)
    port map (
                 Din => thermo_s,
                 Dout => binary_s
             );

    -- Simulation
    process
    begin
        thermo_s <= (others => '0');
        wait for clk_period/2;
        for n in thermo_s'range loop
            thermo_s <= thermo_s(thermo_s'high-1 downto 0) & '1';
            wait for clk_period/2;
        end loop;

        for n in thermo_s'range loop
            thermo_s <= '0' & thermo_s(thermo_s'high downto 1);
            wait for clk_period/2;
        end loop;
        wait;
    end process;
end behav;
