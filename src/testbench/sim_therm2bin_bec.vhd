library ieee;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;


entity sim_therm2bin_bec is
    end sim_therm2bin_bec;

architecture behav of sim_therm2bin_bec is
    constant NBITS : integer := 3;
    -- Timings
    constant clk_period : time := 10 ns;

    -- Signals
    signal thermo_s : std_logic_vector((2**NBITS-2) downto 0);
    signal thermo_i : std_logic_vector((2**NBITS-2) downto 0);
    signal binary_s : std_logic_vector(NBITS-1 downto 0);

begin
    --  Component instantiation.
    therm2bin_0 : entity work.therm2bin_bec
    generic map (N => NBITS)
    port map (
                 Din => thermo_i,
                 Dout => binary_s
             );

    -- Simulation
    process
    begin
        -- Normal cycle
        thermo_s <= (others => '0');
        thermo_i <= thermo_s;
        wait for clk_period/2;
        for n in thermo_s'range loop
            thermo_s <= thermo_s(thermo_s'high-1 downto 0) & '1';
            thermo_i <= thermo_s;
            wait for clk_period/2;
        end loop;

        for n in thermo_s'range loop
            thermo_s <= '0' & thermo_s(thermo_s'high downto 1);
            thermo_i <= thermo_s;
            wait for clk_period/2;
        end loop;

        -- 1st order error
        thermo_s <= (others => '0');
        thermo_i <= "110" & (thermo_s'high-3 downto 0 => '1') and thermo_s;
        wait for clk_period/2;
        for n in thermo_s'range loop
            thermo_s <=  thermo_s(thermo_s'high-1 downto 0) & '1';
            thermo_i <= "110" & (thermo_s'high-3 downto 0 => '1') and thermo_s;
            wait for clk_period/2;
        end loop;

        thermo_i <= "110" & (thermo_s'high-3 downto 0 => '1') and thermo_s;
        for n in thermo_s'range loop
            thermo_s <= '0' & thermo_s(thermo_s'high downto 1);
            thermo_i <= "110" & (thermo_s'high-3 downto 0 => '1') and thermo_s;
            wait for clk_period/2;
        end loop;

        -- 2nd order error
        thermo_s <= (others => '0');
        thermo_i <= "1100" & (thermo_s'high-4 downto 0 => '1') and thermo_s;
        wait for clk_period/2;
        for n in thermo_s'range loop
            thermo_s <=  thermo_s(thermo_s'high-1 downto 0) & '1';
            thermo_i <= "1100" & (thermo_s'high-4 downto 0 => '1') and thermo_s;
            wait for clk_period/2;
        end loop;

        for n in thermo_s'range loop
            thermo_s <= '0' & thermo_s(thermo_s'high downto 1);
            thermo_i <= "1100" & (thermo_s'high-4 downto 0 => '1') and thermo_s;
            wait for clk_period/2;
        end loop;
        wait;
    end process;
end behav;
