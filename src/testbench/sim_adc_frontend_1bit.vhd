library ieee;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

library work;
use work.UtilityPkg.all;

entity sim_adc_frontend_1bit is
end sim_adc_frontend_1bit;

architecture Behavioral of sim_adc_frontend_1bit is

    constant NBITS          : integer := 1;
    constant clk_period     : time    := 5 ns;
    constant reg_clk_period : time    := 50 ns;

    constant ADDR_WIDTH : integer := 32;
    constant DATA_WIDTH : integer := 32;

    signal threshold_s  : std_logic_vector((2**NBITS-2) downto 0);
    signal analog_s     : std_logic_vector((2**NBITS-2) downto 0);
    signal digital_s    : std_logic_deser(NBITS-1 downto 0);
    signal clk_s        : std_logic;
    signal rst_s        : std_logic;

    signal thermo_s     : std_logic_vector((2**NBITS-2) downto 0);
    signal not_thermo_s : std_logic_vector((2**NBITS-2) downto 0);

    signal regclk_s    : std_logic;
    signal regAck_s    : std_logic;
    signal regReq_s    : std_logic;
    signal regOp_s     : std_logic;
    signal regAddr_s   : std_logic_vector(ADDR_WIDTH-1 downto 0);
    signal regWrData_s : std_logic_vector(DATA_WIDTH-1 downto 0);
    signal regRdData_s : std_logic_vector(DATA_WIDTH-1 downto 0);

    signal data_s      : std_logic_vector(DATA_WIDTH-1 downto 0);

begin

    inst_adc_fe : entity work.adc_frontend
    generic map(
                   N           => NBITS,
                   ADDR_WIDTH  => 32,
                   DATA_WIDTH  => 32,
                   BASE_OFFSET => 16#00#,
                   BUFF_SIZE   => 16
               )
    port map(
                rst_i       => rst_s,
                threshold_i => threshold_s,
                analog_i    => analog_s,
                clk_i       => clk_s,
                -- Registers
                regClk    => regclk_s,
                regAddr   => regAddr_s,
                regWrData => regWrData_s,
                regRdData => regRdData_s,
                regReq    => regReq_s,
                regOp     => regOp_s,
                regAck    => regAck_s
            );

    not_thermo_s <= not thermo_s;
    threshold_s  <= not_thermo_s;
    analog_s     <= thermo_s;

    -- Clock genenration
    process
    begin
        clk_s <= '0';
        wait for clk_period/2;
        clk_s <= '1';
        wait for clk_period/2;
    end process;

    -- reg Clock genenration
    process
    begin
        regclk_s <= '0';
        wait for reg_clk_period/2;
        regclk_s <= '1';
        wait for reg_clk_period/2;
    end process;

    -- Reset generation
    process
    begin
        rst_s <= '1';
        wait for 10*clk_period;
        rst_s <= '0';
        wait;
    end process;

    -- Input signal genenration
    data_s <= x"cafec0de";
    process
    begin
        thermo_s <= (others => '0');
        --wait for clk_period/2;
        wait for 19*clk_period-clk_period/4;
        for t in 0 to 10 loop
            for n in data_s'range loop
                thermo_s(0) <= data_s(n);
                wait for clk_period/2;
            end loop;
        end loop;

    end process;

    -- Capture signal and read
    process
    begin
        -- Wait for ISERDESE to start
        wait for 17*clk_period;

        -- Capture data
        regAddr_s   <= (others => '1');
        regWrData_s <= (others => '0');
        regReq_s    <= '0';
        regOp_s     <= '0';
        wait for 30*clk_period;

        -- Read buffer
        for n in 0 to 30 loop
            regAddr_s   <= (others => '0');
            regReq_s    <= '1';
            regOp_s     <= '0';
            wait for reg_clk_period;
            regReq_s    <= '0';
            wait for reg_clk_period;
        end loop;
    end process;


end Behavioral;

