library ieee;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

entity single_deserializer is
    port(
            rst_i       : in  std_logic;
            en_i        : in  std_logic;
            ser_i       : in  std_logic;
            clk_ser_i : in  std_logic;
--            clk_ser_p_i : in  std_logic;
--            clk_ser_n_i : in  std_logic;
            clk_div_i   : in  std_logic;
            par_o       : out std_logic_vector(7 downto 0)
        );
end single_deserializer;

architecture arch of single_deserializer is
    signal clk_ser_s     : std_logic;
    signal clk_div_s     : std_logic;
    signal clk_ser_p_s   : std_logic;
    signal clk_ser_n_s   : std_logic;
    signal not_clk_ser_i : std_logic;
    signal SHIFTOUT1     : std_ulogic;
    signal SHIFTOUT2     : std_ulogic;
begin

    not_clk_ser_i <= not clk_ser_p_s;
    clk_ser_p_s <= clk_ser_i;
    clk_ser_n_s <= not_clk_ser_i;

--    -- Handle multi-regional clk
--    clk_mr_inst : BUFIO
--    port map(
--                O => clk_ser_s,
--                I => clk_ser_i
--            );
--
--    clk_div_inst : BUFR
--    generic map(
--                BUFR_DIVIDE => "4",
--                SIM_DEVICE => "7SERIES"
--               )
--    port map(
--                CE => '1',
--                CLR => '0',
--                O => clk_div_s,
--                I => clk_ser_i
--            );

    inst_ISERDESE2 : ISERDESE2 
    generic map(
                   DATA_RATE         => "DDR",            -- DDR, SDR
                   DATA_WIDTH        => 8,                -- Parallel data width (2-8,10,14)
                   DYN_CLKDIV_INV_EN => "FALSE",          -- Enable DYNCLKDIVINVSEL inversion (FALSE,TRUE)
                   DYN_CLK_INV_EN    => "FALSE",          -- Enable DYNCLKINVSEL inversion (FALSE,TRUE)

                   -- INIT_Q1-INIT_Q4                     : Initial value on the Q outputs (0/1)
                   INIT_Q1           => '0',              
                   INIT_Q2           => '0',
                   INIT_Q3           => '0',
                   INIT_Q4           => '0',
                   INTERFACE_TYPE    => "NETWORKING",     -- MEMORY, MEMORY_DDR3, MEMORY_QDR, NETWORKING, OVERSAMPLE
                   IOBDELAY          => "NONE",           -- NONE, BOTH, IBUF,
                   OFB_USED          => "FALSE",          -- Select OFB path(FALSE,TRUE)
                   SERDES_MODE       => "MASTER",         -- MASTER,SLAVE

                   -- SRVAL_Q1-SRVAL_Q4 :  Q output values when SR isused (0/1)
                   SRVAL_Q1          => '0',
                   SRVAL_Q2          => '0',
                   SRVAL_Q3          => '0',
                   SRVAL_Q4          => '0'
               )
    port map(
                --O=>O, -- 1-bit output : Combinatorial output
               
                -- Q1-Q8 : 1-bit (each) output :Registered data outputs
                Q1 => par_o(0),
                Q2 => par_o(1),
                Q3 => par_o(2),
                Q4 => par_o(3),
                Q5 => par_o(4),
                Q6 => par_o(5),
                Q7 => par_o(6),
                Q8 => par_o(7),
              
                -- SHIFTOUT1-SHIFTOUT2 : 1-bit (each) output: Data width expansion output ports
                --SHIFTOUT1 => SHIFTOUT1,
                --SHIFTOUT2 => SHIFTOUT2,
                BITSLIP => '0', -- 1-bit input : The BITSLIP pin performs a Bitslip operation synchronous to
                                -- CLKDIV when asserted (active High). Subsequently, the data seen on the
                                -- Q1 to Q8 output ports will shift, as in a barrel-shifter operation, one
                                -- position every time Bitslip is invoked (DDR operation is different from
                                -- SDR).
               
                -- CE1, CE2 : 1-bit (each) input : Data register clock enable inputs
                CE1     => en_i,
                CE2     => en_i,
                CLKDIVP => '0',  -- 1-bit input : TBD
               
                -- Clocks : 1-bit (each) input : ISERDESE2 clock input ports
                CLK    => clk_ser_p_s,    -- 1-bit input : High-speed clock
                CLKB   => clk_ser_n_s,    -- 1-bit input : High-speed secondary clock
                --CLKDIV => clk_div_s,      -- 1-bit input : Divided clock

                CLKDIV => clk_div_i,      -- 1-bit input : Divided clock
                --CLKDIV => clk_ser_90_p_s, -- 1-bit input : Divided clock
                OCLK   => '0',            -- 1-bit input : High speed output clock used when INTERFACE_TYPE="MEMORY"
                OCLKB  => '0',            -- 1-bit input : High speed negative edge output clock
                
                -- Dynamic Clock Inversions : 1-bit (each) input : Dynamic clock inversion pins to switch clock polarity
                DYNCLKDIVSEL => '0', -- 1-bit input : Dynamic CLKDIV inversion
                DYNCLKSEL    => '0', -- 1-bit input : Dynamic CLK/CLKB inversion

                -- Input Data : 1-bit (each) input : ISERDESE2 data input ports
                D    => ser_i, -- 1-bit input : Data input
                DDLY => '0',   -- 1-bit input : Serial data from IDELAYE2
                OFB  => '0',   -- 1-bit input : Data feedback from OSERDESE2
                RST  => rst_i, -- 1-bit input : Active high asynchronous reset

                -- SHIFTIN1-SHIFTIN2 : 1-bit (each) input : Data width expansion input ports
                SHIFTIN1 => '0',
                SHIFTIN2 => '0'
            );

end Architecture arch;
