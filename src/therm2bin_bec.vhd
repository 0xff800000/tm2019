library ieee;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity therm2bin_bec is
    generic(N : integer := 1);
    port(
            Din    : in  std_logic_vector((2**N-2) downto 0);
            Dout   : out std_logic_vector(N-1 downto 0)
        );
end therm2bin_bec;

architecture arch of therm2bin_bec is
    signal thermo : std_logic_vector((2**N-2) downto 0);
begin
    -- Bubble Error Correction
    process(Din)
    begin
        for k in 0 to thermo'high-2 loop
            thermo(k) <= Din(k) or Din(k+1) or Din(k+2);
        end loop;
        thermo(thermo'high-1) <= Din(thermo'high-1) or Din(thermo'high);
        thermo(thermo'high) <= Din(thermo'high);
    end process;

    -- Highest '1'
    process(thermo)
        variable bin : unsigned(Dout'range);
    begin
        bin := (others => '0');
        for i in 0 to thermo'high loop
            if thermo(i) = '1' then
                bin := to_unsigned(i + 1,bin'length);
            end if;
        end loop;
        Dout <= std_logic_vector(bin);
    end process;

end architecture arch;
