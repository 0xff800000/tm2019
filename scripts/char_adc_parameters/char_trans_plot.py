import adc
import time
import sys
import pdb
import json
import numpy as np
import matplotlib.pyplot as plt

f = open('adc_parameters.json', 'r')
data = json.load(f)
TnomK = data['TnomK']
TK = data['TK']
G = data['G']
Vos = data['Vos']
res = data['res']
vref = data['vref']
full_scale = 2**res-1


# Compute static gain and offset (terminal based)
Q = TnomK[1] - TnomK[0]
G = Q*(2**res - 2) / (TK[str(2**res-1)] - TK[str(1)])
T1 = TnomK[0]
V_os = T1 - G*TK[str(1)]
print('Gain {}, offset {}'.format(G,V_os))

TK = [ TK[str(i)] for i in range(1,len(TnomK)+1)]

TK = [0] + TK + [3]
TnomK = [0] + TnomK + [3]

f = plt.figure(figsize=(5,3))

code = [ i for i in range(0,len(TnomK)-1)]
code = code + [code[-1]]
print(code)
print(TnomK)
print(TK)
plt.step(TK,code, '-', ms=5, where='post',label='measured') 
plt.step(TnomK,code, 'k-', lw=3, where='post',alpha=0.3,label='ideal')
plt.xlabel('Input voltage [V]')
plt.ylabel('Output code')
plt.grid()
plt.legend(framealpha=1,loc='lower right')
plt.xlim(-0.2, TK[-2]+0.2)
plt.show()
f.savefig("tran.pdf", bbox_inches='tight')

# Save json
f = open('adc_parameters.json', 'r')
adc_data = json.load(f)
f.close()
adc_data['Q']=Q
adc_data['G']=G
adc_data['V_os']=V_os

f = open('adc_parameters.json', 'w')
json.dump(adc_data, f)
