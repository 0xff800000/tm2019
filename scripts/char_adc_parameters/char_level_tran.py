import adc
import time
import sys
import pdb
import json
import numpy as np
import matplotlib.pyplot as plt

ad = adc.ADC()
TnomK = ad.configThresholdDefault(start=0.2,end=2).tolist()

#pdb.set_trace()

TK = {}
eps = 0.1

print('test',ad.full_scale)
volt_level = 0
ad.writeDACvalue(7,int(volt_level))
ad.setSampling(True)
step = 0.02 / (ad.vref/(2**ad.dac_res))
# Measure transition level for each kcode
for kcode in range(1, ad.full_scale):

    while True:
        ad.setSampling(False)
        res = ad.readoutBuffer(2,1)
        ad.setSampling(True)
        data = ad.convert3BitBuffer(res)
        if 15 in data:
            continue

        print(ad.convDAC2volt(volt_level))
        print(data)

        if kcode in data:
            print('break')
            break

        volt_level += step
        ad.writeDACvalue(7,int(volt_level))
        time.sleep(0.5)

    TK[kcode] = ad.convDAC2volt(volt_level)
    print('#'*10)
    print('Transition found for {} : {}'.format(kcode,TK[kcode]))
    print('#'*10)

print(TK)

# Save json
f = open('adc_parameters.json', 'r')
adc_data = json.load(f)
f.close()
adc_data['TK']=TK
adc_data['res']=ad.adc_res
adc_data['vref']=ad.vref
adc_data['TnomK']=TnomK

f = open('adc_parameters.json', 'w')
json.dump(adc_data, f)
