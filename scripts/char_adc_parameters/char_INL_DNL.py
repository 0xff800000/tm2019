import adc
import time
import sys
import pdb
import json
import numpy as np
import matplotlib.pyplot as plt

f = open('adc_parameters.json', 'r')
data = json.load(f)
G = data['G']
Vos = data['Vos']
TK = data['TK']
Q = data['Q']
res = data['res']
vref = data['vref']
TnomK = data['TnomK']


# Compute error epsilon
epsK = []
for k in range(1,len(TnomK)):
    res = (G * TK[str(k)] + Vos - TnomK[k-1]) / Q - 1
    print('t1,',(G * TK[str(k)] + Vos - TnomK[k-1])/Q)
    epsK.append(res)

INLK = [(TnomK[k-1] - TK[str(k)]) / Q for k in range(1,len(TnomK))]

print(Q)
print(epsK)
print(TnomK)
print(TK)

# INL
#INLK = np.array(epsK)
#INLK = np.array(epsK) / (2**res * Q)
###INLK = np.array(epsK) / vref
print(INLK)

code = range(1,len(INLK)+1)

f = plt.figure(figsize=(5,3))
plt.plot(code,INLK, '-o', ms=5)
#plt.title('Integral Non-Linearity [LSB]')
plt.xlabel('code')
plt.ylabel('INL [LSB]')
plt.grid()
plt.ylim(-1, 1)
plt.show()
f.savefig("INL.pdf", bbox_inches='tight')

# DNL
DNLK = []
for k in range(1,len(TK)):
    r = G * (TK[str(k+1)] - TK[str(k)])
    r = (r - Q) / Q
    DNLK.append(r)
DNLK = np.array(DNLK)

code = range(1,len(DNLK)+1)

f = plt.figure(figsize=(5,3))
plt.plot(code, DNLK, '-o', ms=5)
#plt.title('Differential Non-Linearity [LSB]')
plt.xlabel('code')
plt.ylabel('DNL [LSB]')
plt.grid()
plt.ylim(-1, 1)
plt.show()
f.savefig("DNL.pdf", bbox_inches='tight')

# Absolute accuracy error
#r = [TK[str(k)] - Q*(k-1)-TK["1"] for k in range(len(TK))]
#AAE = np.max(r) / Q
#print(AAE)
