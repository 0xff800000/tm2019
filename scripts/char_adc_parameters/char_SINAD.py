import sys
import json
import pdb
import matplotlib.pyplot as plt
import numpy as np
import scipy.optimize

data = np.loadtxt("sine_fit.txt")
t = range(len(data))

f = open('adc_parameters.json', 'r')
adc_data = json.load(f)
f.close()

# 4 parameter sine fit of input : A0 f0 B0 C0
#def func(x, A0,f0,B0,C0):
#    return A0*np.cos(2*np.pi*f0*x) + B0*np.sin(2*np.pi*f0*x) + C0
#
#print(scipy.optimize.curve_fit(func, t, data))

# Guess
A0g = 5
f0g = 0.000700
B0g = 0
C0g = 0


optimize_func = lambda x: x[0]*np.cos(2*np.pi*x[1]*t) + x[2]*np.sin(2*np.pi*x[1]*t) + x[3] - data
res = scipy.optimize.least_squares(optimize_func, [A0g, f0g, B0g, C0g])
print(res)
#res = scipy.optimize.leastsq(optimize_func, [1, 1, 1, 1])
A0, f0, B0, C0 = res['x']

fine_t = np.arange(0,max(t),1)
data_fit = A0*np.cos(2*np.pi*f0*fine_t) + B0*np.sin(2*np.pi*f0*fine_t) + C0
data_guess = A0g*np.cos(2*np.pi*f0g*fine_t) + B0g*np.sin(2*np.pi*f0g*fine_t) + C0g

f = plt.figure(figsize=(5,3))
plt.plot(t, data, '.', ms=5, label='samples')
plt.plot(fine_t, data_fit, label='fit')
#plt.plot(fine_t, data_guess)
#plt.ylim(-0.1, 7.1)
plt.xlabel('# sample')
plt.ylabel('Digital value')
plt.grid()
plt.show()

f.savefig("sine_fit_sinad.pdf", bbox_inches='tight')

# NAD (eq. 65 IEEE)
x = data
xp = A0*np.cos(2*np.pi*f0*t) + B0*np.sin(2*np.pi*f0*t) + C0
M = len(data)
NAD = np.sqrt(1/M * np.sum((x-xp)**2))
pdb.set_trace()
Arms = 5 / np.sqrt(2)

# SINAD (eq. 66 IEEE)
SINAD = Arms / NAD
SINAD_dBFS = (10*np.log(SINAD))
print('SINAD %s dBFS' % SINAD_dBFS)

# SNR (eq. 68, 69 IEEE)
THD = float(adc_data['THD'])
print(NAD**2)
etha = np.sqrt(NAD**2 - (Arms**2)*(THD**2))
print(etha)
SNR = Arms / etha
SNR_dBFS = 10*np.log(SNR)
print('SNR %s dBFS' % SNR_dBFS)

# ENOB (eq. 70 IEEE)
FSR = 2**adc_data['res']
G = adc_data['G']
ENOB = np.log2((FSR / G) / (NAD * np.sqrt(12)))
print('ENOB %s bit' % ENOB)

# Save json
adc_data['Arms'] = Arms
adc_data['NAD'] = NAD
adc_data['SINAD_lin'] = SINAD
adc_data['SINAD_dBFS'] = SINAD_dBFS
adc_data['SNR_lin'] = SNR
adc_data['SNR_dBFS'] = SNR_dBFS
adc_data['ENOB'] = ENOB

f = open('adc_parameters.json', 'w')
json.dump(adc_data, f)
