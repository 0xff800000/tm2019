import serial
import time
import os
import numpy as np
import pdb

class ADC(serial.Serial):
    def __init__(self,
            headless=False,
            port='',
            baudrate=115200,
            timeout=1,
            addr_size=32,
            data_size=32,
            dac_res=12,
            buff_size=1024,
            vref=2.5
            ):
        if not headless:
            if port == '':
                port = self.scanPorts()
            serial.Serial.__init__(self, port=port, baudrate=baudrate, timeout=timeout)
            print('Opening serial on port ', self.name)
        self.addr_size_bytes = int(addr_size / 8)
        self.data_size_bytes = int(data_size / 8)
        self.dac_res = dac_res
        self.buffer_size = buff_size
        self.vref = vref
        self.readRegister(0xff,fail=True) # Start sampling
        self.adc_res = 3
        self.full_scale = 2**self.adc_res
        self.fs = 927.782e6

    def scanPorts(self):
        path = ''
        for i in range(0,256):
            path = '/dev/ttyUSB{}'.format(i)
            if os.path.exists(path):
                break
        return path

    def serialize_data(self, data, size_bytes):
        ser_data = []
        for b in range(0,size_bytes):
            shift = size_bytes-b-1
            ser_data.append((data & 0xff << 8*shift) >> 8*shift)
        return ser_data

    def readRegister(self, addr, fail=False):
        command = [ord('r')] + self.serialize_data(addr,self.addr_size_bytes)
        while True:
            try:
                self.write(bytearray(command))
                self.flush()
                data_raw = [ord(self.read()) for i in range(0,self.data_size_bytes)]
            except Exception as e:
                if fail==True:
                    return []
                print(e)
                #self.write(10*'\0')
                self.flush()
                self.reset_input_buffer()
                continue
            break
        ret = 0
        for i,byte in enumerate(data_raw):
            ret |= byte << (self.data_size_bytes-1-i)*8
        return ret

    def streamRegister(self, fail=False):
        command = [ord('s')]
        while True:
            try:
                self.write(bytearray(command))
                self.flush()
                data_raw = [ord(self.read()) for i in range(0,self.data_size_bytes)]
            except Exception as e:
                if fail==True:
                    return []
                print(e)
                #self.write(10*'\0')
                self.flush()
                self.reset_input_buffer()
                continue
            break
        ret = 0
        for i,byte in enumerate(data_raw):
            ret |= byte << (self.data_size_bytes-1-i)*8
        return ret


    def writeRegister(self, addr, data):
        command = [ord('w')]
        command += self.serialize_data(addr, self.addr_size_bytes)
        command += self.serialize_data(data, self.data_size_bytes)
        while True:
            self.write(bytearray(command))
            self.flush()
            rd_data = self.readRegister(addr)
            if data != rd_data:
                print('Error : *({}) == {} != {}'.format(hex(addr),hex(rd_data),hex(data)))
                continue
            else:
                break

    # Write raw value
    def writeDAC(self,channel,value):
        # MCP4911
        #self.writeRegister(0x11,channel)
        #self.writeRegister(0x12,value)
        #self.writeRegister(0x10,value)

        # AD5672
        data = 0
        data |= (0x3 << 20) # Command : write to Input Register
        data |= ((channel & 0xf) << 16) # Address
        data |= (value & 0xffff) # Data
        print(hex(data))
        self.writeRegister(0x10,data)
        

    # Write sample value
    def writeDACvalue(self,channel,value):
        # MCP4911
        #dacvalue = (value & 0xffff) << (12 - self.dac_res)

        # AD5672
        dacvalue = (value & 0xffff) << 4
        self.writeDAC(channel,dacvalue)

    def setThresholdVolt(self,channel,voltage):
        dn = int((voltage*(2**self.dac_res))/self.vref) - 1
        print(dn)
        self.writeDACvalue(channel,dn)

    def configThresholdDefault(self,start=0.25,end=2.1):
        TnomK = np.linspace(start,end,self.full_scale-1)
        print(TnomK)
        for c,level in enumerate(TnomK):
            print('Set threshold of channel {} to {} V'.format(c,level))
            self.setThresholdVolt(c,level)
            time.sleep(0.1)
        return TnomK

    def convDAC2volt(self,dac):
        return dac * self.vref/(2**self.dac_res)

    def setCtrlReg(self,reg):
        self.writeRegister(0x00,reg)
        
    def resetADC(self):
        self.setCtrlReg(2)

    def setSampling(self,state):
        if state == True:
            self.setCtrlReg(0)
        else:
            self.setCtrlReg(1)

    def readoutBuffer(self,channel,size=-1):
        if size==-1:
            size=self.buffer_size
        samples = self.readRegister(channel)
        buff = [samples]
        bytes_read = 0
        while bytes_read != self.buffer_size and bytes_read<size:
            samples = self.streamRegister()
            if samples == 0xffffffff:
                break
            buff.append(samples)
            bytes_read += 1
        print(bytes_read)
        return buff

    def convert1BitBuffer(self,buff):
        buff_bits = np.array(buff,dtype='>u1')
        buff_bits = np.unpackbits(buff_bits)
        return buff_bits

    def convert2BitBuffer(self,buff):
        #pdb.set_trace()
        # Cast from big-endian uint16 to uint8
        buff_bits = np.array(buff,dtype='>u2')
        buff_bits = buff_bits.view('>u1')
        buff_bits = buff_bits.reshape(-1,2)

        # Unpack bits : every odd rows are MSB and every even rows are LSB
        buff_bits = np.unpackbits(buff_bits, axis=1)
        buff_bits = buff_bits.reshape(-1,8)
        buff_bits = buff_bits[::2]*2 + buff_bits[1::2]

        buff_bits = buff_bits.reshape(1,-1)[0]
        return buff_bits

    def convert3BitBuffer(self,buff):
        # Cast from big-endian uint32 to uint8
        #pdb.set_trace()
        buff_bits = np.array(buff,dtype='>u4')
        buff_bits = buff_bits.view('>u1')
        buff_bits = buff_bits.reshape(-1,4)

        # Unpack bits : 
        buff_bits = np.unpackbits(buff_bits, axis=1)
        buff_bits = buff_bits.reshape(-1,8)
        buff_bits = buff_bits[0::4]*8 + buff_bits[1::4]*4 + buff_bits[2::4]*2+ buff_bits[3::4]

        buff_bits = buff_bits.reshape(1,-1)[0]
        return buff_bits
