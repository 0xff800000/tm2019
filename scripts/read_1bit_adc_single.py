import adc
import time
import sys
import pdb
import matplotlib.pyplot as plt

print(sys.argv[1])

ad = adc.ADC()
ad.writeDAC(0,1*1024)
res = ad.readoutBuffer()
print([hex(r) for r in res])
data = ad.convert1BitBuffer(res)
print(data[:20])

f = plt.figure(figsize=(5,3))
plt.plot(data)
plt.ylim(-0.1,1.1)
plt.xlabel('# sample')
plt.ylabel('Digital value')
plt.show()
f.savefig("{}.pdf".format(sys.argv[1]), bbox_inches='tight')
