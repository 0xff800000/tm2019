import adc
import time
import sys
import pdb
import glob
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

threshold = 2.5
pulse_frequency = 5e6
fs = 463.88889e6 * 2
min_diff = 4

def getThresholdCross(signal):
    indexes = []
    for i in range(1,len(signal)):
        if signal[i - 1] < threshold and signal[i] > threshold:
            if len(indexes) > 0:
                if np.abs(i - indexes[-1]) <= min_diff:
                    continue
            indexes.append(i)
    return indexes

def interpolate(indexes,data):
    times = []
    for i in indexes:
        slope = data[i]-data[i-1]
        time = i-1 + (threshold-data[i-1])/slope
        times.append(time)
    return times

diff_hist = []

for filename in glob.glob('data_*'):
    data = np.load(filename)

    # Get thresholds
    thresholds_ch1 = getThresholdCross(data[0])
    thresholds_ch2 = getThresholdCross(data[1])
    thresholds_ch1 = interpolate(thresholds_ch1,data[0])
    thresholds_ch2 = interpolate(thresholds_ch2,data[1])

    # Sanitize missing
    if len(thresholds_ch1) != len(thresholds_ch2):
        print('Error threshold vectors different')
        if len(thresholds_ch1) < len(thresholds_ch2):
            thresholds_ch2 = thresholds_ch2[:-1]
        if len(thresholds_ch1) > len(thresholds_ch2):
            thresholds_ch1 = thresholds_ch1[:-1]

    # Get difference
    diff = np.array(thresholds_ch2) - np.array(thresholds_ch1)
    diff_hist = np.append(diff_hist,diff)

formatter = mpl.ticker.EngFormatter()
print('Number of pules {}'.format(len(diff_hist)))
print('Standard dev {}S'.format(formatter(np.std(diff_hist))))
print('Sampling freq {}Hz'.format(formatter(fs)))
print('Sampling period {}s'.format(formatter(1/fs)))

# Plot histogram
f = plt.figure(figsize=(5,3))
diff_hist *= 1/fs
weights = np.ones_like(diff_hist)/float(len(diff_hist))
plt.hist(diff_hist,bins=4,weights=weights,edgecolor='black', linewidth=1)

mean_str = formatter(np.mean(diff_hist))
std_str = formatter(np.std(diff_hist))
plt.title(r'$\mu = {} s, \quad \sigma = {} s$'.format(mean_str,std_str))
plt.xlabel('Time [s]')
plt.ylabel('Density')
plt.show()
f.savefig("timing_resol.pdf", bbox_inches='tight')
