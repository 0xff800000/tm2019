import adc
import time
import sys
import pdb
import timeit
import numpy as np
import matplotlib.pyplot as plt

threshold = 2.5
pulse_frequency = 5e6
fs = 463.88889e6 * 2
min_diff = 4

def getThresholdCross(signal):
    indexes = []
    for i in range(1,len(signal)):
        if signal[i - 1] < threshold and signal[i] > threshold:
            if len(indexes) > 0:
                if np.abs(i - indexes[-1]) <= min_diff:
                    continue
            indexes.append(i)
    return indexes

def interpolate(indexes,data):
    times = []
    for i in indexes:
        slope = data[i]-data[i-1]
        time = i-1 + (threshold-data[i-1])/slope
        times.append(time)
    return times

filename = sys.argv[1]

data = np.load(filename)

thresholds_ch1 = getThresholdCross(data[0])
thresholds_ch2 = getThresholdCross(data[1])
thresholds_ch1 = interpolate(thresholds_ch1,data[0])
thresholds_ch2 = interpolate(thresholds_ch2,data[1])

f = plt.figure(figsize=(5,3))
# Plot channels
t = np.arange(0,len(data[0])) * 1/fs
plt.plot(t,data[0], '-o', ms=3)
plt.plot(t,data[1], '-o', ms=3)
# Plot threshold crossings
plt.plot(np.array(thresholds_ch1)*1/fs, np.ones(len(thresholds_ch1)) * threshold, '^', ms=9)
plt.plot(np.array(thresholds_ch2)*1/fs, np.ones(len(thresholds_ch2)) * threshold, '^', ms=9)
# Plot threshold line
plt.plot([min(t),max(t)], [threshold, threshold], '--')
# Zoom on first pulse
plt.ylim(-0.1, 4.1)
plt.xlim((thresholds_ch1[0]-10) * 1/fs, (thresholds_ch1[0]+60) * 1/fs)
plt.xlabel('Time [s]')
plt.ylabel('Digital value')
plt.show()
f.savefig("pulse_plot.pdf", bbox_inches='tight')
