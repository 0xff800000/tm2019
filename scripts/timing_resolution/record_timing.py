import adc
import time
import sys
import pdb
import glob
import numpy as np
import matplotlib.pyplot as plt

# Pulse signal : 3 Vpp amplitude, 5 MHz frequency, 30 ns width, 0.298 V offset (on bias tee)

ad = adc.ADC()
ad.setSampling(True)
expect_size = 1000

# Set threshold
ad.configThresholdDefault()

# Get data
while True:
    ad.setSampling(False)
    res_ch1 = ad.readoutBuffer(1,expect_size)
    res_ch2 = ad.readoutBuffer(2,expect_size)
    ad.setSampling(True)

    if len(res_ch1) >= expect_size and len(res_ch2) >= expect_size:
        break

data_ch1 = ad.convert3BitBuffer(res_ch1)
data_ch2 = ad.convert3BitBuffer(res_ch2)

data = np.array([data_ch1,data_ch2])

# Save with filename with highest sequence nb + 1
filenames = glob.glob('data_*.npy')
sequence_number = 0
if filenames != []:
    sequence_number = max([int(f[:-4].split('_')[1]) for f in filenames]) + 1
np.save('data_{}.npy'.format(sequence_number), data)
