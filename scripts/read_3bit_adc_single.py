import adc
import time
import sys
import pdb
import timeit
import matplotlib.pyplot as plt

print(sys.argv[1])

ad = adc.ADC()

# Set threshold
ad.configThresholdDefault(start=0.25,end=1.8)


f = plt.figure(figsize=(5,3))

ad.setSampling(False)
res = ad.readoutBuffer(1)
ad.setSampling(True)
data = ad.convert3BitBuffer(res)
#    print([hex(d) for d in res])
print(data)
plt.plot(data, '-o', ms=1)
#    ymax = max(data)
#    ymin = min(data)
#    ymargin = (ymax - ymin) / 10
#    plt.ylim(ymin-ymargin, ymax + ymargin)
plt.ylim(-0.1, 7.1)
plt.xlabel('# sample')
plt.ylabel('Digital value')
plt.show()

f.savefig("{}.pdf".format(sys.argv[1]), bbox_inches='tight')
