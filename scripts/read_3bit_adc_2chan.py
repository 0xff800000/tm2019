import adc
import time
import sys
import pdb
import timeit
import matplotlib.pyplot as plt

ad = adc.ADC()

# Set threshold
ad.configThresholdDefault()

f = plt.figure(figsize=(5,3))

while True:
    ad.setSampling(False)
    res_ch1 = ad.readoutBuffer(1,100)
    res_ch2 = ad.readoutBuffer(2,100)
    ad.setSampling(True)
    data_ch1 = ad.convert3BitBuffer(res_ch1)
    data_ch2 = ad.convert3BitBuffer(res_ch2)
    plt.plot(data_ch1, '-o', ms=1)
    plt.plot(data_ch2, '-o', ms=1)
    plt.ylim(-0.1, 7.1)
    plt.xlabel('# sample')
    plt.ylabel('Digital value')
    plt.draw()
    plt.pause(0.0001)
    plt.clf()

