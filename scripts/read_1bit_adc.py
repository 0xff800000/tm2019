import adc
import time
import pdb
import matplotlib.pyplot as plt

plt.ion()
ad = adc.ADC()
ad.writeDAC(0,2*808)

while True:
    res = ad.readoutBuffer()
    print([hex(r) for r in res])
    print(res)
    data = ad.convert1BitBuffer(res)
    plt.plot(data)
    plt.ylim(-0.1,1.1)
    plt.xlabel('# sample')
    plt.ylabel('Digital value')
    plt.draw()
    plt.pause(0.0001)
    time.sleep(1)
    plt.clf()
