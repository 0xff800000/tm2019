import matplotlib.pyplot as plt
import numpy as np
import pdb

data = np.loadtxt("current.csv",delimiter=',')
freq = data[:,0]
attenuation = data[:,1]

f = plt.figure(figsize=(5,3))
plt.plot(freq, attenuation, '-o', ms=5, rasterized=True)
plt.xlabel('Sampling frequency [Hz]')
plt.ylabel('Current [A]')
plt.grid()
plt.show()
f.savefig("current.pdf", bbox_inches='tight', dpi=400)
