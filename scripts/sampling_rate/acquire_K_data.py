import adc
import time
import sys
import pdb
import timeit
import numpy as np
import matplotlib.pyplot as plt

fs = sys.argv[1]
ad = adc.ADC()
ad.configThresholdDefault()
#ad.configThresholdDefault(start=0.6, end=1.9)
ad.setSampling(True)
time.sleep(0.5)

f = plt.figure(figsize=(5,3))
plt.ylim(-0.1, 7.1)
plt.xlabel('# sample')
plt.ylabel('Digital value')

K = 10
size = 8000
xkn = []

for k in range(0, K):
    while True:
        ad.setSampling(False)
        res = ad.readoutBuffer(1)
        ad.setSampling(True)
        data = ad.convert3BitBuffer(res)
        if len(data) < size:
            time.sleep(1)
        else:
            break
    
    print(data)
    xkn.append(data)
    plt.plot(data, '-o', ms=1)
    plt.draw()
    plt.pause(0.0001)
    plt.clf()
    time.sleep(0.5)

np.save('K_data_{}.npy'.format(fs), np.array(xkn))
