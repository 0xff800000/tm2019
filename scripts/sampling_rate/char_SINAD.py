import sys
import json
import pdb
import glob
import matplotlib.pyplot as plt
import numpy as np
import scipy.optimize

f = open('adc_parameters.json', 'r')
adc_data = json.load(f)
f.close()

SINAD_f = []
SINAD_dBFS_f = []
SNR_f = []
SNR_dBFS_f = []
ENOB_f = []
freq_char = []
THD_f = []

for filename in glob.glob('K_data_*'):
    data = np.load(filename)[1]
    M = len(data)
    fs_str = filename[:-4].split('_')[-1]
    fs = float(fs_str)
    freq_char.append(fs)
    t = np.array(range(M))*1/fs
    fi = 11e6

    if fs_str == '100e6':
        plt.plot(data)
        plt.show()

    # 4 parameter sine fit of input : A0 f0 B0 C0
    #def func(x, A0,f0,B0,C0):
    #    return A0*np.cos(2*np.pi*f0*x) + B0*np.sin(2*np.pi*f0*x) + C0
    #
    #print(scipy.optimize.curve_fit(func, t, data))

    # Guess
    A0g = 5
    f0g = fi
    B0g = 0
    C0g = 0


    optimize_func = lambda x: x[0]*np.cos(2*np.pi*x[1]*t) + x[2]*np.sin(2*np.pi*x[1]*t) + x[3] - data
    res = scipy.optimize.least_squares(optimize_func, [A0g, f0g, B0g, C0g])
    print(res)
    #res = scipy.optimize.leastsq(optimize_func, [1, 1, 1, 1])
    A0, f0, B0, C0 = res['x']

    fine_t = np.arange(0,max(t),1)
    fine_t = t
    data_fit = A0*np.cos(2*np.pi*f0*fine_t) + B0*np.sin(2*np.pi*f0*fine_t) + C0
    data_guess = A0g*np.cos(2*np.pi*f0g*fine_t) + B0g*np.sin(2*np.pi*f0g*fine_t) + C0g

    #f = plt.figure(figsize=(5,3))
    #plt.plot(t, data, '.', ms=5, label='samples')
    #plt.plot(fine_t, data_fit, label='fit')
    #plt.plot(fine_t, data_guess)
    #plt.ylim(-0.1, 7.1)
    #plt.xlabel('# sample')
    #plt.ylabel('Digital value')
    #plt.grid()
    #plt.show()

    #f.savefig("sine_fit_sinad_{}.pdf".format(fi), bbox_inches='tight')

    # NAD (eq. 65 IEEE)
    x = data
    xp = A0*np.cos(2*np.pi*f0*t) + B0*np.sin(2*np.pi*f0*t) + C0
    M = len(data)
    NAD = np.sqrt(1/M * np.sum((x-xp)**2))
    Arms = np.sqrt(A0**2 + B0**2) / np.sqrt(2)

    # SINAD (eq. 66 IEEE)
    SINAD = Arms / NAD
    SINAD_dBFS = (10*np.log(SINAD))
    print('SINAD %s dBFS' % SINAD_dBFS)

    # SNR (eq. 68, 69 IEEE)
    THD = float(adc_data['THD_fs'][str(fs)])
    THD_f.append(float(adc_data['THD_fs_dBFS'][str(fs)]))
    print(NAD**2)
    etha = np.sqrt(NAD**2 - (Arms**2)*(THD**2))
    print(etha)
    SNR = Arms / etha
    SNR_dBFS = 10*np.log(SNR)
    print('SNR %s dBFS' % SNR_dBFS)

    # ENOB (eq. 70 IEEE)
    FSR = 2**adc_data['res']
    G = adc_data['G']
    ENOB = np.log2((FSR / G) / (NAD * np.sqrt(12)))
    print('ENOB %s bit' % ENOB)

    # Append
    SINAD_f.append(SINAD)
    SNR_f.append(SNR)
    SINAD_dBFS_f.append(SINAD_dBFS)
    SNR_dBFS_f.append(SNR_dBFS)
    ENOB_f.append(ENOB)

SINAD_f.sort()
SNR_f.sort()
SINAD_dBFS_f.sort()
SNR_dBFS_f.sort()
ENOB_f.sort()
freq_char.sort()

# Remove 1st data point, it is too close to fs
SINAD_f = SINAD_f[1:]
SNR_f = SNR_f[1:]
SINAD_dBFS_f = SINAD_dBFS_f[1:]
SNR_dBFS_f = SNR_dBFS_f[1:]
ENOB_f = ENOB_f[1:]
freq_char = freq_char[1:]
THD_f = THD_f[1:]

# Print performance drop in %
def minmax_diff(array, inv=False):
    min_ = min(array)
    max_ = max(array)
    ret = (max_ - min_) / max_
    if inv:
        ret = np.abs(ret)
    return ret
print('SINAD_dBFS_f ',minmax_diff(SINAD_dBFS_f))
print('SNR_dBFS_f ',minmax_diff(SNR_dBFS_f))
print('ENOB_f ',minmax_diff(ENOB_f))
print('THD_f ',minmax_diff(THD_f,True))


# Save json
adc_data['SINAD_lin_fs'] = SINAD_f
adc_data['SINAD_dBFS_fs'] = SINAD_dBFS_f
adc_data['SNR_lin_fs'] = SNR_f
adc_data['SNR_dBFS_fs'] = SNR_dBFS_f
adc_data['ENOB_fs'] = ENOB_f

f = open('adc_parameters.json', 'w')
json.dump(adc_data, f)


f = plt.figure(figsize=(5,3))
plt.plot(freq_char, ENOB_f, '-o', ms=5)
plt.xlabel('Frequency [Hz]')
plt.ylabel('ENOB [bit]')
plt.grid()
plt.show()
f.savefig("ENOB_fs.pdf", bbox_inches='tight')

f = plt.figure(figsize=(5,3))
plt.plot(freq_char, SNR_dBFS_f, '-o', ms=5)
plt.xlabel('Frequency [Hz]')
plt.ylabel('SNR [dBFS]')
plt.grid()
plt.show()
f.savefig("SRN_fs.pdf", bbox_inches='tight')

f = plt.figure(figsize=(5,3))
plt.plot(freq_char, SINAD_dBFS_f, '-o', ms=5)
plt.xlabel('Frequency [Hz]')
plt.ylabel('SINAD [dBFS]')
plt.grid()
plt.show()
f.savefig("SINAD_fs.pdf", bbox_inches='tight')

f = plt.figure(figsize=(5,3))
plt.plot(freq_char, THD_f, '-o', ms=5)
plt.xlabel('Frequency [Hz]')
plt.ylabel('THD [dBFS]')
plt.grid()
plt.show()
f.savefig("THD_fs.pdf", bbox_inches='tight')

