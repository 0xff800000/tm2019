import adc
import time
import sys
import pdb
import timeit
import matplotlib.pyplot as plt

print(sys.argv[1])

ad = adc.ADC()
ad.setThresholdVolt(0,1)
time.sleep(0.5)
ad.setThresholdVolt(1,2.0)
#ad.setThresholdVolt(1,1)
time.sleep(0.5)
ad.setThresholdVolt(2,3.0)
#ad.setThresholdVolt(2,1)
time.sleep(0.5)

f = plt.figure(figsize=(5,3))

while True:
    res = ad.readoutBuffer(128)
    data = ad.convert2BitBuffer(res)
    print([hex(d) for d in res])
    print(data)
    plt.plot(data, '-o', ms=1)
#    ymax = max(data)
#    ymin = min(data)
#    ymargin = (ymax - ymin) / 10
#    plt.ylim(ymin-ymargin, ymax + ymargin)
    plt.ylim(-0.1, 3.1)
    plt.xlabel('# sample')
    plt.ylabel('Digital value')
    plt.draw()
    plt.pause(0.0001)
    plt.clf()

f.savefig("{}.pdf".format(sys.argv[1]), bbox_inches='tight')
