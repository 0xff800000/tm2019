import sys
import json
import pdb
import glob
import matplotlib.pyplot as plt
import numpy as np
import scipy.optimize

f = open('adc_parameters.json', 'r')
adc_data = json.load(f)
f.close()

SINAD_f = []
SINAD_dBFS_f = []
SNR_f = []
SNR_dBFS_f = []
ENOB_f = []

for kFile in range(10):
    filename = glob.glob('{}_*.npy'.format(kFile))[0]
    data_k = np.load(filename)
    SINAD_avg = []
    SINAD_dBFS_avg = []
    SNR_avg = []
    SNR_dBFS_avg = []
    ENOB_avg = []
    for data in data_k:
        M = len(data)
        fs = 927.782e6
        t = np.array(range(M))*1/fs
        fi = float(filename[:-4].split('_')[1])

        # 4 parameter sine fit of input : A0 f0 B0 C0
        #def func(x, A0,f0,B0,C0):
        #    return A0*np.cos(2*np.pi*f0*x) + B0*np.sin(2*np.pi*f0*x) + C0
        #
        #print(scipy.optimize.curve_fit(func, t, data))

        # Guess
        A0g = 5
        f0g = fi
        B0g = 0
        C0g = 0


        optimize_func = lambda x: x[0]*np.cos(2*np.pi*x[1]*t) + x[2]*np.sin(2*np.pi*x[1]*t) + x[3] - data
        res = scipy.optimize.least_squares(optimize_func, [A0g, f0g, B0g, C0g])
        print(res)
        #res = scipy.optimize.leastsq(optimize_func, [1, 1, 1, 1])
        A0, f0, B0, C0 = res['x']

        fine_t = np.arange(0,max(t),1)
        fine_t = t
        data_fit = A0*np.cos(2*np.pi*f0*fine_t) + B0*np.sin(2*np.pi*f0*fine_t) + C0
        data_guess = A0g*np.cos(2*np.pi*f0g*fine_t) + B0g*np.sin(2*np.pi*f0g*fine_t) + C0g

        f = plt.figure(figsize=(5,3))
        plt.plot(t, data, '.', ms=5, label='samples')
        plt.plot(fine_t, data_fit, label='fit')
        #plt.plot(fine_t, data_guess)
        #plt.ylim(-0.1, 7.1)
        plt.xlabel('# sample')
        plt.ylabel('Digital value')
        plt.grid()
        #plt.show()
        plt.close()


        # NAD (eq. 65 IEEE)
        x = data
        xp = A0*np.cos(2*np.pi*f0*t) + B0*np.sin(2*np.pi*f0*t) + C0
        M = len(data)
        NAD = np.sqrt(1/M * np.sum((x-xp)**2))
        Arms = np.sqrt(A0**2 + B0**2) / np.sqrt(2)

        # SINAD (eq. 66 IEEE)
        SINAD = Arms / NAD
        SINAD_dBFS = (10*np.log(SINAD))
        print('SINAD %s dBFS' % SINAD_dBFS)

        # SNR (eq. 68, 69 IEEE)
        THD = float(adc_data['THD_f'][kFile])
        print(NAD**2)
        etha = np.sqrt(NAD**2 - (Arms**2)*(THD**2))
        print(etha)
        SNR = Arms / etha
        SNR_dBFS = 10*np.log(SNR)
        print('SNR %s dBFS' % SNR_dBFS)

        # ENOB (eq. 70 IEEE)
        FSR = 2**adc_data['res']
        G = adc_data['G']
        ENOB = np.log2((FSR / G) / (NAD * np.sqrt(12)))
        print('ENOB %s bit' % ENOB)

        SINAD_avg.append(SINAD)
        SNR_avg.append(SNR)
        SINAD_dBFS_avg.append(SINAD_dBFS)
        SNR_dBFS_avg.append(SNR_dBFS)
        ENOB_avg.append(ENOB)



    # Append
    SINAD_f.append(np.mean(SINAD_avg))
    SNR_f.append(np.mean(SNR_avg))
    SINAD_dBFS_f.append(np.mean(SINAD_dBFS_avg))
    SNR_dBFS_f.append(np.mean(SNR_dBFS_avg))
    ENOB_f.append(np.mean(ENOB_avg))

# Save json
adc_data['SINAD_lin_f'] = SINAD_f
adc_data['SINAD_dBFS_f'] = SINAD_dBFS_f
adc_data['SNR_lin_f'] = SNR_f
adc_data['SNR_dBFS_f'] = SNR_dBFS_f
adc_data['ENOB_f'] = ENOB_f

f = open('adc_parameters.json', 'w')
json.dump(adc_data, f)

freq_char = adc_data['freq_char']
pdb.set_trace()

f = plt.figure(figsize=(5,3))
plt.semilogx(freq_char[:-1], ENOB_f[:-1], '-o', ms=5)
plt.xlabel('Frequency [Hz]')
plt.ylabel('ENOB [bit]')
plt.grid()
plt.show()
f.savefig("ENOB_f.pdf", bbox_inches='tight')

f = plt.figure(figsize=(5,3))
plt.semilogx(freq_char[:-1], SNR_dBFS_f[:-1], '-o', ms=5)
plt.xlabel('Frequency [Hz]')
plt.ylabel('SNR [dBFS]')
plt.grid()
plt.show()
f.savefig("SRN_f.pdf", bbox_inches='tight')

f = plt.figure(figsize=(5,3))
plt.semilogx(freq_char[:-1], SINAD_dBFS_f[:-1], '-o', ms=5)
plt.xlabel('Frequency [Hz]')
plt.ylabel('SINAD [dBFS]')
plt.grid()
plt.show()
f.savefig("SINAD_f.pdf", bbox_inches='tight')
