import matplotlib.pyplot as plt
import numpy as np
import scipy.optimize
import json

data = np.loadtxt("sine_fit.txt")
t = range(len(data))

# 4 parameter sine fit of input : A0 f0 B0 C0
#def func(x, A0,f0,B0,C0):
#    return A0*np.cos(2*np.pi*f0*x) + B0*np.sin(2*np.pi*f0*x) + C0
#
#print(scipy.optimize.curve_fit(func, t, data))

# Guess
A0g = 5
f0g = 0.000700
B0g = 0
C0g = 0


optimize_func = lambda x: x[0]*np.cos(2*np.pi*x[1]*t) + x[2]*np.sin(2*np.pi*x[1]*t) + x[3] - data
res = scipy.optimize.least_squares(optimize_func, [A0g, f0g, B0g, C0g])
print(res)
#res = scipy.optimize.leastsq(optimize_func, [1, 1, 1, 1])
A0, f0, B0, C0 = res['x']

fine_t = np.arange(0,max(t),1)
data_fit = A0*np.cos(2*np.pi*f0*fine_t) + B0*np.sin(2*np.pi*f0*fine_t) + C0
data_guess = A0g*np.cos(2*np.pi*f0g*fine_t) + B0g*np.sin(2*np.pi*f0g*fine_t) + C0g

# Plot sine fit
f = plt.figure(figsize=(5,3))
plt.plot(t, data, '.', ms=5, rasterized=True)
plt.plot(fine_t, data_fit, rasterized=True)
#plt.plot(fine_t, data_guess)
#plt.ylim(-0.1, 7.1)
plt.xlabel('# sample')
plt.ylabel('Digital value')
plt.show()
f.savefig("THD_sine_fit.pdf", bbox_inches='tight', dpi=400)

A1 = np.sqrt(A0**2 + B0**2)
fi = f0

# Residuals
data_fit_t = A0*np.cos(2*np.pi*f0*t) + B0*np.sin(2*np.pi*f0*t) + C0
residual = data - data_fit_t

# Plot t mod T plot
T = 1 / fi
f = plt.figure(figsize=(5,3))
plt.plot(t % T, residual, '.', ms=5, rasterized=True)
plt.plot(fine_t % T, data_fit, '.', rasterized=True)
#plt.plot(fine_t, data_guess)
#plt.ylim(-0.1, 7.1)
plt.xlabel('# sample')
plt.ylabel('Digital value')
plt.show()
f.savefig("THD_tmodT.pdf", bbox_inches='tight', dpi=400)


# 3 param sine fit for each harmonic
Nh = 100
Ah = []
for h in range(2, Nh):
    optimize_func_harmonic = lambda x: x[0]*np.cos(2*np.pi*h*fi*t) + x[1]*np.sin(2*np.pi*h*fi*t) + x[2] - residual
    res = scipy.optimize.least_squares(optimize_func_harmonic, [A0g, B0g, C0g])
    print(res)
    A0, B0, C0 = res['x']
    Ah.append(np.sqrt(A0**2 + B0**2))
    data_fit = A0*np.cos(2*np.pi*h*fi*fine_t) + B0*np.sin(2*np.pi*h*fi*fine_t) + C0
    if h == 2:
        f = plt.figure(figsize=(5,3))
        plt.plot(t, residual, '.', ms=5, rasterized=True)
        plt.plot(fine_t, data_fit, rasterized=True)
        #plt.plot(fine_t, data_guess)
        #plt.ylim(-0.1, 7.1)
        plt.xlabel('# sample')
        plt.ylabel('Digital value')
        plt.show()
        f.savefig("THD_sine_fit_harm.pdf", bbox_inches='tight', dpi=400)

    data_fit_res = A0*np.cos(2*np.pi*h*fi*t) + B0*np.sin(2*np.pi*h*fi*t) + C0
    residual -= data_fit_res

THD = np.sqrt(np.sum(np.array(Ah)**2)) / A1
THD_dBFS = 10*np.log(THD)
print(THD)
print(THD_dBFS)

# Save json
f = open('adc_parameters.json', 'r')
adc_data = json.load(f)
f.close()
adc_data['THD'] = THD
adc_data['THD_dBFS'] = THD_dBFS

f = open('adc_parameters.json', 'w')
json.dump(adc_data, f)
