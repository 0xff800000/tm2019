import adc
import time
import sys
import pdb
import timeit
import numpy as np
import matplotlib.pyplot as plt


ad = adc.ADC()
ad.configThresholdDefault(start=0.6, end=1.9)
ad.setSampling(True)
time.sleep(0.5)

start_freq = 250e3
#start_freq = 1 / (8176 / 2 * 1/ad.fs)
stop_freq = 450e6
steps = 10
#stop_freq = ad.fs / 2
freqs = np.logspace(np.log10(start_freq), np.log10(stop_freq), num=steps)
print(freqs)


f = plt.figure(figsize=(5,3))
###plt.ylim(-0.1, 7.1)
plt.xlabel('# sample')
plt.ylabel('Digital value')
plt.semilogx(freqs, freqs, 'o')
plt.show()
sys.exit(0)

K = 10
size = 8000
xkn = []

for freq in freqs:
    print('Set input frequency to %s' % freq)
    input('Press any key to continue ')
    print('Input signal verification')
    while True:
        ad.setSampling(False)
        res = ad.readoutBuffer(1)
        ad.setSampling(True)
        data = ad.convert3BitBuffer(res)
        plt.plot(data, '-o', ms=1)
        plt.show()
        if input('
        

        
    
    continue
    for k in range(0, K):
        while True:
            ad.setSampling(False)
            res = ad.readoutBuffer(1)
            ad.setSampling(True)
            data = ad.convert3BitBuffer(res)
            if len(data) < size:
                time.sleep(1)
            else:
                break
        
        print(data)
        xkn.append(data)
        plt.plot(data, '-o', ms=1)
        plt.draw()
        plt.pause(0.0001)
        plt.clf()
        time.sleep(0.5)

    np.save('K_data.npy', np.array(xkn))
