import adc
import time
import sys
import pdb
import timeit
import numpy as np
import matplotlib.pyplot as plt

sequence_number = sys.argv[1]
frequency = sys.argv[2]

start_freq = 250e3
#start_freq = 1 / (8176 / 2 * 1/ad.fs)
stop_freq = 450e6
steps = 10
#stop_freq = ad.fs / 2
freqs = np.logspace(np.log10(start_freq), np.log10(stop_freq), num=steps)
print('Expected frequencies')
print(freqs)
nb_cycles = 4
window_size = int((1/int(frequency)*nb_cycles) / (1/927.782e6))
print(window_size)
if window_size > 1022 * 8:
    window_size = 1022 * 8

ad = adc.ADC()
ad.configThresholdDefault(start=0.4, end=1.7)
ad.setSampling(True)
time.sleep(0.5)

def plotData(data):
    f = plt.figure(figsize=(5,3))
    plt.plot(data, '-o', ms=1)
    plt.ylim(-0.1, 7.1)
    plt.xlabel('# sample')
    plt.ylabel('Digital value')
    plt.show()

print('####################\nSignal verification\n####################')

while True:
    ad.setSampling(False)
    res = ad.readoutBuffer(1,window_size)
    ad.setSampling(True)
    data = ad.convert3BitBuffer(res)
    plotData(data)
    if input('Is the input signal ready ? [y/n]') == 'n':
        instr = input('New start and stop thresholds : ')
        instr = instr.split()
        if len(instr) <= 1:
            continue
        else:
            ad.configThresholdDefault(start=float(instr[0]), end=float(instr[1]))
    else:
        break
        
print('####################\nAcquiring data\n####################')

K = 10
size = 8000
xkn = []

for k in range(0, K):
    print('####################\nK = %s\n####################' % k)
    while True:
        ad.setSampling(False)
        res = ad.readoutBuffer(1)
        ad.setSampling(True)
        data = ad.convert3BitBuffer(res)
        if len(data) < size:
            time.sleep(1)
        else:
            break
    
    print(data)
    xkn.append(data)
    plt.plot(data, '-o', ms=1)
    plt.draw()
    plt.pause(0.0001)
    plt.clf()
    time.sleep(0.5)

np.save('{}_{}.npy'.format(sequence_number, frequency), np.array(xkn))
