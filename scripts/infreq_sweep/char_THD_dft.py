import matplotlib.pyplot as plt
import numpy as np
import scipy.optimize
import json
import pdb
import sys
import glob

freq_char = []
THD_f = []
THD_dBFS_f = []

for kFile in range(11):
    filename = glob.glob('{}_*.npy'.format(kFile))[0]
    print(filename)
    data = np.load(filename)

    # Definitions
    K = len(data)
    fs = 927.782e6
    fi = float(filename[:-4].split('_')[1])
    Ts = 1 / fs
    M = len(data[0])
    t = np.arange(0,M) * Ts
    f = np.arange(0,M) * fs / M
    L = 1
    NNPG = 0.625
    Nh = 100
    ni = [-M*fi/fs, M*fi/fs]
    ni = np.array([int(np.ceil(i)) for i in ni])

    # Prevent overlaping of harmonics
    print(Nh*ni[1])
    print(M / 2)
    if Nh * ni[1] > M / 2:
        Nh = int(M / (ni[1]*2))
        print('Nh reduced to {}'.format(Nh))

    # Hann window
    def w_hann(n):
        return (np.sin(np.pi * n / M))**2
    wn = w_hann(np.arange(0,M))
#    plt.plot(wn)
#    plt.show()

    # Average spectrum (eq. 55 IEEE)
    Xwk = np.fft.fft(data * wn)
    Xwavg = 1 / K * np.sum(np.abs(Xwk), axis=0)

    # (eq. 53 IEEE)
    nh = np.concatenate( (np.flip(-np.arange(2,Nh+1)), np.arange(2,Nh+1)) ) * ni[1] % M
    # (eq. 56 IEEE)
    def Xavmn2(n):
        sumk = 0
        for k in range(-(L+1), L+1):
            sumk += np.sum(Xwavg[n+k] ** 2)
        return 1 / NNPG * sumk
        #k = np.arange(-(L+1), L+1)
        #return 1 / NNPG * np.sum(Xwavg[k+n] **2)

    fig = plt.figure(figsize=(5,3))
    plt.semilogy(f, Xwavg, '-o', ms=1)
    plt.semilogy(f[ni], Xwavg[ni], 's', ms=10)
    plt.semilogy(f[nh], Xwavg[nh], '^', ms=10)

    #plt.semilogy(Xwavg, '-o', ms=1)
    #plt.semilogy(ni,Xwavg[ni], 's', ms=10)
    #plt.semilogy(nh,Xwavg[nh], '^', ms=10)

    plt.xlabel('frequency [Hz]')
    plt.ylabel('DFT X_wavg')
    plt.grid()
    plt.xlim(0, 10*fi)
    #plt.show()
    fig.savefig("THD_dft_{}.pdf".format(fi), bbox_inches='tight')
    plt.close()

    # (eq. 54 IEEE)
    #THD = np.sqrt(1/(M**2) * np.sum(Xavmn2(nh))) / (1/M*np.sqrt(Xavmn2(ni) + Xavmn2(M-ni)))
    THD = np.sqrt(1/(M**2) * np.sum(Xavmn2(nh)))
    THD /= (1/M*np.sqrt(Xavmn2(ni)))
    THD_dBFS = 10*np.log(THD)
    print(THD)
    print(THD_dBFS)

    # Append
    freq_char.append(fi)
    THD_f.append(THD)
    THD_dBFS_f.append(THD_dBFS)

# Save json
fig = open('adc_parameters.json', 'r')
adc_data = json.load(fig)
fig.close()
adc_data['freq_char'] = freq_char
adc_data['THD_f'] = THD_f
adc_data['THD_dBFS_f'] = THD_dBFS_f

fig = open('adc_parameters.json', 'w')
json.dump(adc_data, fig)

fig = plt.figure(figsize=(5,3))
plt.semilogx(freq_char, THD_dBFS_f, '-o', ms=5)
#plt.semilogx(freq_char[:-1], THD_dBFS_f[:-1], '-o', ms=5)
plt.xlabel('frequency [Hz]')
plt.ylabel('THD(f) [dBFS]')
plt.grid()
plt.show()
fig.savefig("THD_dft_f.pdf", bbox_inches='tight')
