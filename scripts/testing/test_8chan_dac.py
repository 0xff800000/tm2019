import adc
import time
import sys
import pdb
import timeit
import matplotlib.pyplot as plt

ad = adc.ADC()

val = 0
while True:
    for c in range(0,7):
        print('{}'.format(val))
        time.sleep(0.01)
        ad.writeDAC(c,val)

    if val == 0:
        val = -1
    else:
        val = 0

# Set threshold
#for c in range(0,7):
#    level = 2.5 / 7 * (c+1)
#    print('Set threshold of channel {} to {} V'.format(c,level))
#    ad.setThresholdVolt(c,level)
#    time.sleep(0.01)

f = plt.figure(figsize=(5,3))

while True:
    res = ad.readoutBuffer(2)
    data = ad.convert3BitBuffer(res)
#    print([hex(d) for d in res])
    print(data)
    plt.plot(data, '-o', ms=1)
#    ymax = max(data)
#    ymin = min(data)
#    ymargin = (ymax - ymin) / 10
#    plt.ylim(ymin-ymargin, ymax + ymargin)
    plt.ylim(-0.1, 3.1)
    plt.xlabel('# sample')
    plt.ylabel('Digital value')
    plt.draw()
    plt.pause(0.0001)
    plt.clf()

f.savefig("{}.pdf".format(sys.argv[1]), bbox_inches='tight')
