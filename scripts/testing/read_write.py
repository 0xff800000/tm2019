import serial

def readRegister(ser, addr):
    command = [
            'r',
            (addr & 0xff000000) >> 24,
            (addr & 0x00ff0000) >> 16,
            (addr & 0x0000ff00) >> 8,
            (addr & 0x000000ff),
            ]
    while True:
        try:
            ser.write(bytearray(command))
            ser.flush()
            data_raw = [ord(ser.read()) for i in range(0,4)]
        except:
            continue
        break
    return data_raw[3] | data_raw[2] << 8 | data_raw[1] << 16 | data_raw[0] << 24


def writeRegister(ser, addr, data):
    command = [
            'w',
            (addr & 0xff000000) >> 24,
            (addr & 0x00ff0000) >> 16,
            (addr & 0x0000ff00) >> 8,
            (addr & 0x000000ff),
            (data & 0xff000000) >> 24,
            (data & 0x00ff0000) >> 16,
            (data & 0x0000ff00) >> 8,
            (data & 0x000000ff),
            ]
    while True:
        ser.write(bytearray(command))
        ser.flush()
        rd_data = readRegister(ser, addr)
        if data != rd_data:
            print('Error : *({}) == {} != {}'.format(hex(adr),hex(rd_data),hex(data)))
            continue
        else:
            break

ser = serial.Serial('/dev/ttyUSB1', 115200, timeout=1)
print(ser.name)


# Test 1
print('Test 1')
data = 0xdeadbeef
writeRegister(ser,0x0, data)
rd_data = readRegister(ser,0x0)

print('Data sent : {}\nData received : {}'.format(hex(data),hex(rd_data)))
if rd_data == data:
    print('TEST 1 PASSED')
else:
    print('TEST 1 FAILED')
print('\n\n')


# Test 2
print('Test 2')
data = 0xdeadbeef
for adr in range(0,512):
    print('Writing : *({}) := {}'.format(hex(adr),hex(adr)))
    writeRegister(ser,adr, adr)
failed = False
maxretries = 4
for adr in range(0,512):
    retries = 0
    while retries < maxretries:
        rd_data = readRegister(ser,adr)
        print('Reading : *({}) := {}'.format(hex(adr),hex(rd_data)))
        if rd_data == adr:
            break
        else:
            print('Error : *({}) != {}'.format(hex(adr),hex(adr)))
            retries += 1
    if retries >= maxretries:
        failed = True

if not failed:
    print('TEST 2 PASSED')
else:
    print('TEST 2 FAILED')
