import adc
import sys
import pdb

ad = adc.ADC(headless=True)


#   Value : 01230123
#           00001111 = 0x0f
#           00110011 = 0x33
#           01010101 = 0x55
res = [0x000f3355,0x000f3355] 
expected = [0,1,2,3,4,5,6,7,0,1,2,3,4,5,6,7] 
data = ad.convert3BitBuffer(res)
print(data)

fail=False
for i,n in enumerate(data):
    if n != expected[i]:
        fail=True
if not fail:
    print('PASSED')
else:
    print('FAILED')
    sys.exit(-1)

res = [0x0000ffff,0x0000ffff,0x0000ffff] 
data = ad.convert2BitBuffer(res)
print(data)

fail=False
for i,n in enumerate(data):
    if n != 3:
        fail=True
if not fail:
    print('PASSED')
else:
    print('FAILED')
    sys.exit(-1)

res = [0x000000ff,0x000000ff,0x000000ff] 
data = ad.convert2BitBuffer(res)
print(data)

fail=False
for i,n in enumerate(data):
    if n != 1:
        fail=True
if not fail:
    print('PASSED')
else:
    print('FAILED')
    sys.exit(-1)

res = [0x0000ff00,0x0000ff00,0x0000ff00] 
data = ad.convert2BitBuffer(res)
print(data)

fail=False
for i,n in enumerate(data):
    if n != 2:
        fail=True
if not fail:
    print('PASSED')
else:
    print('FAILED')
    sys.exit(-1)
pdb.set_trace()
