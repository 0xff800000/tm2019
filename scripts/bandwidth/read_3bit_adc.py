import adc
import time
import sys
import pdb
import timeit
import matplotlib.pyplot as plt

print(sys.argv[1])
freq = float(sys.argv[1])
nb_cycles = 4
window_size = int((1/freq*nb_cycles) / (1/927.782e6))
print(window_size)
if window_size > 1022 * 8:
    window_size = 1022 * 8

ad = adc.ADC()

# Set threshold
#ad.configThresholdDefault(start=0.25,end=2.1)
scaling = 8
start=0.25
end=2.1
gap=(end-start)/(2*scaling)
start+=gap
end-=gap
ad.configThresholdDefault(start=start,end=end) # scaled

f = plt.figure(figsize=(5,3))

while True:
    ad.setSampling(False)
    res = ad.readoutBuffer(1,window_size/8)
    ad.setSampling(True)
    data = ad.convert3BitBuffer(res)
#    print([hex(d) for d in res])
    print(data)
    plt.plot(data, '-o', ms=1)
#    ymax = max(data)
#    ymin = min(data)
#    ymargin = (ymax - ymin) / 10
#    plt.ylim(ymin-ymargin, ymax + ymargin)
    plt.ylim(-0.1, 7.1)
    plt.xlabel('# sample')
    plt.ylabel('Digital value')
    plt.draw()
    plt.pause(0.0001)
    plt.clf()

f.savefig("{}.pdf".format(sys.argv[1]), bbox_inches='tight')
