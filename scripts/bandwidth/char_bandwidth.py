import matplotlib.pyplot as plt
import numpy as np
import pdb

data = np.loadtxt("bandwidth.csv",delimiter=',')
freq = data[:,0]
attenuation = data[:,1]
attenuation = 10*np.log10(attenuation[0]/attenuation)

f = plt.figure(figsize=(5,3))
plt.semilogx(freq, attenuation, '-o', ms=5, rasterized=True)
plt.xlabel('Frequency [Hz]')
plt.ylabel('Attenuation [dB]')
plt.grid()
plt.show()
f.savefig("attenuation.pdf", bbox_inches='tight', dpi=400)
