# 
# Synthesis run script generated by Vivado
# 

set TIME_start [clock seconds] 
proc create_report { reportName command } {
  set status "."
  append status $reportName ".fail"
  if { [file exists $status] } {
    eval file delete [glob $status]
  }
  send_msg_id runtcl-4 info "Executing : $command"
  set retval [eval catch { $command } msg]
  if { $retval != 0 } {
    set fp [open $status w]
    close $fp
    send_msg_id runtcl-5 warning "$msg"
  }
}
set_param chipscope.maxJobs 1
set_msg_config -id {HDL 9-1061} -limit 100000
set_msg_config -id {HDL 9-1654} -limit 100000
create_project -in_memory -part xc7a35tcpg236-1

set_param project.singleFileAddWarning.threshold 0
set_param project.compositeFile.enableAutoGeneration 0
set_param synth.vivado.isSynthRun true
set_msg_config -source 4 -id {IP_Flow 19-2162} -severity warning -new_severity info
set_property webtalk.parent_dir /home/d/tm2019/tm2019/tm2019.cache/wt [current_project]
set_property parent.project_path /home/d/tm2019/tm2019/tm2019.xpr [current_project]
set_property XPM_LIBRARIES {XPM_CDC XPM_MEMORY} [current_project]
set_property default_lib xil_defaultlib [current_project]
set_property target_language VHDL [current_project]
set_property ip_output_repo /home/d/tm2019/tm2019/tm2019.cache/ip [current_project]
set_property ip_cache_permissions {read write} [current_project]
read_vhdl -library xil_defaultlib {
  /home/d/tm2019/src/UtilityPkg.vhd
  /home/d/tm2019/src/CommandInterpreter.vhd
  /home/d/tm2019/src/DAC_AD5672.vhd
  /home/d/tm2019/src/registers_rw.vhd
  /home/d/tm2019/src/DAC_top_AD5672.vhd
  /home/d/tm2019/src/DFF.vhd
  /home/d/tm2019/src/UartClockGenerator.vhd
  /home/d/tm2019/src/UartRx.vhd
  /home/d/tm2019/src/UartTx.vhd
  /home/d/tm2019/src/UartTop.vhd
  /home/d/tm2019/src/UartFrontEnd.vhd
  /home/d/tm2019/src/adc_ctrl.vhd
  /home/d/tm2019/src/comparator.vhd
  /home/d/tm2019/src/single_deser.vhd
  /home/d/tm2019/src/deserializer_frontend.vhd
  /home/d/tm2019/src/therm2bin_bec.vhd
  /home/d/tm2019/src/adc_deser_top.vhd
  /home/d/tm2019/src/registers_ro.vhd
  /home/d/tm2019/src/clockcrossing_Buffer.vhd
  /home/d/tm2019/src/cirular_buffer.vhd
  /home/d/tm2019/src/adc_frontend.vhd
  /home/d/tm2019/tm2019/tm2019.srcs/sources_1/bd/clk_adc/hdl/clk_adc_wrapper.vhd
  /home/d/tm2019/tm2019/tm2019.srcs/sources_1/bd/simple_clk/hdl/simple_clk_wrapper.vhd
  /home/d/tm2019/src/top.vhd
}
add_files /home/d/tm2019/tm2019/tm2019.srcs/sources_1/bd/simple_clk/simple_clk.bd
set_property used_in_implementation false [get_files -all /home/d/tm2019/tm2019/tm2019.srcs/sources_1/bd/simple_clk/ip/simple_clk_clk_wiz_0_0_1/simple_clk_clk_wiz_0_0_board.xdc]
set_property used_in_implementation false [get_files -all /home/d/tm2019/tm2019/tm2019.srcs/sources_1/bd/simple_clk/ip/simple_clk_clk_wiz_0_0_1/simple_clk_clk_wiz_0_0.xdc]
set_property used_in_implementation false [get_files -all /home/d/tm2019/tm2019/tm2019.srcs/sources_1/bd/simple_clk/ip/simple_clk_clk_wiz_0_0_1/simple_clk_clk_wiz_0_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/d/tm2019/tm2019/tm2019.srcs/sources_1/bd/simple_clk/simple_clk_ooc.xdc]

add_files /home/d/tm2019/tm2019/tm2019.srcs/sources_1/bd/clk_adc/clk_adc.bd
set_property used_in_implementation false [get_files -all /home/d/tm2019/tm2019/tm2019.srcs/sources_1/bd/clk_adc/ip/clk_adc_clk_wiz_0_0_2/clk_adc_clk_wiz_0_0_board.xdc]
set_property used_in_implementation false [get_files -all /home/d/tm2019/tm2019/tm2019.srcs/sources_1/bd/clk_adc/ip/clk_adc_clk_wiz_0_0_2/clk_adc_clk_wiz_0_0.xdc]
set_property used_in_implementation false [get_files -all /home/d/tm2019/tm2019/tm2019.srcs/sources_1/bd/clk_adc/ip/clk_adc_clk_wiz_0_0_2/clk_adc_clk_wiz_0_0_late.xdc]
set_property used_in_implementation false [get_files -all /home/d/tm2019/tm2019/tm2019.srcs/sources_1/bd/clk_adc/ip/clk_adc_clk_wiz_0_0_2/clk_adc_clk_wiz_0_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/d/tm2019/tm2019/tm2019.srcs/sources_1/bd/clk_adc/clk_adc_ooc.xdc]

read_ip -quiet /home/d/tm2019/tm2019/tm2019.srcs/sources_1/ip/blk_mem_gen_0_1/blk_mem_gen_0.xci
set_property used_in_implementation false [get_files -all /home/d/tm2019/tm2019/tm2019.srcs/sources_1/ip/blk_mem_gen_0_1/blk_mem_gen_0_ooc.xdc]

# Mark all dcp files as not used in implementation to prevent them from being
# stitched into the results of this synthesis run. Any black boxes in the
# design are intentionally left as such for best results. Dcp files will be
# stitched into the design at a later time, either when this synthesis run is
# opened, or when it is stitched into a dependent implementation run.
foreach dcp [get_files -quiet -all -filter file_type=="Design\ Checkpoint"] {
  set_property used_in_implementation false $dcp
}
read_xdc /home/d/tm2019/tm2019/Cmod-A7-Master.xdc
set_property used_in_implementation false [get_files /home/d/tm2019/tm2019/Cmod-A7-Master.xdc]

read_xdc dont_touch.xdc
set_property used_in_implementation false [get_files dont_touch.xdc]
set_param ips.enableIPCacheLiteLoad 1

read_checkpoint -auto_incremental -incremental /home/d/tm2019/tm2019/tm2019.srcs/utils_1/imports/synth_1/toplevel.dcp
close [open __synthesis_is_running__ w]

synth_design -top toplevel -part xc7a35tcpg236-1


# disable binary constraint mode for synth run checkpoints
set_param constraints.enableBinaryConstraints false
write_checkpoint -force -noxdef toplevel.dcp
create_report "synth_1_synth_report_utilization_0" "report_utilization -file toplevel_utilization_synth.rpt -pb toplevel_utilization_synth.pb"
file delete __synthesis_is_running__
close [open __synthesis_is_complete__ w]
