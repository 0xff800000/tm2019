--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
--Date        : Wed Jan 15 13:13:25 2020
--Host        : d-VirtualBox running 64-bit Ubuntu 18.04.3 LTS
--Command     : generate_target simple_clk_wrapper.bd
--Design      : simple_clk_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity simple_clk_wrapper is
  port (
    clk_out1_0 : out STD_LOGIC;
    reset : in STD_LOGIC;
    sys_clock : in STD_LOGIC
  );
end simple_clk_wrapper;

architecture STRUCTURE of simple_clk_wrapper is
  component simple_clk is
  port (
    sys_clock : in STD_LOGIC;
    reset : in STD_LOGIC;
    clk_out1_0 : out STD_LOGIC
  );
  end component simple_clk;
begin
simple_clk_i: component simple_clk
     port map (
      clk_out1_0 => clk_out1_0,
      reset => reset,
      sys_clock => sys_clock
    );
end STRUCTURE;
