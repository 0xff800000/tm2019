--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
--Date        : Wed Jan 15 12:54:10 2020
--Host        : d-VirtualBox running 64-bit Ubuntu 18.04.3 LTS
--Command     : generate_target clk_adc_wrapper.bd
--Design      : clk_adc_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity clk_adc_wrapper is
  port (
    clk_in1_0 : in STD_LOGIC;
    clk_out1_0 : out STD_LOGIC;
    reset_0 : in STD_LOGIC
  );
end clk_adc_wrapper;

architecture STRUCTURE of clk_adc_wrapper is
  component clk_adc is
  port (
    clk_out1_0 : out STD_LOGIC;
    clk_in1_0 : in STD_LOGIC;
    reset_0 : in STD_LOGIC
  );
  end component clk_adc;
begin
clk_adc_i: component clk_adc
     port map (
      clk_in1_0 => clk_in1_0,
      clk_out1_0 => clk_out1_0,
      reset_0 => reset_0
    );
end STRUCTURE;
