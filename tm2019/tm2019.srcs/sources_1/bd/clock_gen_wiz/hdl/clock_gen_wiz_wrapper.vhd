--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
--Date        : Thu Sep 26 14:54:55 2019
--Host        : d-vm-1804 running 64-bit Ubuntu 18.04.3 LTS
--Command     : generate_target clock_gen_wiz_wrapper.bd
--Design      : clock_gen_wiz_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity clock_gen_wiz_wrapper is
  port (
    clk_100M_o_0 : out STD_LOGIC;
    clk_ser_90_o_0 : out STD_LOGIC;
    clk_ser_o_0 : out STD_LOGIC;
    clk_spi_o_0 : out STD_LOGIC;
    reset_rtl : in STD_LOGIC;
    sys_clock : in STD_LOGIC
  );
end clock_gen_wiz_wrapper;

architecture STRUCTURE of clock_gen_wiz_wrapper is
  component clock_gen_wiz is
  port (
    sys_clock : in STD_LOGIC;
    clk_ser_o_0 : out STD_LOGIC;
    clk_ser_90_o_0 : out STD_LOGIC;
    reset_rtl : in STD_LOGIC;
    clk_100M_o_0 : out STD_LOGIC;
    clk_spi_o_0 : out STD_LOGIC
  );
  end component clock_gen_wiz;
begin
clock_gen_wiz_i: component clock_gen_wiz
     port map (
      clk_100M_o_0 => clk_100M_o_0,
      clk_ser_90_o_0 => clk_ser_90_o_0,
      clk_ser_o_0 => clk_ser_o_0,
      clk_spi_o_0 => clk_spi_o_0,
      reset_rtl => reset_rtl,
      sys_clock => sys_clock
    );
end STRUCTURE;
