//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
//Date        : Fri Sep 20 11:06:55 2019
//Host        : d-vm-1804 running 64-bit Ubuntu 18.04.3 LTS
//Command     : generate_target clock_gen_wiz_wrapper.bd
//Design      : clock_gen_wiz_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module clock_gen_wiz_wrapper
   (sys_clock);
  input sys_clock;

  wire sys_clock;

  clock_gen_wiz clock_gen_wiz_i
       (.sys_clock(sys_clock));
endmodule
