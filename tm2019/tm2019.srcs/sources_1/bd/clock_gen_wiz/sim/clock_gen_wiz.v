//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
//Date        : Fri Sep 20 11:06:55 2019
//Host        : d-vm-1804 running 64-bit Ubuntu 18.04.3 LTS
//Command     : generate_target clock_gen_wiz.bd
//Design      : clock_gen_wiz
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "clock_gen_wiz,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=clock_gen_wiz,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=1,numReposBlks=1,numNonXlnxBlks=0,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=USER,synth_mode=OOC_per_IP}" *) (* HW_HANDOFF = "clock_gen_wiz.hwdef" *) 
module clock_gen_wiz
   (sys_clock);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLK.SYS_CLOCK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLK.SYS_CLOCK, CLK_DOMAIN clock_gen_wiz_sys_clock, FREQ_HZ 12000000, INSERT_VIP 0, PHASE 0.000" *) input sys_clock;

  wire sys_clock_1;

  assign sys_clock_1 = sys_clock;
  clock_gen_wiz_clk_wiz_0_0 clk_wiz_0
       (.clk_in1(sys_clock_1),
        .reset(1'b0));
endmodule
